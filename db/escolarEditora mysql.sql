-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for escolareditora
CREATE DATABASE IF NOT EXISTS `escolareditora` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `escolareditora`;


-- Dumping structure for table escolareditora.autor
CREATE TABLE IF NOT EXISTS `autor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `primeiroNome` varchar(50) NOT NULL DEFAULT '',
  `nomeDoMeio` varchar(50) DEFAULT NULL,
  `apelido` varchar(50) NOT NULL DEFAULT '',
  `NIF` char(12) DEFAULT NULL,
  `morada` varchar(50) DEFAULT NULL,
  `codPostal` char(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.dadosproducaolivro
CREATE TABLE IF NOT EXISTS `dadosproducaolivro` (
  `id` int(11) NOT NULL,
  `numeroPaginas` int(11) DEFAULT NULL,
  `peso` int(11) DEFAULT NULL,
  `tiragem` int(11) NOT NULL,
  `grafica` varchar(50) DEFAULT NULL,
  `idPaginador` int(11) DEFAULT NULL,
  `idDesignerCapa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dadosProducaoLivro_recursos` (`idPaginador`),
  KEY `FK_dadosProducaoLivro_recursos_2` (`idDesignerCapa`),
  CONSTRAINT `FK_dadosProducaoLivro_publicacao` FOREIGN KEY (`id`) REFERENCES `publicacao` (`id`),
  CONSTRAINT `FK_dadosProducaoLivro_recursos` FOREIGN KEY (`idPaginador`) REFERENCES `recurso` (`id`),
  CONSTRAINT `FK_dadosProducaoLivro_recursos_2` FOREIGN KEY (`idDesignerCapa`) REFERENCES `recurso` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.editora
CREATE TABLE IF NOT EXISTS `editora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.evolucaovendas
CREATE TABLE IF NOT EXISTS `evolucaovendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ISBN` char(13) NOT NULL,
  `data` date NOT NULL,
  `quantidade` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ISBN_data` (`ISBN`,`data`),
  CONSTRAINT `FK_evolucaovendas_publicacao` FOREIGN KEY (`ISBN`) REFERENCES `publicacao` (`ISBN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.publicacao
CREATE TABLE IF NOT EXISTS `publicacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoPublicacao` enum('Livro','Outro') NOT NULL DEFAULT 'Livro',
  `ISBN` char(13) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `subTitulo` varchar(100) DEFAULT '',
  `dataEdicao` date NOT NULL,
  `pVenda` double NOT NULL,
  `taxaIva` tinyint(4) DEFAULT NULL,
  `idEditora` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ISBN` (`ISBN`),
  KEY `FK_publicacao_editora` (`idEditora`),
  CONSTRAINT `FK_publicacao_editora` FOREIGN KEY (`idEditora`) REFERENCES `editora` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.publicacaoautor
CREATE TABLE IF NOT EXISTS `publicacaoautor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPublicacao` int(11) NOT NULL,
  `idAutor` int(11) NOT NULL,
  `tipoAutoria` enum('Autor','Coordenador') NOT NULL DEFAULT 'Autor',
  `comissao` double NOT NULL COMMENT 'Percentagem de comição',
  `contrato` varchar(255) NOT NULL DEFAULT '' COMMENT 'link para o contrato',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idPublicacao_idAutor_tipoAutoria` (`idPublicacao`,`idAutor`,`tipoAutoria`),
  KEY `FK_publicacaoautor_autor` (`idAutor`),
  CONSTRAINT `FK_publicacaoautor_autor` FOREIGN KEY (`idAutor`) REFERENCES `autor` (`id`),
  CONSTRAINT `FK_publicacaoautor_publicacao` FOREIGN KEY (`idPublicacao`) REFERENCES `publicacao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for view escolareditora.publicacaoautorcomissao
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `publicacaoautorcomissao` (
	`idPublicacao` INT(11) NOT NULL,
	`ISBN` CHAR(13) NOT NULL COLLATE 'utf8_general_ci',
	`tipoAutoria` ENUM('Autor','Coordenador') NOT NULL COLLATE 'utf8_general_ci',
	`idAutor` INT(11) NOT NULL,
	`comissao` DOUBLE NOT NULL COMMENT 'Percentagem de comição',
	`totalComissaoAutor` DOUBLE NULL,
	`total` DOUBLE NOT NULL,
	`tiragem` INT(11) NOT NULL,
	`totalVendas` DOUBLE NULL,
	`qtVendas` BIGINT(11) NULL,
	`comissaoVendas` DOUBLE NULL
) ENGINE=MyISAM;


-- Dumping structure for view escolareditora.publicacaoautorpagamento
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `publicacaoautorpagamento` (
	`idPublicacao` INT(11) NOT NULL,
	`ISBN` CHAR(13) NOT NULL COLLATE 'utf8_general_ci',
	`total` DOUBLE NOT NULL,
	`tiragem` INT(11) NOT NULL,
	`tipoAutoria` ENUM('Autor','Coordenador') NOT NULL COLLATE 'utf8_general_ci',
	`idAutor` INT(11) NOT NULL,
	`comissao` DOUBLE NOT NULL COMMENT 'Percentagem de comição',
	`totalComissaoAutor` DOUBLE NULL,
	`totalVendas` DOUBLE NULL,
	`qtVendas` BIGINT(11) NULL,
	`comissaoVendas` DOUBLE NULL,
	`totalComissaoPaga` DOUBLE NULL
) ENGINE=MyISAM;


-- Dumping structure for view escolareditora.publicacaoautorvendas
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `publicacaoautorvendas` (
	`idEvolucaoVendas` INT(11) NULL,
	`idPublicacao` INT(11) NOT NULL,
	`ISBN` CHAR(13) NOT NULL COLLATE 'utf8_general_ci',
	`data` DATE NULL,
	`qtVendas` BIGINT(20) NOT NULL,
	`totalVendas` DOUBLE NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view escolareditora.publicacaoautorvendascomissao
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `publicacaoautorvendascomissao` (
	`idPublicacao` INT(11) NOT NULL,
	`ISBN` CHAR(13) NOT NULL COLLATE 'utf8_general_ci',
	`tipoAutoria` ENUM('Autor','Coordenador') NOT NULL COLLATE 'utf8_general_ci',
	`idAutor` INT(11) NOT NULL,
	`comissao` DOUBLE NOT NULL COMMENT 'Percentagem de comição',
	`data` DATE NULL,
	`totalVendas` DOUBLE NOT NULL,
	`qtVendas` BIGINT(20) NOT NULL,
	`comissaoVendas` DOUBLE NULL
) ENGINE=MyISAM;


-- Dumping structure for table escolareditora.publicacaocusto
CREATE TABLE IF NOT EXISTS `publicacaocusto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPublicacao` int(11) NOT NULL,
  `rubrica` varchar(50) NOT NULL,
  `valor` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idPublicacao_rubrica` (`idPublicacao`,`rubrica`),
  KEY `FK_publicacaocusto_rubrica` (`rubrica`),
  CONSTRAINT `FK_publicacaocusto_rubrica` FOREIGN KEY (`rubrica`) REFERENCES `rubrica` (`rubrica`),
  CONSTRAINT `FK__publicacao` FOREIGN KEY (`idPublicacao`) REFERENCES `publicacao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for view escolareditora.publicacaototais
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `publicacaototais` (
	`id` INT(11) NOT NULL,
	`ISBN` CHAR(13) NOT NULL COLLATE 'utf8_general_ci',
	`pVenda` DOUBLE NOT NULL,
	`total` DOUBLE NOT NULL,
	`tiragem` INT(11) NOT NULL,
	`dataEdicao` DATE NOT NULL,
	`dataUltimavenda` DATE NULL,
	`totalVendas` DOUBLE NULL,
	`qtVendas` BIGINT(11) NULL
) ENGINE=MyISAM;


-- Dumping structure for table escolareditora.recibo
CREATE TABLE IF NOT EXISTS `recibo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idAutor` int(11) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idAutor_data` (`idAutor`,`data`),
  KEY `FK_recibos_autor` (`idAutor`),
  CONSTRAINT `FK_recibos_autor` FOREIGN KEY (`idAutor`) REFERENCES `autor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.recibopublicacao
CREATE TABLE IF NOT EXISTS `recibopublicacao` (
  `idRecibo` int(11) NOT NULL,
  `idPublicacao` int(11) NOT NULL,
  `idAutor` int(11) NOT NULL,
  `tipoAutoria` enum('Autor','Coordenador') NOT NULL,
  `valor` double NOT NULL,
  PRIMARY KEY (`idRecibo`,`idPublicacao`,`idAutor`,`tipoAutoria`),
  UNIQUE KEY `idRecibo_idPublicacao_idAutor_tipoAutoria` (`idRecibo`,`idPublicacao`,`idAutor`,`tipoAutoria`),
  KEY `FK_recibopublicacao_publicacaoautor` (`idPublicacao`,`idAutor`,`tipoAutoria`),
  CONSTRAINT `FK_recibopublicacao_publicacaoautor` FOREIGN KEY (`idPublicacao`, `idAutor`, `tipoAutoria`) REFERENCES `publicacaoautor` (`idPublicacao`, `idAutor`, `tipoAutoria`),
  CONSTRAINT `FK_recibopublicacao_recibo` FOREIGN KEY (`idRecibo`) REFERENCES `recibo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.recurso
CREATE TABLE IF NOT EXISTS `recurso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `tipoRecurso` enum('Paginação','Capa') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.rubrica
CREATE TABLE IF NOT EXISTS `rubrica` (
  `rubrica` varchar(50) NOT NULL,
  PRIMARY KEY (`rubrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.tbl_cod_postal
CREATE TABLE IF NOT EXISTS `tbl_cod_postal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_distrito` varchar(2) DEFAULT NULL,
  `id_concelho` varchar(2) DEFAULT NULL,
  `cod_localidade` varchar(5) DEFAULT NULL,
  `localidade` varchar(45) DEFAULT NULL,
  `arteria_cod` varchar(15) DEFAULT NULL,
  `arteria_tipo` varchar(25) DEFAULT NULL,
  `pri_prep` varchar(5) DEFAULT NULL,
  `arteria_titulo` varchar(25) DEFAULT NULL,
  `seg_prep` varchar(5) DEFAULT NULL,
  `arteria_designacao` varchar(70) DEFAULT NULL,
  `morada_local` varchar(70) DEFAULT NULL,
  `troco` varchar(80) DEFAULT NULL,
  `porta` varchar(20) DEFAULT NULL,
  `cliente` varchar(80) DEFAULT NULL,
  `cp4` varchar(4) DEFAULT NULL,
  `cp3` varchar(3) DEFAULT NULL,
  `cp_designacao` varchar(30) DEFAULT NULL,
  `morada` varchar(80) DEFAULT NULL,
  `codpostal` varchar(8) DEFAULT NULL,
  `distrito` varchar(30) DEFAULT NULL,
  `concelho` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `cp4_cp3` (`cp4`,`cp3`),
  KEY `id_distrito` (`id_distrito`),
  KEY `id_distrito_id_conselho` (`id_distrito`,`id_concelho`),
  FULLTEXT KEY `codpostal` (`codpostal`,`cp_designacao`,`morada`,`morada_local`,`localidade`,`distrito`,`concelho`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.tbl_concelho
CREATE TABLE IF NOT EXISTS `tbl_concelho` (
  `id_distrito` varchar(2) DEFAULT NULL,
  `id` varchar(2) DEFAULT NULL,
  `designacao` varchar(30) DEFAULT NULL,
  KEY `id_distrito_id` (`id_distrito`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.tbl_distrito
CREATE TABLE IF NOT EXISTS `tbl_distrito` (
  `id` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `designacao` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table escolareditora.utilizador
CREATE TABLE IF NOT EXISTS `utilizador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `authenticationKey` varchar(50) DEFAULT NULL,
  `accessToken` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `tipoUtilizador` enum('Admin','Utilizador') NOT NULL DEFAULT 'Utilizador',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for view escolareditora.publicacaoautorcomissao
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaoautorcomissao`;
CREATE VIEW `publicacaoautorcomissao` AS select pa.idPublicacao,pt.ISBN,pa.tipoAutoria,pa.idAutor,pa.comissao,
			pt.total*comissao/100 AS totalComissaoAutor,pt.total,pt.tiragem,
			pt.totalVendas,pt.qtVendas,pt.totalVendas*comissao/100 as comissaoVendas
from publicacaoautor pa
inner join publicacaototais pt
on pt.id=pa.idPublicacao
inner join autor a
on a.id=pa.idAutor 
group by pa.idPublicacao,pa.tipoAutoria,pa.idAutor ;


-- Dumping structure for view escolareditora.publicacaoautorpagamento
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaoautorpagamento`;
CREATE VIEW `publicacaoautorpagamento` AS select pac.idPublicacao,pac.ISBN,pac.total,pac.tiragem,pac.tipoAutoria,pac.idAutor,pac.comissao,
		pac.totalComissaoAutor as totalComissaoAutor,pac.totalVendas,pac.qtVendas,pac.comissaoVendas,
		ifnull(sum(valor),0.0) as totalComissaoPaga
from publicacaoautorcomissao pac
left outer join recibopublicacao rp
  on rp.idPublicacao=pac.idPublicacao 
and rp.idAutor=pac.idAutor
and rp.tipoAutoria=pac.tipoAutoria
group by pac.idPublicacao,pac.ISBN,pac.total,pac.tiragem,pac.tipoAutoria,pac.idAutor,pac.comissao,
		pac.totalComissaoAutor,pac.totalVendas,pac.qtVendas,pac.comissaoVendas ;


-- Dumping structure for view escolareditora.publicacaoautorvendas
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaoautorvendas`;
CREATE VIEW `publicacaoautorvendas` AS select ev.id as idEvolucaoVendas,p.id as idPublicacao,p.ISBN,ifnull(ev.data,date(now())) as data,
		ifnull(ev.quantidade,0) as qtVendas ,p.pVenda*ifnull(ev.quantidade,0) as totalVendas

from publicacao p
inner join dadosproducaolivro d
on d.id=p.id 
inner join evolucaovendas ev
on ev.ISBN=p.ISBN 
union
select null,id,isbn,dataEdicao,0,0 from publicacao

/*select ev.id as idEvolucaoVendas,p.id as idPublicacao,p.ISBN,ifnull(ev.data,date(now())) as data,
		ifnull(ev.quantidade,0) as qtVendas ,p.pVenda*ifnull(ev.quantidade,0) as totalVendas

from publicacao p
inner join dadosproducaolivro d
on d.id=p.id 
left outer join evolucaovendas ev
on ev.ISBN=p.ISBN */ ;


-- Dumping structure for view escolareditora.publicacaoautorvendascomissao
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaoautorvendascomissao`;
CREATE VIEW `publicacaoautorvendascomissao` AS select pa.idPublicacao,rv.ISBN,pa.tipoAutoria,pa.idAutor,pa.comissao,			
			rv.data,rv.totalVendas,rv.qtVendas,rv.totalVendas*comissao/100 as comissaoVendas
from publicacaoautor pa
inner join publicacaoautorvendas rv
on rv.idPublicacao=pa.idPublicacao
inner join autor a
on a.id=pa.idAutor 
order by pa.tipoAutoria,pa.idAutor,pa.idPublicacao ;


-- Dumping structure for view escolareditora.publicacaototais
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaototais`;
CREATE VIEW `publicacaototais` AS select p.id,p.ISBN,p.pVenda,d.tiragem*p.pVenda as total,
		d.tiragem,
		p.dataEdicao,
		max(ev.data) dataUltimavenda,
		p.pVenda*max(ifnull(ev.quantidade,0)) as totalVendas,
		max(ifnull(ev.quantidade,0)) as qtVendas 
from publicacao p
inner join dadosproducaolivro d
on d.id=p.id 
left outer join evolucaovendas ev
on ev.ISBN=p.ISBN
group by p.id,p.ISBN,p.pVenda,d.tiragem,p.pVenda,p.dataEdicao ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
