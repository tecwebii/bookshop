-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table escolareditora.dadosproducaolivro
CREATE TABLE IF NOT EXISTS `dadosproducaolivro` (
  `id` int(11) NOT NULL,
  `numeroPaginas` int(11) DEFAULT NULL,
  `peso` int(11) DEFAULT NULL,
  `tiragem` int(11) NOT NULL,
  `grafica` varchar(50) DEFAULT NULL,
  `idPaginador` int(11) DEFAULT NULL,
  `idDesignerCapa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dadosProducaoLivro_recursos` (`idPaginador`),
  KEY `FK_dadosProducaoLivro_recursos_2` (`idDesignerCapa`),
  CONSTRAINT `FK_dadosProducaoLivro_publicacao` FOREIGN KEY (`id`) REFERENCES `publicacao` (`id`),
  CONSTRAINT `FK_dadosProducaoLivro_recursos` FOREIGN KEY (`idPaginador`) REFERENCES `recurso` (`id`),
  CONSTRAINT `FK_dadosProducaoLivro_recursos_2` FOREIGN KEY (`idDesignerCapa`) REFERENCES `recurso` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
