-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for view escolareditora.publicacaototais
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaototais`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `publicacaototais` AS select p.id,p.ISBN,p.pVenda,d.tiragem*p.pVenda as total,
		d.tiragem,
		p.dataEdicao,
		max(ev.data) dataUltimavenda,
		p.pVenda*max(ifnull(ev.quantidade,0)) as totalVendas,
		max(ifnull(ev.quantidade,0)) as qtVendas 
from publicacao p
inner join dadosproducaolivro d
on d.id=p.id 
left outer join evolucaovendas ev
on ev.ISBN=p.ISBN
group by p.id,p.ISBN,p.pVenda,d.tiragem,p.pVenda,p.dataEdicao ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
