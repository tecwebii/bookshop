-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table escolareditora.publicacaoautor
CREATE TABLE IF NOT EXISTS `publicacaoautor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPublicacao` int(11) NOT NULL,
  `idAutor` int(11) NOT NULL,
  `tipoAutoria` enum('Autor','Coordenador') NOT NULL DEFAULT 'Autor',
  `comissao` double NOT NULL COMMENT 'Percentagem de comição',
  `contrato` varchar(255) NOT NULL DEFAULT '' COMMENT 'link para o contrato',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idPublicacao_idAutor_tipoAutoria` (`idPublicacao`,`idAutor`,`tipoAutoria`),
  KEY `FK_publicacaoautor_autor` (`idAutor`),
  CONSTRAINT `FK_publicacaoautor_autor` FOREIGN KEY (`idAutor`) REFERENCES `autor` (`id`),
  CONSTRAINT `FK_publicacaoautor_publicacao` FOREIGN KEY (`idPublicacao`) REFERENCES `publicacao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
