-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for view escolareditora.publicacaoautorvendascomissao
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaoautorvendascomissao`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `publicacaoautorvendascomissao` AS select pa.idPublicacao,rv.ISBN,pa.tipoAutoria,pa.idAutor,pa.comissao,			
			rv.data,rv.totalVendas,rv.qtVendas,rv.totalVendas*comissao/100 as comissaoVendas
from publicacaoautor pa
inner join publicacaoautorvendas rv
on rv.idPublicacao=pa.idPublicacao
inner join autor a
on a.id=pa.idAutor 
order by pa.tipoAutoria,pa.idAutor,pa.idPublicacao ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
