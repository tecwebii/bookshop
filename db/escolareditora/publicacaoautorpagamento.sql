-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for view escolareditora.publicacaoautorpagamento
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaoautorpagamento`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `publicacaoautorpagamento` AS select pac.idPublicacao,pac.ISBN,pac.total,pac.tiragem,pac.tipoAutoria,pac.idAutor,pac.comissao,
		pac.totalComissaoAutor as totalComissaoAutor,pac.totalVendas,pac.qtVendas,pac.comissaoVendas,
		cast(ifnull(sum(valor),0) as double) as totalComissaoPaga
from publicacaoautorcomissao pac
left outer join recibopublicacao rp
  on rp.idPublicacao=pac.idPublicacao 
and rp.idAutor=pac.idAutor
and rp.tipoAutoria=pac.tipoAutoria
group by pac.idPublicacao,pac.ISBN,pac.total,pac.tiragem,pac.tipoAutoria,pac.idAutor,pac.comissao,
		pac.totalComissaoAutor,pac.totalVendas,pac.qtVendas,pac.comissaoVendas ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
