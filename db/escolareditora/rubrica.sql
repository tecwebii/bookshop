-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table escolareditora.rubrica
CREATE TABLE IF NOT EXISTS `rubrica` (
  `rubrica` varchar(50) NOT NULL,
  PRIMARY KEY (`rubrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table escolareditora.rubrica: ~8 rows (approximately)
/*!40000 ALTER TABLE `rubrica` DISABLE KEYS */;
INSERT INTO `rubrica` (`rubrica`) VALUES
	('Cartolina'),
	('Outros'),
	('Paginação'),
	('Papel'),
	('Revisão'),
	('Trabalho gráfico'),
	('Tradução'),
	('Transporte');
/*!40000 ALTER TABLE `rubrica` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
