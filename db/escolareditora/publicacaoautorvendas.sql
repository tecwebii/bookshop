-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for view escolareditora.publicacaoautorvendas
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `publicacaoautorvendas`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `publicacaoautorvendas` AS select ev.id as idEvolucaoVendas,p.id as idPublicacao,p.ISBN,ifnull(ev.data,date(now())) as data,
		ifnull(ev.quantidade,0) as qtVendas ,p.pVenda*ifnull(ev.quantidade,0) as totalVendas

from publicacao p
inner join dadosproducaolivro d
on d.id=p.id 
inner join evolucaovendas ev
on ev.ISBN=p.ISBN 
union
select null,id,isbn,dataEdicao,0,0 from publicacao

/*select ev.id as idEvolucaoVendas,p.id as idPublicacao,p.ISBN,ifnull(ev.data,date(now())) as data,
		ifnull(ev.quantidade,0) as qtVendas ,p.pVenda*ifnull(ev.quantidade,0) as totalVendas

from publicacao p
inner join dadosproducaolivro d
on d.id=p.id 
left outer join evolucaovendas ev
on ev.ISBN=p.ISBN */ ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
