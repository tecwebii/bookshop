-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table escolareditora.recibopublicacao
CREATE TABLE IF NOT EXISTS `recibopublicacao` (
  `idRecibo` int(11) NOT NULL,
  `idPublicacao` int(11) NOT NULL,
  `idAutor` int(11) NOT NULL,
  `tipoAutoria` enum('Autor','Coordenador') NOT NULL,
  `valor` double NOT NULL,
  PRIMARY KEY (`idRecibo`,`idPublicacao`,`idAutor`,`tipoAutoria`),
  UNIQUE KEY `idRecibo_idPublicacao_idAutor_tipoAutoria` (`idRecibo`,`idPublicacao`,`idAutor`,`tipoAutoria`),
  KEY `FK_recibopublicacao_publicacaoautor` (`idPublicacao`,`idAutor`,`tipoAutoria`),
  CONSTRAINT `FK_recibopublicacao_publicacaoautor` FOREIGN KEY (`idPublicacao`, `idAutor`, `tipoAutoria`) REFERENCES `publicacaoautor` (`idPublicacao`, `idAutor`, `tipoAutoria`),
  CONSTRAINT `FK_recibopublicacao_recibo` FOREIGN KEY (`idRecibo`) REFERENCES `recibo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
