-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table escolareditora.tbl_distrito
CREATE TABLE IF NOT EXISTS `tbl_distrito` (
  `id` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `designacao` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table escolareditora.tbl_distrito: 29 rows
/*!40000 ALTER TABLE `tbl_distrito` DISABLE KEYS */;
INSERT INTO `tbl_distrito` (`id`, `designacao`) VALUES
	('05', 'Castelo Branco'),
	('04', 'Bragança'),
	('03', 'Braga'),
	('02', 'Beja'),
	('01', 'Aveiro'),
	('06', 'Coimbra'),
	('07', 'Évora'),
	('08', 'Faro'),
	('09', 'Guarda'),
	('10', 'Leiria'),
	('11', 'Lisboa'),
	('12', 'Portalegre'),
	('13', 'Porto'),
	('14', 'Santarém'),
	('15', 'Setúbal'),
	('16', 'Viana do Castelo'),
	('17', 'Vila Real'),
	('18', 'Viseu'),
	('31', 'Ilha da Madeira'),
	('32', 'Ilha de Porto Santo'),
	('41', 'Ilha de Santa Maria'),
	('42', 'Ilha de São Miguel'),
	('43', 'Ilha Terceira'),
	('44', 'Ilha da Graciosa'),
	('45', 'Ilha de São Jorge'),
	('46', 'Ilha do Pico'),
	('47', 'Ilha do Faial'),
	('48', 'Ilha das Flores'),
	('49', 'Ilha do Corvo');
/*!40000 ALTER TABLE `tbl_distrito` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
