<?php


namespace app\components;

use Yii;
use alexgx\phpexcel;
use \PHPExcel_IOFactory;
use yii\helpers\Json;

/**
 * Description of MyExcel
 *
 * @author vitor
 * @property \PHPExcel $data     
 * @property PHPExcel_Reader_IReader $reader
 * @property MyExcelImportModel $model
 */
class MyExcel {
    public $data;
    public $reader;
    public $arrayFieldPrefix='c';    
    private $pModel;
            
    /**
     * 
     * @param String $file
     * @param String $forceFileType
     * @param Mixed $importModel - String with fullpath 
     *                      or String with filename oncontroller views folder 
     *                      or MyExcelImportModel object
     */
    public function __construct($file=null, $forceFileType=null, $importModel=null) {
        if(!is_null($file))
            $this->readSpreadsheetData($file,$forceFileType);
        
        if(!is_null($importModel))
            $this->setModel($importModel);
    }
    
    /**
     * 
     * @param \app\components\MyExcelImportModel $importModel
     */
    public function setModel( $importModel ){            
        if($importModel instanceof MyExcelImportModel){
            $im = $importModel;
        }else if(is_string($importModel) && file_exists($path=$importModel)) {
            $im = new MyExcelImportModel($path);
        }else if(is_string($importModel) && file_exists($path=Yii::getAlias('@app/views/'.Yii::$app->controller->id) .'/'.$importModel)) {
            $im = new MyExcelImportModel($path);
        }
        $this->pModel =$im;        
    }
    
    /**
     * 
     * @return \app\components\MyExcelImportModel 
     */
    public function getModel() {
        return $this->pModel;
    }


    /**
     * 
     * @param String $FileName
     * @param String $fileType
     * @return \app\components\MyExcel
     * @throws Exception
     */
    public function readSpreadsheetData($FileName, $fileType=null) {
          $this->data = array();                    
          
          if (empty($FileName)) {
            throw new Exception("No file specified.");
          }

          if (!file_exists($FileName)) {
            throw new Exception("Could not open " . $FileName . " for reading! File does not exist.");
          }

          try {
              
            $FileType = $fileType=='' ? PHPExcel_IOFactory::identify($FileName) : $fileType;
            $this->reader = PHPExcel_IOFactory::createReader($FileType);
            
            switch ($FileType) {
              case 'Excel2007':
              case 'Excel2003XML':
              case 'Excel5':
              case 'OOCalc':
              case 'SYLK':
                break;
              case 'CSV':
                $SpreadsheetReaderObj->setDelimiter(',');
                $SpreadsheetReaderObj->setEnclosure('"');
                $SpreadsheetReaderObj->setLineEnding('\r\n');
                $SpreadsheetReaderObj->setInputEncoding('UTF-8');
                break;
            }

            $this->data = $this->reader->load($FileName);
            
            return $this;
            
          } catch (Exception $ExceptionObj) {
            //
            // Exception Error Handling
            //
          }
    }
    
    public function Worksheet2Array( $sheetNumber = 1 ) {        
        $ws=1;
        $ret=[];                
        foreach ($this->data->getWorksheetIterator() as $worksheet) {            
            if($ws==$sheetNumber) {
                foreach ($worksheet->getRowIterator() as $row) {    
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false); 
                        $row=[];
                        $i=1;
                        foreach ($cellIterator as $cell) {                        
                                $colName= $this->arrayFieldPrefix.'$1';
                                if (!is_null($cell)) 
                                    $row[$colName] = $cell->getCalculatedValue();
                                $i++;
                        }
                        $ret[]=$row;
                }
                break;
            }
            $ws++;
        }
        return $ret;
    }
    
    
    public function export2Array( $importModel=null ) {                
        $this->setModel($importModel);        
        $im = $this->getModel();
        
        $ret=[];                
        $wsCt=1;
        foreach ($this->data->getWorksheetIterator() as $worksheet) {            
            if($wsCt==$im->workSheetNumber) {
                $rCt =1;
                
                foreach ($worksheet->getRowIterator() as $row) {    
                    
                    if($rCt>=$im->topLine){
                            $cellIterator = $row->getCellIterator();
                            $cellIterator->setIterateOnlyExistingCells(false); 
                            $row=[];
                            $i=1;
                            $cCt=1;
                            foreach ($cellIterator as $cell) {                        
                                    if($cCt>=$im->leftColumn && $cCt<=($im->leftColumn+ sizeof($im->columns))) {
                                        $colName= (isset($im->columns[$i-1])
                                                ? $im->columns[$i-1]
                                                        : $this->arrayFieldPrefix.'$1');
                                        if (!is_null($cell)) 
                                            $row[$colName] = $cell->getCalculatedValue();
                                        $i++;
                                    }
                                    $cCt++;
                            }                           
                           $ret[]=$row;
                    }
                    $rCt++;
                    
                }
                break;
            }
            $wsCt++;
        }
        $i=0;
        while($i<$im->ExcludeLines){
            unset($ret[sizeof($ret)-1]);
            $i++;
        }
        return $ret;
    }

}
