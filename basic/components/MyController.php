<?php

namespace app\components;

use Yii;
use yii\web\Controller;


/**
*
 * Description of MyController
 *
 * @author vitor
 */
class MyController extends Controller{
	
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
            'class' => \yii\filters\AccessControl::className(),
                'rules' =>  [
                    // allow all authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],                        
                    ],
                    // deny all unauthenticated
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ]
            ]

        ];
    }
	 
    public function beforeAction($action) {
        
        
        return parent::beforeAction($action);
    }
    
    public function afterAction($action, $result) {

        Yii::$app->response->headers->add('X-Response-Url', \yii\helpers\Url::current() );
        return parent::afterAction($action, $result);
    }
}
