<?php

/**
      <?= $form->field($model, 'codPostal')->widget(\app\components\MyAutoComplete::classname(), 
        [   'ajaxUrl'       => $acUrl ,
            'renderLabel'   => "function( item ) {
    return item.valueid + ' - ' + item.label 
                    + '<br>Designação: ' + item.cp_designacao 
                    + '<br>Distrito: '   + item.distrito
                    + '<br>Concelho: '   + item.concelho
                    + (item.morada==''? '':'<br>Morada: ' + item.morada);
}",
            'renderItem'    => "function( ul, item ) {
    return $( '<li></li>' )
        .data( 'item.autocomplete', item )
        .append( '<a>' + item.label + '</a>' )
        .appendTo( ul );
}",
            'options'       => ['class' => 'form-control'],            
            'clientOptions' => [
                'name'  => 'Autor[codPostal]',
                'value' => $model->codPostal,
                ]
            ]) ?>

 */

namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\web\JsExpression;

/**
 * Description of myAutoComplete
 *
 * @author vitor
 */
class MyAutoComplete extends \yii\jui\InputWidget {
    public $ajaxUrl='';
    public $renderItem='';
    public $renderLabel='';
    public $minLength=0;
    
    public function run()
    {                        
        echo $this->renderWidget();
        $this->setupAutoComplete($this->options['id']);
        $this->registerWidget('autocomplete');
        $this->registerAutoCompleteLabel($this->options['id']);
    }
    
    
    private function setupAutoComplete($id){  
        $this->clientOptions['minLength'] = $this->minLength;
        $this->clientOptions['options'] = [            
                        'showAnim'  => 'fold',                        
                        'delay'     => 500,
                        'select'    => new JsExpression("
function(event, ui){ 
      $('#$id').val(ui.item['value']);                                                     
}"              
                                                        ),    
//                        'open'      => new JsExpression("function(event, ui){}"),
//                        'close'     => new JsExpression("function(event, ui){}"),
                            ];
        $this->clientOptions['htmlOptions'] = [
                          'style'   =>'height: 20px; width: 300px;',
                            ];        
        $this->clientOptions['source'] = new JsExpression("
    function(request, response) {
            var aux     = $('#$id').data('auxiliar'),
                value   = $('#$id').val();
            /*if(value!='' && $(aux).length>0 && ( line= $.grep(aux, function(e){ 
                                                        return (e.value+'').match( new RegExp(value+'*'))[0]==(e.value+'') ; 
                                                  })) ){ //&& $(line).length>0 && line[0]==value ) {             
                return;
            }*/
            
            
            var _this = $(this);
            var _element = $(this.element);
            var jqXHR = _element.data('jqXHR');
            if(jqXHR)
                    jqXHR.abort();                                    
            _element.data('jqXHR', $.ajax({
                    url: '$this->ajaxUrl',
                    dataType: 'jsonp',
                    jsonpCallback: '_callback',
                    data: {
                            value   : $('#$id').val(),                                 
                            offset  : $('#$id')[0].selectionStart,
                            limit   : 40
                    },
                    success: function(data) {
                            _this.removeData('jqXHR'); 
                            if($(data).length>0)
                                $('#$id').data('auxiliar',data);
                            response(data);
                    },
                    error: function( jqXHR ){
                            if(jqXHR.statusText!='')
                            var a=jqXHR.statusText;
                    }
                }));                            
    }");        
    }

    /**
     * Renders the AutoComplete widget.
     * @return string the rendering result.
     */
    public function renderWidget()
    {
        if ($this->hasModel()) {
            return Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            return Html::textInput($this->name, $this->value, $this->options);
        }
    }
    

    private function registerAutoCompleteLabel($id) {        
        $lclass= 'autocomplete-label';
        $formId= self::$stack[0]->options['id'];         
        $divClass= "form-group";
        $_renderItem= $this->AutoCompleteRenderItemScript($id);
        $_renderLabel= $this->renderLabel=='' ? "function(item) { return item.label; }" : $this->renderLabel;
        $this->getView()->registerJs("                                
var _callback= function( receivedData ) {
    return receivedData;
};            
var MyAutoComplete=function( inputId, formId, ajaxUrl ) {            
    var me=this;    
    this.renderLabel=$(inputId).data('autocomplete_renderLabel'),
    this.inputVal=function() {
        var value= $(inputId).val();    
        return $.trim(value);
    },
    this.setLabel=function ( value ) {                                
            if($(value).length==0) {
                vazio=me.inputVal()==''?'[vazio]':me.inputVal();
                value=$(inputId).attr('empty')==undefined ? vazio: $(inputId).attr('empty');
            }else{
                value=me.renderLabel(value);
            }
            if( $(inputId).closest('.$lclass').find('.value').length>0 ){
                $(inputId).closest('.$lclass').find('.value').html(value);          
            }else{            
                $(inputId).before('<div class=\"value\" style=\"display:block;position:absolute;\">'+value+'</div>');
                $(inputId).closest('.$lclass').find('.value').css('marginTop',1);          
                $(inputId).closest('.$lclass').find('.value').css('marginLeft',7);                          
                $(inputId).closest('.$lclass').find('.value').css('padding',6);                      
                $(inputId).closest('.$lclass').find('.value').css('height',29);                                      
                $(inputId).closest('.$lclass').find('.value').css('overflow','hidden');                                          
                $(inputId).closest('.$lclass').find('.value').hover(function(e) { 
                    $(this).css('height',e.type === 'mouseenter'?'auto':29); 
                });

            }            
            $(inputId).closest('.$lclass').find('.value').css('backgroundColor',$(inputId).css('backgroundColor'));          
    },
    this.getLabel=function () {
        var value= me.inputVal();
        if( value==''){
            me.setLabel([]);   
            return;
        }
        var aux = $(inputId).data('auxiliar');
        var line;    
        if($(aux).length>0 && ( line= $.grep(aux, function(e){ 
                                                        return (e.value+'').match( new RegExp(value+'*'))[0]==(e.value+'') ; 
                                                  })) ){ //&& $(line).length>0 && line[0]==value ) {             
            me.setLabel(line[0]);
        }
        else {
            $.ajax({
                    url: ajaxUrl+'?id='+value,               
                    dataType: 'jsonp',
                    jsonpCallback: '_callback',                
            }).complete(function(jqXHR, textStatus ){
                var texto=jqXHR.responseText.substr(this.jsonpCallback.length);
                data = eval(texto);                
                if(typeof data === 'object'){                    
                    if(data && data.length>0) {
                        $(inputId).data('auxiliar',data);
                        me.getLabel();
                    }else
                        me.setLabel([]);
                }
            });
        }
    };

    $(document).on('focus',formId+' input,'+formId+' textarea,'+formId+' select', function(ev,p2){
        $(formId).data('lastfocus',this.id);
        $(this).data('lastValue',me.inputVal());
        if(ev.target== $(inputId)[0] && (typeof $(inputId).attr('readonly') == 'undefined'))
            $(inputId).closest('.$lclass').find('.value').hide();    
    });

    $(document).on('blur',inputId, function(ev,p2){
        me.getLabel();                
        $(inputId).closest('.$lclass').find('.value').show();
    });

    $(document).on('click',inputId, function(e){
        if(! $(this).is(':visible') ) return;
        var x = $(e.target).width()-e.offsetX;
        if(x<=60) {
            $(e.target).autocomplete('search');
        }
    });
    $(document).on('mousemove',inputId, function(e){
        if(! $(this).is(':visible') ) return;
        var x = $(e.target).width()-e.offsetX;
        if(x<=60) 
            $(this).css('cursor','pointer');
        else
            $(this).css('cursor','auto');
    });


    $(document).on('click', 'form .$divClass .value', function(){
        //$(this).hide();
        $(inputId).get(0).focus();
    });    

    $(inputId).closest('.$divClass').addClass('$lclass').css('position','relative');
    me.getLabel();    
}
    
",\yii\web\view::POS_BEGIN,"autocomplete-label-JS");
        
        $this->getView()->registerJs("
$('#$id').data('autocomplete_renderLabel',$_renderLabel);            
$('#$id').data('$lclass', new MyAutoComplete( '#$id','#$formId', '$this->ajaxUrl' ) );
".$this->AutoCompleteRenderItemScript($id).";

        ");
   
    }
    
    private function AutoCompleteRenderItemScript($id){
        if( empty($this->renderItem) ) {
            return "";
        }else{
            return "$('#$id').data('ui-autocomplete')._renderItem = ".$this->renderItem.";";
        }
    }
            
}

