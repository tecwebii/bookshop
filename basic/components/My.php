<?php


namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use \DateTime;

/**
 * Description of My
 *
 * @author Vitor
 */
class My {
    
    /**
 * SQL Like operator in PHP.
 * Returns TRUE if match else FALSE.
 * @param string $pattern
 * @param string $subject
 * @return bool
 */
    
    public static function like_match($pattern, $subject)
    {
        $pattern = str_replace('%', '.*', preg_quote($pattern));
        return (bool) preg_match("/^{$pattern}$/i", $subject);
    }
    
    public static function prettyPrint( $json,$newline="\n" ,$tab="\t")
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = NULL;
        $json_length = strlen( $json );

        for( $i = 0; $i < $json_length; $i++ ) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if( $ends_line_level !== NULL ) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if ( $in_escape ) {
                $in_escape = false;
            } else if( $char === '"' ) {
                $in_quotes = !$in_quotes;
            } else if( ! $in_quotes ) {
                switch( $char ) {
                    case '}': case ']':
                        $level--;
                        $ends_line_level = NULL;
                        $new_line_level = $level;
                        break;

                    case '{': case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ": case "\t": case "\n": case "\r":
                        $char = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level = NULL;
                        break;
                }
            } else if ( $char === '\\' ) {
                $in_escape = true;
            }
            if( $new_line_level !== NULL ) {
                $result .= $newline.str_repeat( $tab, $new_line_level );
            }
            $result .= $char.$post;
        }
        return $result;
    }
    
    public static function http_requested_data_type(){
        if(isset($_GET['_simul_http_accept']))
            return $_GET['_simul_http_accept'];
        $dt= isset($_SERVER['HTTP_ACCEPT'])?strtolower($_SERVER['HTTP_ACCEPT']):$simul;
        $dt=str_replace(',', ';', $dt);
        $a=explode(';',$dt);
        $ret= 'unknown';
        foreach($a as $i=>$v){
            $st = strtolower(substr(trim($v),0,11));            
            $a=explode('/',$v);
            if( isset($a[1]) ) {
                $ret = trim($a[1]);
                break;
            }
            $ret=isset($a[1]) ?  : 'unknown';            
        }       
        return $ret;
    }
         
    static public function response_json( $array,$callback=null )
    {                        
        if(! in_array(My::http_requested_data_type(),['json','javascript','ecmascript','x-ecmascript','*'])) {     
                header('Content-type: text/html; charset=UTF-8'); 
                echo self::prettyPrint((JSON::encode($array)),"<br>","&nbsp;&nbsp;&nbsp;");//).'<br>';
            }else{  
                $callback= is_null($callback) ? Yii::$app->getRequest()->getQueryParam('callback') : '';
                if( $callback=='' ) {
                    header('Content-type: application/json; charset=UTF-8 ');            
                    echo JSON::encode($array);
                }else{
                    header('Content-type: application/javascript; charset=UTF-8 ');            
                    echo $callback.'('.JSON::encode($array).')';                    
                }
            }
            exit();
    }
    
    /**
     * 
     * @param type $provider
     * @param String $fieldName
     * @return double
     */
    public static function pageTotal($provider, $fieldName)
    {
        $total=0;
        foreach($provider as $item){
            $total+=$item[$fieldName];
        }
        return $total;
    }
    
    
    
    public static function sqlData($stringData) {
        $data=DateTime::createFromFormat('d-m-Y',$stringData);        
        return $data==false ? $stringData : $data->format('Y-m-d');
    }

    public static function phpData($stringData) {
        $data=DateTime::createFromFormat('Y-m-d',$stringData);
        return $data==false ? $stringData : $data->format('d-m-Y');        
    }
    
    public static function cURL2String($url) 
    {   
      $ch = curl_init(); 
        if($ch) 
        { 
            $ret = '';


            if( !curl_setopt($ch, CURLOPT_URL, $url) )  { 
                curl_close($ch); // to match curl_init() 
                return ""; 
            }       
            if( !curl_setopt($ch, CURLOPT_HEADER, 0) ) return "";//FAIL: curl_setopt(CURLOPT_HEADER)"; 
            if( !curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) ) return "";//FAIL: curl_setopt(CURLOPT_RETURNTRANSFER)"; 

            if( ! ($result = curl_exec($ch)) ) return "";//FAIL: curl_exec()";       
            curl_close($ch); 
            return $result;     
        } 
        else return ""; //FAIL: curl_init()"; 
    } 

    
    /**     
     *
     * @param string $message the message to be formatted.
     * @param array $params the parameters that will be used to replace the corresponding placeholders in the message.
     * @return string the formatted message.
     */
    public static function format($message, $params)
    {        
        foreach ($params as $name => $value) {
            $p['{' . $name . '}'] = $value;
        }

        return strtr($message, $p);
    }
    
    
    public static function tableExists($table) {
        $pdo = Yii::$app->db->pdo;
        // Try a select statement against the table
        // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
        try {
            $result = $pdo->query("SELECT * 
                                    FROM information_schema.tables
                                    WHERE table_schema = 'yourdb' 
                                        AND table_name = '$table'
                                    LIMIT 1;");
            return $result->rowCount()==1;
        } catch (Exception $e) {
            // We got an exception == table not found
            return FALSE;
        }

        // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return $result !== FALSE;
    }
    
    public static function is_subclass_of($obj , $parent_className) {
        $objClassName=get_class($obj);
        $arr=class_parents($objClassName, $parent_className);
        $ret = in_array($parent_className, $arr) || in_array('\\'.$parent_className, $arr);
        return $ret;
    }
    
    
    /**
    * Decrypt data from a CryptoJS json encoding string
    *
    * @param mixed $passphrase
    * @param mixed $jsonString
    * @return mixed
    */
    public static function cryptoJsAesDecrypt($passphrase, $jsonString){
        $jsondata = json_decode($jsonString, true);
        $salt = hex2bin($jsondata["s"]);
        $ct = base64_decode($jsondata["ct"]);
        $iv  = hex2bin($jsondata["iv"]);
        $concatedPassphrase = $passphrase.$salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }

    /**
    * Encrypt value to a cryptojs compatiable json encoding string
    *
    * @param mixed $passphrase
    * @param mixed $value
    * @return string
    */
    public static function cryptoJsAesEncrypt($passphrase, $value){
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx.$passphrase.$salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv  = substr($salted, 32,16);
        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return json_encode($data);
    }
    
    
    public static function modelError2String( $arr ) {
        $st="";
        foreach($arr as $k=>$v){                        
            if(is_array($v)) foreach($v as $v1)
                $st = $st . $v1 . "<br>";
            else
                $st = $st . $v . "<br>";
        }
        return $st;
    }


}
   
?>
