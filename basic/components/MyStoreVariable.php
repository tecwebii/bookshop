<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \Yii;

namespace app\components;

/**
 * Description of myStoreVariable
 *
 * @author vitor
 */
class MyStoreVariable {
    //put your code here
    public $directory='';
    private $fileName;
        
    public function checkDirectory() {
        if(!file_exists($this->directory)) {
            mkdir($this->directory);
        }
    }
    
    static public function randomString( $len=32 ){
        return \Yii::$app->security->generateRandomString($len);
    }
    
    public function __construct( $fileName="" ) {
        $this->directory = realpath('../').'/var'; //\Yii::getAlias('@app'.'/'.vars);
        if($fileName=='')
            $this->fileName = self::randomString().'.php';
        else
            $this->setFullPath($fileName);
    }
    
    public function exists() {
        return file_exists($this->getFullPath());
    }
    
    public function getFullPath() {
        return $this->directory.'/'.$this->fileName;
    }
    
    public function setFullPath( $fileName ){
        if ( dirname($fileName)!='.' ){
            $this->directory = dirname($fileName);
        }
        $this->fileName = basename($fileName);
    }
    
    public function save( $var ) {
        $var_fileName = $this->getFullPath();
        $this->checkDirectory();
        file_put_contents($var_fileName, '<?php return ' . var_export($var, true) . ';');
    }    
    
    public function saveTo( $fileName ,$var ) {
        $this->setFullPath( $fileName );        
        $this->save($var);
    }
    
    public function load() {
        $var_fileName = $this->getFullPath();
        $arr = require ($var_fileName);
        return $arr;
    }
    
    public function loadFrom( $fileName ){
        $this->setFullPath( $fileName );
        $this->fileName = basename($fileName);
        return $this->load();        
    }
}
