<?php


namespace app\components;

use Yii;
use yii\helpers\Json;

class MyExcelImportModel {
    public $filetype;
    public $workSheetNumber;
    public $topLine;
    public $ExcludeLines;
    public $leftColumn;    
    public $columns = [];
    public $types = [];
    private $cTypes = [];
    
    /**
     * 
     * @param String $modelFile
     */
    public function __construct($file=null) {
        if(!is_null($file))
            $this->loadJson ($file);
    }
    
    public function defaultValues(){
        $this->filetype = 'Excel2007';
        $this->workSheetNumber = 1;
        $this->topLine = 5;
        $this->ExcludeLines = 0;
        $this->leftColumn = 1;    
        $this->columns = ['isbn','titulo','quantidade','valor'];
        $this->types = ['string','string','integer','double'];
        $this->cTypes=[];
    }
        
    public function loadJson( $file ){
        if(dirname($file)=='.')
            $path = Yii::getAlias('@app/views/'.Yii::$app->controller->id) .'/'.$file;
        else
            $path = $file;        
        $this->types=[];
        $this->cTypes=[];
        $arr = Json::decode(file_get_contents($path));
        foreach ($arr as $k=>$v)
            $this->$k = $v;
        foreach ($this->columns as $k=>$v) {
            $a= explode(':',$v);            
            $this->columns[$k] = $a[0];            
            if(isset($a[1]))
                $this->types[$k] = $a[1];                
        }
        return $this;
    }

    public function saveJson( $file){
        $arr=[];
        foreach($this as $k=>$v)
            $arr[$k] = $v;  
        $a=dirname($file);
        if(dirname($file)=='.')
            $path = Yii::getAlias('@app/views/'.Yii::$app->controller->id).'/'.$file;
        else
            $path = $file;
        file_put_contents($path,Json::encode($arr));
        return $this;
    }
    
    public function typeOfColumn( $columnName ){
        if(isset($this->cTypes[$columnName])) {
            return $this->cTypes[$columnName];
        }
        foreach($this->columns as $i=>$c) {
            $this->cTypes[$c]= isset($this->types[$i]) ? $this->types[$i] : 'string';
        }
        return isset($this->cTypes[$columnName]) 
                    ? $this->cTypes[$columnName]
                    : '';
    }
    
    public function fixVal( $columnName, $val ){
        $ret=$val.'';
        $type=$this->typeOfColumn($columnName);
        if($type=='integer')
            $ret = intval($val);
        elseif($type=='double')
            $ret = doubleval($val);
        return $ret;
    }
}
