<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoCustos */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Publicacao Custos',
]) . ' ' . $model->idPublicacao;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Publicacao Custos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idPublicacao, 'url' => ['view', 'idPublicacao' => $model->idPublicacao, 'rubrica' => $model->rubrica]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="publicacao-custos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
