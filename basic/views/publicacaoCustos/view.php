<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoCustos */

$this->title = $model->idPublicacao;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Publicacao Custos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-custos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'idPublicacao' => $model->idPublicacao, 'rubrica' => $model->rubrica], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'idPublicacao' => $model->idPublicacao, 'rubrica' => $model->rubrica], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPublicacao',
            'rubrica',
            'valor',
        ],
    ]) ?>

</div>
