<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoCustos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicacao-custos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPublicacao')->textInput() ?>

    <?= $form->field($model, 'rubrica')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'valor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
