<?php
use yii\helpers\Html;

?>

<style>
    #ajaxActionBoard div
    {
        width:auto;
    }
    #ajaxActionBoard{
        position: fixed;    
        width:auto;    
        padding: 3px;
        top: 5px;
        left: 5px;    
        background-color: #373837;
        opacity: 0.3;
        filter: alpha(opacity = 30);
        -moz-border-radius: 15px;
        -webkit-border-radius: 15px;
        -khtml-border-radius: 15px;
        border-radius: 15px;
    }
</style>
<?php /* @var $this Controller */ 

$this->registerJs('
    // load Bottom Footer
    $(document.body).children().eq(0).before('."'". '<div id="ajaxActionBoard" style="position:fixed;display:none"></div>'."'". ');    
    $(document).ajaxSend( function(event, jqXHR, ajaxOptions) {
        /* create smallbox with ajax animated gif && title description of ajax action */            
        $("#ajaxActionBoard").append('."'".'<img id="" title="" src="/img/loader3.gif">'."'".')
                                .show();
    }).ajaxComplete( function(event, XMLHttpRequest, ajaxOptions) {
        /* delete previous created smallbox */
        $("#ajaxActionBoard").find("img:first").remove();
        if($("#ajaxActionBoard").find("img").length==0) {
            $("#ajaxActionBoard").hide();
        }
        /* define object class frameTarget with empty (this layout does not use iframe) */
        $(".frameTarget").attr("target","");
    });
');    