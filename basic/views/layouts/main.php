<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon32x32.png">    
    <link rel="icon" type="image/png" href="/favicon64x64.png" sizes="64x64" />
    <link rel="icon" type="image/png" href="/favicon32x32.png" sizes="32x32" />    
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="/css/print.css" rel="stylesheet" type="text/css" media="print"/>
</head>
<body>

<?php $this->beginBody() ?>    
    <?php $this->render('/layouts/ajax'); ?>
    <div class="wrap">        
        <?php
            NavBar::begin([
                'brandLabel' => '<img src="/img/logo_gee-int.png">',                
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [                    
                    'class' => 'navbar-inverse',
                ],
            ]);
            $userItems= [];
            if(! Yii::$app->user->isGuest)
                $userItems= [
                    ['label' => Yii::t('app','Tabelas'), 
                        'items' => [
                            ['label' => Yii::t('app','Autores'), 'url' => ['/autor']],
                            ['label' => Yii::t('app','Editoras'), 'url' => ['/editora']],
                            ['label' => Yii::t('app','Recursos'), 'url' => ['/recurso']],
                            ['label' => Yii::t('app','Publicacaos'), 'url' => ['/publicacao']],
                            '<li class="divider"></li>',
                            ['label' => Yii::t('app','Recibos'), 'url' => ['/recibo']],
                            ['label' => Yii::t('app','Evolucao Vendas'), 'url' => ['/evolucaovendas/index']],
                            '<li class="divider"></li>',
                            ['label' => Yii::$app->user->identity->isAdmin() ?Yii::t('app','Utilizadores'):'Dados pessoais', 
                                        'url' => [Yii::$app->user->identity->isAdmin() ?'/utilizador':'/utilizador/'.Yii::$app->user->getId()]],
                    ]]
                ];
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => 
                    array_merge($userItems,[
//                        ['label' => 'Home', 'url' => ['/site/index']],
                        ['label' => Yii::t('app','Sobre'), 'url' => ['/site/about']],
//                        ['label' => 'Contact', 'url' => ['/site/contact']], 
                        Yii::$app->user->isGuest ?
                            ['label' => Yii::t('app','Login'), 'url' => ['/site/login']] :
                            ['label' => Yii::t('app','Logout').' (' . Yii::$app->user->identity->username . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']],
                    ])
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; <?=  Yii::$app->params['copyright'] ?> <?= '2015' ?></p>
            <p class="pull-right"><?= Html::a(Yii::t('app','Sobre'), ['/site/about']); ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
    <script>
        $(document).ready(function(){
            $('#topLogo').show('slow');
        });
    </script>
</body>
</html>
<?php $this->endPage() ?>
