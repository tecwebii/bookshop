<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UtilizadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Utilizadores');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilizador-index">
    <?php Pjax::begin(); echo GridView::widget([        
        'id' => 'grid-utilizador',
        'resizableColumns'=>true,
        'persistResize'=>true,
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,               
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Create Utilizador'), ['create'], ['class' => 'btn btn-success']).' '.
                      Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-info']),
            'after' =>false,
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,        
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'nome',           
             'email',
             'telefone',
             'tipoUtilizador',
            ['class' => '\kartik\grid\ActionColumn',
                'hiddenFromExport'=>true
            ],        
        ],
    ]); Pjax::end(); ?>

</div>
