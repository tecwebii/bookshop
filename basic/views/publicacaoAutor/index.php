<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $tipoAutoria String */

$this->title = Yii::t('app', '{tipoAutoria} Publicacao',['tipoAutoria'=>$filtro['tipoAutoria']]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-autor-index">
    <p>
        <?= Html::a(Yii::t('app', 'Create {tipoAutoria} Publicacao',
                                ['tipoAutoria'=>$filtro['tipoAutoria']]), 
                        ['/publicacaoautor/create/'.$filtro['id'].'/'.$filtro['tipoAutoria']], 
                        ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
            'id' => 'grelha-publicacao-'.$filtro['tipoAutoria'],            
            'dataProvider'=>$dataProvider,                        
            'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => Yii::t('app', $filtro['tipoAutoria']),
                    'format' => 'raw',
                    'value' => function ($model) {                      
                            return '<div>'.$model->autor->primeiroNome.' '.
                                           $model->autor->nomeDoMeio.' '.
                                           $model->autor->apelido.'</div>';
                    },
                ],
                ['class'=>  'yii\grid\DataColumn',
                    'attribute'=>Yii::t('app', 'Comissao '.$filtro['tipoAutoria']),
                    'value' => function ($model) {                      
                            return $model->comissao;
                    },
                    'format'=>['decimal'],
                    'contentOptions'=>['style'=>'text-align:right'],
                    'headerOptions'=>['style'=>'text-align:right'],
                ],
                [
                    'attribute' =>'contratoHTML',
                    'format' =>'raw',
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>

</div>
