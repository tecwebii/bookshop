<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoAutor */

$this->title = Yii::t('app', 'Create {tipoAutoria} Publicacao', 
    ['tipoAutoria' => $model->tipoAutoria]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '{tipoAutoria} Publicacao',
                                ['tipoAutoria'=>$model->tipoAutoria]), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-autor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
