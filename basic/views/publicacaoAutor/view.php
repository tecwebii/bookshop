<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoAutor */

$this->title = $model->idPublicacao;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '{tipoAutoria} Publicacao',
                                ['tipoAutoria'=>$model->tipoAutoria]), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-autor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'idPublicacao' => $model->idPublicacao, 'idAutor' => $model->idAutor, 'tipoAutoria' => $model->tipoAutoria], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'idPublicacao' => $model->idPublicacao, 'idAutor' => $model->idAutor, 'tipoAutoria' => $model->tipoAutoria], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>                 
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        //    'tipoAutoria',
            [                      // the owner name of the model
                'label' => Yii::t('app','ISBN'),
                'value' => $model->publicacao->ISBN,
            ],            
            [                      // the owner name of the model
                'label' => Yii::t('app',$model->tipoAutoria),
                'value' => $model->autor->primeiroNome.' '.$model->autor->nomeDoMeio.' '.$model->autor->apelido,
            ],            
            'comissao',
            [
                'format' => 'raw',
                'attribute'  => 'contratoHTML',
            ],
        ],
    ]) ?>

</div>
