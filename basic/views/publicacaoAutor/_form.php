<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoAutor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicacao-autor-form">

    <?php $form = ActiveForm::begin([
        'id' => 'Wpublicacao'.$model->tipoAutoria,
        'options' => ['enctype' => 'multipart/form-data'],
        'validationUrl' => ['/publicacaoautor/validate'],//,'idPublicacao'=>$model->id],
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
    ]); ?>
    <?= $form->field($model, 'id',['options'=>["style"=>"display:none"]])->hiddenInput() ?>
    
    <?= $form->field($model, 'idPublicacao',['options'=>["style"=>"display:none"]])->hiddenInput() ?>
    
    <?= $form->field($model, 'tipoAutoria',['options'=>["style"=>"display:none"]])->hiddenInput() ?>
    
    <?= $form->field($model, 'idAutor')->widget(\app\components\MyAutoComplete::classname(), 
        [               
            'ajaxUrl'       => '/autor/autocomplete' ,
            'options'       => ['class' => 'form-control',
                                'readonly' => ! $model->isNewRecord,],            
            'clientOptions' => [
                'name'  => 'PublicacaoAutor[idAutor]',
                'value' => $model->idAutor,
                ]
            ]) ?>
    
    <?= $form->field($model, 'comissao')->textInput() ?>
    
    <div class="form-group">       
    <?= Html::activeFileInput($model,'contrato',['maxlength' => 255,'style' => 'display:none']) ?>
    <?= Html::label(Yii::t('app','Contrato'),'publicacaoautor-originalfilename',['class'=>'control-label']); ?>
    <?= Html::buttonInput($model->originalFileName!=''
                    ? $model->originalFileName
                    : Yii::t('app','Escolher ficheiro...'),[
            'id'        => 'publicacaoautor-originalfilename',
            'maxlength' => 255,
            'readonly'=>true,            
            'style'=>'min-width:100%',
            'class' =>'btn btn-primary',
        ]); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

                    
<?php
$script = "   
    
$('#Wpublicacao$model->tipoAutoria').find('#publicacaoautor-originalfilename').click(function(event) {
    event.preventDefault();
    $('#Wpublicacao$model->tipoAutoria').find('#publicacaoautor-contrato').trigger('click');
});
$('#Wpublicacao$model->tipoAutoria').find('#publicacaoautor-contrato').change(function(event) {
    $('#Wpublicacao$model->tipoAutoria').find('#publicacaoautor-originalfilename').attr('value',
                    $('#Wpublicacao$model->tipoAutoria').find('#publicacaoautor-contrato').val().replace(/^.*[\\\/]/, ''));
});
";
$this->registerJs($script);
?>