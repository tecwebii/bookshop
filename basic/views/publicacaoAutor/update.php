<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoAutor */

$this->title = Yii::t('app', 'Update {tipoAutoria} Publicacao: ', 
    ['tipoAutoria' => $model->tipoAutoria . ' ' . $model->idPublicacao]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '{tipoAutoria} Publicacao',
                                ['tipoAutoria'=>$model->tipoAutoria]), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idPublicacao, 
                            'url' => ['view', 
                            'idPublicacao' => $model->idPublicacao, 
                            'idAutor' => $model->idAutor, 
                            'tipoAutoria' => $model->tipoAutoria]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="publicacao-autor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
