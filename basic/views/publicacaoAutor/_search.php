<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoAutorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicacao-autor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idPublicacao') ?>

    <?= $form->field($model, 'idAutor') ?>

    <?= $form->field($model, 'tipoAutoria') ?>

    <?= $form->field($model, 'comissao') ?>

    <?= $form->field($model, 'contrato') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
