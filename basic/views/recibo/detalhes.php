<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;
use yii\data\ActiveDataProvider;
use \kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $detalhes yii\db\ActiveRecord */

$this->title = Yii::t('app', 'Detalhes Recibo');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-details">

    <h2><?= Html::encode($this->title) ?></h2>
    <?= GridView::widget([
        'dataProvider' => new activeDataProvider([
                                'query' => $detalhes,
                                'pagination' => [
                                    'pageSize' => 9999,
                                ],
                            ]),      
        'columns' => [
//            [   'class' => 'yii\grid\SerialColumn'],            
            [
                    'attribute' => Yii::t('app', 'Publicacao'),
                    'format' => 'raw',
                    'value' => function ($model) {                                                  
                            return DetailView::widget([
                                        'model' => $model,
                                        'attributes' => [                                                        
                                            [
                                                'label' => Yii::t('app','ISBN'),
                                                'value'=>$model->publicacao->ISBN,
                                            ],
                                            [
                                                'format' => 'raw',
                                                'label' => Yii::t('app','Titulo'),
                                                'value'  => $model->publicacao->descricaoHTML,
                                            ],
                                            'tiragem',
                                            [
                                                'label' => Yii::t('app','P Venda'),
                                                'value'=>Yii::$app->formatter->asCurrency($model->publicacao->pVenda),                                                
                                            ],  
                                            [
                                                'label' => Yii::t('app','Valor Total'),
                                                'value' =>Yii::$app->formatter->asCurrency($model->valorTotal),  
                                            ] ,
                                    ]
                            ]);
                                            
                    },
                ],
                [
                    'attribute' => Yii::t('app', 'Autoria'),
                    'format' => 'raw',
                    'value' => function ($model) {                                                  
                            return DetailView::widget([
                                        'model' => $model,
                                        'attributes' => [                                                                                                              
                                            'tipoAutoria',
                                            [
                                                'label' => Yii::t('app','Comissao '.$model->tipoAutoria),
                                                'value'=>$model->comissao,
                                            ], 
                                            [
                                                'label' => Yii::t('app','Comissao '.$model->tipoAutoria.' Total'),
                                                'value' =>Yii::$app->formatter->asCurrency($model->comissaoTotal),  
                                            ] ,                                            
                                        ]
                            ]);                                            
                    },                    
                ],
                [
                    'attribute' => Yii::t('app', 'Vendas'),
                    'format' => 'raw',
                    'value' => function ($model) {                                                  
                            return DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [                                                        
                                        [
                                            'label' => Yii::t('app','Data'),
                                            'value'=>$model->dataVenda,
                                        ],                               
                                        [
                                            'label' => Yii::t('app','Total'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->totalVendas),
                                        ],                                  
                                        [
                                            'label' => Yii::t('app','Quantidade'),
                                            'value'=>$model->qtVendas,
                                        ],                           
                                        [
                                            'label' => Yii::t('app','Comissao '.$model->tipoAutoria),
                                            'value'=>Yii::$app->formatter->asCurrency($model->comissaoVendas),
                                        ],                                                                      
                                    ]                                    
                        ]);
                                            
                    },
                ],		
                [
                    'attribute' => Yii::t('app', 'Pagamento'),
                    'format' => 'raw',
                    'value' => function ($model) {                                                  
                            return DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [                                                        
                                        [
                                            'label' => Yii::t('app','Data'),
                                            'value'=>$model->dataPagamento,
                                        ],                               
                                        [
                                            'label' => Yii::t('app','Pago'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->pagamentosAnteriores),
                                        ],                                  
                                        [
                                            'label' => Yii::t('app','Divida'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->comissaoVendas-$model->pagamentosAnteriores),
                                        ],                           
                                        [
                                            'label' => Yii::t('app','Por Liquidar'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->comissaoTotal-$model->pagamentosAnteriores),
                                        ],                                                                      
                                        [
                                            'label' => Yii::t('app','recibo'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->pagamento),
                                        ],                                                                      
                                    ]                                    
                        ]);
                                            
                    },
                ]
        ],
    ]); ?>
</div>
