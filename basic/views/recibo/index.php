<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modelsReciboSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Recibos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-index">
    <?php Pjax::begin(); echo GridView::widget([        
        'id' => 'grid-recibo',
        'resizableColumns'=>true,
        'persistResize'=>true,
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,               
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Create Recibo'), ['create'], ['class' => 'btn btn-success']).' '.
                      Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-info']),
            'after' =>false,
        ],        
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            [   'class' => 'kartik\grid\SerialColumn'],
            [   'class' => '\kartik\grid\ExpandRowColumn',                
                'expandTitle'=>Yii::t('app','Expande'),
                'collapseTitle'=>Yii::t('app','Colapsa'),
                'collapseAllTitle' => Yii::t('app','Colapsa todos'),
                'expandAllTitle' => Yii::t('app','Expande todos'),                
                'expandIcon'=>'<span class="glyphicon glyphicon-plus-sign"></span>',
                'collapseIcon'=>'<span class="glyphicon glyphicon-minus-sign"></span>',
                'value' => function ($model, $key, $index, $column) {
                                return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => Url::to(['recibopublicacao/index']),
                'detailRowCssClass' => GridView::TYPE_DANGER,
                'pageSummary' => false,
            ],
            'id',
            'nome',                           
            'data',
            ['class'=>  '\kartik\grid\DataColumn',
                'attribute'=>'valor',
                //'format'=>['decimal',2],
                'value' => function($model,$key,$index,$column) {
                    return Yii::$app->formatter->asCurrency($model->valor);
                },
                'hAlign'=>'right',
                ],            
            ['class' => '\kartik\grid\ActionColumn',
                'hiddenFromExport'=>true
            ],        
        ],
    ]); Pjax::end(); ?>

</div>