<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use \yii\data\ActiveDataProvider;
use \kartik\detail\DetailView;
use app\components\My;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Recibo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recibo-form">

    <?php $form = ActiveForm::begin([
        'id' => 'formRecibo',
     
     //   'validationUrl' => ['/publicacaoautor/validate'],//,'idPublicacao'=>$model->id],
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'idAutor')->widget(\app\components\MyAutoComplete::classname(), 
        [               
            'ajaxUrl'       => '/autor/autocomplete' ,
            'options'       => ['class' => 'form-control',
                                'readonly' => ! $model->isNewRecord,],            
            'clientOptions' => [
                'name'  => 'PublicacaoAutor[idAutor]',
                'value' => $model->idAutor,
                ]
            ]) ?>
    
    <?= $form->field($model, 'data')->widget(\yii\jui\DatePicker::classname(), [
        'options'       => ['class' => 'form-control'],
        'language' => 'pt',
        'dateFormat' => 'dd-MM-yyyy',        
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    
    <?php 
    $detalhes = $model->getDetalhes();
    $dataProvider = new activeDataProvider([
            'query' => $detalhes,
            'pagination' => [
                'pageSize' => 9999,
            ],
        ]); 
    if( $detalhes!=null ) { 
    ?>   
    <?php Pjax::begin(); echo GridView::widget([        
        'id' => 'grid-editRecibo',
        'resizableColumns'=>true,
        'persistResize'=>true,
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,               
        'panel' => [
            'heading'=>'<h3 class="panel-title">'.
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>  ', ['recibo/update','id'=>$model->id]).
                            '<i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).
                        ' </h3>',            
            'type'=>'info',
            'before'=>false,
            'after'=>false,
            'footer'=>false,
        ],
        
        'dataProvider' => $dataProvider,
        'showFooter'=>TRUE,
        'footerRowOptions'=>['style'=>'border-top-style: ridge;font-weight:bold;background-color:#FFFFCC;'],

        'columns' => [
//            [   'class' => 'kartik\grid\SerialColumn'],            
            [   'class' => '\kartik\grid\ExpandRowColumn',                
                    'expandTitle'=>Yii::t('app','Expande'),
                'collapseTitle'=>Yii::t('app','Colapsa'),
                'collapseAllTitle' => Yii::t('app','Colapsa todos'),
                'expandAllTitle' => Yii::t('app','Expande todos'),                
                'expandIcon'=>'<span class="glyphicon glyphicon-plus-sign"></span>',
                'collapseIcon'=>'<span class="glyphicon glyphicon-minus-sign"></span>',
                    'value' => function ($model, $key, $index, $column) {
                                    return GridView::ROW_COLLAPSED;
                    },
                    'detail' => function ($model, $key, $index, $column) {
                            return DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [                                                                                                              
                                        [
                                            'group'=>true,
                                            'label'=>Yii::t('app','Autoria'),
                                            'rowOptions'=>['class'=>'info'],
                                        ],                                                    
                                        [
                                            'label' => Yii::t('app','Comissao '.$model->tipoAutoria),
                                            'value'=>$model->comissao,
                                        ],                                                                              
                                        [
                                            'label' => Yii::t('app','Comissao '.$model->tipoAutoria.' Total'),
                                            'value'=>  Yii::$app->formatter->asCurrency($model->comissaoTotal),
                                        ],                                                                                                                          
                                        [
                                            'group'=>true,
                                            'label'=>Yii::t('app','Publicacao'),
                                            'rowOptions'=>['class'=>'info'],                                            
                                        ],

                                        'tiragem',
                                        [
                                            'label' => Yii::t('app','P Venda'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->publicacao->pVenda),
                                        ],                                            
                                        [
                                            'label' => Yii::t('app','Valor Total'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->valorTotal),
                                        ],                                            
                                        [
                                            'group'=>true,
                                            'label'=>Yii::t('app','Vendas'),
                                            'rowOptions'=>['class'=>'info'],
                                            //'groupOptions'=>['class'=>'text-center']
                                        ],                                                    
                                        [
                                            'label' => Yii::t('app','Data'),
                                            'value'=>  My::sqlData($model->dataVenda),
                                        ],                               
                                        [
                                            'label' => Yii::t('app','Total'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->totalVendas),
                                        ],                                  
                                        [
                                            'label' => Yii::t('app','Quantidade'),
                                            'value'=>$model->qtVendas,
                                        ],                           
                                        [
                                            'label' => Yii::t('app','Comissao '.$model->tipoAutoria),
                                            'value'=>Yii::$app->formatter->asCurrency($model->comissaoVendas),
                                        ],                                                                      
                                        [
                                            'group'=>true,
                                            'label'=>Yii::t('app','Pagamento'),
                                            'rowOptions'=>['class'=>'info'],
                                            //'groupOptions'=>['class'=>'text-center']
                                        ],                                                    
                                        [
                                            'label' => Yii::t('app','Data'),
                                            'value'=>My::sqlData($model->dataPagamento),
                                        ],                               
                                        [
                                            'label' => Yii::t('app','Pago'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->pagamentosAnteriores),
                                        ],                                  
                                        [
                                            'label' => Yii::t('app','Divida'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->comissaoVendas-$model->pagamentosAnteriores),
                                        ],                           
                                        [
                                            'label' => Yii::t('app','Por Liquidar'),
                                            'value'=>Yii::$app->formatter->asCurrency($model->comissaoTotal-$model->pagamentosAnteriores),
                                        ],                                                                                                                          
                                    ]                                    
                                ]);
                },


                'detailRowCssClass' => GridView::TYPE_DEFAULT,
                'pageSummary' => false,
            ],  
            'tipoAutoria',
            [
                'footer'=>Yii::t('app','Total'),            
                'format' => 'raw',
                'label' => Yii::t('app','Publicacao'),
                'value' => function ($model, $key, $index, $column) {
                                return $model->publicacao->ISBN.'<br>'.
                                    $model->publicacao->descricaoHTML;
                },
            ],                   
            [
                'label' => Yii::t('app','Divida'),
                'hAlign'=>'right', 
                'vAlign'=>'middle',                                        
                'value'=>function ($model, $key, $index, $column) {
                                return Yii::$app->formatter->asCurrency($model->comissaoVendas-$model->pagamentosAnteriores);
                },
                'footer'=> Yii::$app->formatter->asCurrency(
                            app\components\My::pageTotal($dataProvider->models,'comissaoVendas') -
                            app\components\My::pageTotal($dataProvider->models,'pagamentosAnteriores')),                                
            ], 
            [
                'class' => 'kartik\grid\EditableColumn',                    

                'attribute' => 'pagamento',                                        
                'value'     => function ($model) {                      
                    return Yii::$app->formatter->asCurrency($model->pagamento);
                },
                'readonly'=>function($model, $key, $index, $widget) {
                    return false;
                },
                'hAlign'=>'right', 
                'vAlign'=>'middle',                                        
                'footer'=> Yii::$app->formatter->asCurrency(app\components\My::pageTotal($dataProvider->models,'pagamento')),                
                'editableOptions' => function ($model, $key, $index) {
                    return [                                                         
                        'asPopover' => true,
                        'header'=>'Pagamento',
                        'placement'=>  kartik\popover\PopoverX::ALIGN_TOP_RIGHT ,
                        'type'      => kartik\popover\PopoverX::TYPE_PRIMARY,                        
                        'afterInput'=> function ($form, $widget) {
                                /* @var $form yii\widgets\ActiveForm */
                                echo $form->field($widget->model, 'porLiquidar',['template'=>'{input}',])->hiddenInput();
                        },
                        'submitButton'=> ['label'=>Yii::t('app','Enviar'),
                                        'icon'=>'<i class="ui-icon ui-icon-arrowthickstop-1-s"></i>'],
                        'resetButton'=> ['label'=>Yii::t('app','Voltar'),
                                        'icon'=>'<i class="ui-icon ui-icon-arrowreturnthick-1-w"></i>'],                        
                    ];
                },
            ],

        ],
    ]); 
}?>
    
</div>

