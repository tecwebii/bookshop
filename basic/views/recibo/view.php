<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Recibo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recibos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view'],
        'attributes' => [
            'id',
            [
                'label' => Yii::t('app','Autor/Coordenador'),
                'value' => $model->autor->getNome(),
            ],
            'data',
        ],
    ]) ?>
    
    <?= $this->render('/recibopublicacao/index', [
        'model'=>$model->getRecibopublicacoes(),        
    ]);
    ?>

</div>
