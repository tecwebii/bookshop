<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Yii::t('app','About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <p>Somos um grupo de estudantes de Sistemas de Informacao, web e multimedia da Universidade Europeia.</p>
    <p></p>
    <br>
    <br>

    <div class="img">
      <a target="_blank" href="/img/ines.jpg">
        <img src="/img/ines.jpg" alt="" width="200" height="200">
      </a>
      <div class="desc"></div>
       <p>Ines Pires</p>
      <p><a href= "https://pt.linkedin.com/pub/inês-pires-pinho/89/624/795">Linkedin</a></p>

    </div>
    <div class="img">
      <a target="_blank" href="/img/Lidiane.jpg">
        <img src="/img/Lidiane.jpg" alt="" width="200" height="200">
      </a>
      <div class="desc"></div>
       <p>Lidiane Miranda</p>
      <p><a href= "https://www.linkedin.com/in/lidianemiranda">Linkedin</a></p>

    </div>
    <div class="img">
      <a target="_blank" href="/img/taz.jpg">
        <img src="/img/taz.jpg" alt="" width="200" height="200">
      </a>
      <div class="desc"></div>
      <p>Taslima Abedin</p>
      <p><a href= "https://www.linkedin.com/pub/taslima-abedin/80/574/612">Linkedin</a></p>

    </div>
    <div class="img">
      <a target="_blank" href="/img/vitor.jpg">
        <img src="/img/vitor.jpg" alt="" width="200" height="200">
      </a>
      <div class="desc"></div>
      <p>Vitor Oliveira</p>
      <p><a href= "https://pt.linkedin.com/pub/vitor-oliveira/69/122/592">Linkedin</a></p>

    </div>
    
</div>  
<div id="powered">
<?= Yii::powered(); ?>
</div>
