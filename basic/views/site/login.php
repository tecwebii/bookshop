<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = Yii::t('app','Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'Please fill out the following fields to login:') ?></p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal' ],
        'encodeErrorSummary'=>true,
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe', [
        'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
    ])->checkbox() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>

<?php
$a='$("body").attr("oncontextmenu","return false;");';
$script = <<<EOD
$("#login-form").submit(function(ev){                
        var postData = $(this).serialize(); 
        var encrypted = CryptoJS.AES.encrypt(
                            JSON.stringify(postData), 
                            $("input[name=_csrf]").val(), 
                            {format: CryptoJSAesJson}
                        ).toString();                
        var data = [{json:encrypted,
                    username:$("#loginform-username").val(),
                    _csrf:$("input[name=_csrf]").val()}][0];       
        var form=$(this);                
        $.ajax({
            url: form.attr('action'),
            type: (form.attr('method')!=''?form.attr('method'):'post').toUpperCase(),
            data: data,            
            contentType: ($(this).find('input[type=file]').length>0 ? false : 'application/x-www-form-urlencoded; charset=UTF-8'),
            cache: false,
            complete:function(jqXHR, textStatus ){
                var redir=jqXHR.getResponseHeader('X-Redirect');
                if (redir!=null) {
                    jqXHR.abort();
                    window.stop();
                    return false;
                }                                        
            },
            success: function (data, textStatus, jqXHR ){
                //loadData(data,form.attr('action'),panel);                    
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
        return false;
});
EOD;
//$this->registerJs($script); //,['depends'=>'yii\base\Security']);
?>
