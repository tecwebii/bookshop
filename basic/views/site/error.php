<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>
    <?php if( isset(Yii::$app->user->identity) &&
                Yii::$app->user->identity->isAdmin() && $exception->getMessage()!=$message ) {?>
    <div class="alert alert-danger">
        <?= nl2br(Html::encode($exception->getMessage())) ?>
    </div>
    <?php } ?>
    <?php if (app\components\My::is_subclass_of($exception, "yii\web\HttpException")) { ?>    
    <p>
        <?=Yii::t('app','The above error occurred while the Web server was processing your request.');?>
    </p>
    <p>
        <?=Yii::t('app','Please contact us if you think this is a server error. Thank you.');?>
    </p>
    <?php } ?>
</div>
