<?php

use yii\helpers\Html;
use app\components\My;
use fruppel\googlecharts\GoogleCharts;


/* @var $this yii\web\View */
/* @var $model app\models\Publicacao */

$arr = $model->chart->all(); //'v.data','v.totalVendas','t.total'])
$data= [['Mes', 'Vendas',['type' => 'string', 'role' => 'tooltip']]];
if(count($arr)==0 && ! isset($model->dadosProducao) ){
    echo 'não existem vendas';
    return;
}
$maxVendas=isset($model->dadosProducao) ? $model->dadosProducao->tiragem * $model->pVenda : 0;
$totalVendas=0;    
$tiragem=0;
$qtVendas=0;
foreach($arr as  $k=>$v){
    $tiragem=doubleval($v['tiragem']);
    $qtVendas=doubleval($v['qtVendas']);
    $maxVendas=doubleval($v['total']);
    $totalVendas=doubleval($v['totalVendas']);
    $d= explode('-', My::sqlData($v['data']));
    $mes= new yii\web\JsExpression( "new Date($d[0],$d[1],$d[2])");
    $toolTip = Yii::$app->formatter->asDate($v['data'],'php:d-m-Y')." \n";
    $perc = Yii::$app->formatter->asDecimal($totalVendas/$maxVendas*100,1). '%';
    $toolTip.= Yii::$app->formatter->asCurrency($v['totalVendas'])."\n $perc do total vendas";
    $data[] = [$mes,  $totalVendas,$toolTip];
}
$maxVendas = $totalVendas!=0 ? $totalVendas : $maxVendas;?>
<div class="chartPubAreabar" style="width:58%;height:98%;display:inline-block">
<?= GoogleCharts::widget([
        'id' => 'AreaChart'.$model->ISBN,
        'visualization' => 'AreaChart',
        'responsive'   => true,
        'options' => [            
            'title'         => 'Vendas '.$model->getDescricao(' - '),
            'pointsVisible' => true,
            'tooltip'       => [ 'isHtml'      => true ],
            'hAxis'         => [
                'title'             => 'Mes',
                'format'            => 'MMM, y',
                'titleTextStyle'    => ['color' => '#333'],
            ],
            'vAxis'         => [
                'format'    => 'currency',  
                'minValue'  => 0,
                'maxValue'  => 500,
                'ticks'     => [ 0, floor($maxVendas/4*1) ,floor($maxVendas/2), floor($maxVendas/4*3),$maxVendas],
                'gridlines' => [ 'count'    => 3 ],
            ],            
        ],
        'dataArray' =>  $data 
    ]);
?>
</div>
<div class="charPubPie" style="width:40%;height:98%;display:inline-block">
<?php echo GoogleCharts::widget([
	'id' => 'PieChart'.$model->ISBN,
	'visualization' => 'PieChart',
        'options' => [
            'title' => '',
            'is3D' => true,
        ],
        'responsive' => true,
	'data' => [
            'cols' => [
                [
                    'id' => 'topping',
                    'label' => 'Topping',
                    'type' => 'string'
                ],
                [
                    'id' => 'slices',
                    'label' => 'Slices',
                    'type' => 'number'
                ]
            ],
            'rows' => [
                            [
                                'c' => [
                                    ['v' => 'Vendidos'],
                                    ['v' => $qtVendas]
                                ],
                            ],
                            [
                                'c' => [
                                    ['v' => 'Por Vender'],
                                    ['v' => $tiragem-$qtVendas]
                                ]
                            ],                
                    ]
        ],
    
]); ?>
</div>