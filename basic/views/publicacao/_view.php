<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\jui\Accordion;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveRecord;
use app\components\My;

/* @var $this yii\web\View */
/* @var $model app\models\Publicacao */



$custosProvider=new activeDataProvider([
        
            'query' => $model->getCusto(),
            'pagination' => [
                'pageSize' => 9999,
            ],
]);
echo Accordion::widget([
        'id'    => 'publicacao_'.$model->id.'_detalhe',
        'items' => [
            [
                'header' => Yii::t('app', 'Evolução Vendas'),                                                
                'content' => $this->render('_chart',['model'=>$model]), //'<iframe src="/publicacao/chart/'.$model->id.'"style="border:0;width:100%;height:250px"></iframe>',

            ],
            [
                'header' => Yii::t('app', 'Publicacao'),                                
                'format'=>['decimal', 2],
                'content' => DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'id',
                                    'tipoPublicacao',
                                    'ISBN',
                                    [
                                        'format' => 'raw',
                                        'attribute'  => 'descricaoHTML',
                                    ],
                                    'dataEdicao',
                                    [   'class'     => '\kartik\grid\DataColumn',
                                        'attribute' =>'pVenda',
                                        'value' => Yii::$app->formatter->asCurrency($model->pVenda),
                                    ],
                                    'taxaIva',
                                    [                      // the owner name of the model
                                        'label' => Yii::t('app','Editora'),
                                        'value' => isset($model->editora) 
                                                ? $model->editora->id.' '.$model->editora->nome
                                                : '',
                                    ],                                      
                                ]
                    ])
            ],
            [
                'header' => Yii::t('app', 'Dados Producao Livro'),                
                'content' => isset($model->dadosProducao) 
                        ? $this->render('/dadosproducaolivro/view',['model'=>$model->dadosProducao])                
                        : '',
            ],
            [   'header' => Yii::t('app','Autores'),
                'content' => GridView::widget([                        
                        'layout'=>"{items}",                        
                        'dataProvider' =>  new activeDataProvider([
                                'query' => $model->getAutores(),
                                'pagination' => [
                                    'pageSize' => 9999,
                                ],
                            ]),

                        'columns' => [        
                            [
                                'attribute' => Yii::t('app','Autor'),
                                'format' => 'raw',
                                'value' => function ($model) {                      
                                        return '<div>'.$model->autor->nome.'</div>';
                                },
                            ],
                            [
                                'attribute'=>Yii::t('app','Comissao Autor'),   
                                'format' => 'raw',
                                'value' => function ($model) {                      
                                        return $model->comissao;
                                },
                                'contentOptions'=>['style'=>'text-align:right'],
                                'headerOptions'=>['style'=>'text-align:right'],
                            ],
                            [
                                'attribute'=>'contratoHTML',
                                'format'=>'raw',
                            ],                     
                        ],
                    ])
            ],
            [   'header' => Yii::t('app','Coordenadores'),
                'content' => GridView::widget([
                        'layout'=>"{items}",                       
                        'dataProvider' =>  new activeDataProvider([
                                'query' => $model->getCoordenadores(),
                                'pagination' => [
                                    'pageSize' => 9999,
                                ],
                            ]),

                        'columns' => [                                    
                            [
                                'attribute' => Yii::t('app','Coordenador'),
                                'format' => 'raw',
                                'value' => function ($model) {                      
                                        return '<div>'.$model->autor->nome.'</div>';
                                },
                            ],
                            [
                                'attribute'=>Yii::t('app','Comissao Coordenador'),   
                                'format' => 'raw',
                                'value' => function ($model) {                      
                                        return $model->comissao;
                                },                            
                                'format'=>['decimal'],                                
                                'contentOptions'=>['style'=>'text-align:right'],                                
                                'headerOptions'=>['style'=>'text-align:right'],
                            ],
                            [
                                'attribute'=>'contratoHTML',
                                'format'=>'raw',
                            ],
                            
                        ],
                    ])
            ],
            [   'header' => Yii::t('app','Custos'),
                'content' => GridView::widget([                        
                        'id' => 'grelha-publicacao-custo',
                        'layout'=>"{items}",
                        'dataProvider' =>  $custosProvider,
                        'showFooter'=>true,
                        'footerRowOptions'=>['style'=>'border-top-style: ridge;font-weight:bold;background-color:#FFFFCC;'],
                        'columns' => [
                            ['class' => 'yii\grid\DataColumn',
                                'attribute' => 'rubrica',                                
                                'footer'=>Yii::t('app','Total'),
                                
                            ],
                            [ 'class' => 'yii\grid\DataColumn',                                
                                'contentOptions'=>['style'=>'text-align:right'],
                                'headerOptions'=>['style'=>'text-align:right'],
                                'footerOptions'=>['style'=>'text-align:right'],
                                'attribute' => 'valor',                         
                                'value' => function($model){
                                        return Yii::$app->formatter->asCurrency($model->valor);                                                
                                },
                                'footer' => Yii::$app->formatter->asCurrency(My::pageTotal($custosProvider->models,'valor')),                                
                            ],
                        ],
                    
                    ])
            ],
        ],
            
        'options' => ['tag' => 'div'],        
        'itemOptions' => ['tag' => 'div'],
        'headerOptions' => ['tag' => 'h3'],
        'clientOptions' => ['collapsible' => true,
                            'heightStyle' => 'content',
                            'animated'=>'bounceslide',],
    ]);

    ?>


