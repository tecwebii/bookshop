<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\EvolucaoVendasImporta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicacao-upload-form">

    <?php $form = ActiveForm::begin([
        'id' => 'PublicacaoImporta',
        'options' => ['enctype' => 'multipart/form-data'],
        'validationUrl' => ['/publicacao/validate'],
        'method'=>'post',
    ]); ?>
    <?= $form->field($model, 'dataImportacao',['options'=>["style"=>"display:none"]])->hiddenInput() ?>
    <?= $form->field($model, 'random',['options'=>["style"=>"display:none"]])->hiddenInput() ?>
    <?= $form->field($model, 'ficheiro')->fileInput(['maxlength' => 255]) ?>   

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Importa'), ['btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <div id="ajaxWait" style="display:none;">
        <div id="bodyCover"></div>
        <img src="/img/ajax loader.gif">
    </div>
</div>

<?php
$script = <<<EOD
$("form").submit( function(event) {        
        $("#ajaxWait").show("slow");
});    
EOD;
$this->registerJs($script);        
?>