<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\Accordion;
/* Inicializa GoogleCharts */
use fruppel\googlecharts\GoogleCharts;

echo GoogleCharts::widget([
        'id'=>'initPieChart',
	'visualization' => 'PieChart',                
        'data' =>[ 'cols'=>[], 'rows'=>[]],  
]); 
$this->registerJs(' 
    $(document).ready(function(){
        $("#googlechart_initPieChart").hide();
    });
');  
/* termina inicializacao GoogleCharts */

/* @var $this yii\web\View */
/* @var $searchModel app\models\PublicacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

Accordion::begin();
Accordion::end();
$this->title = Yii::t('app', 'Publicacaos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-index">
    <?php Pjax::begin(); echo GridView::widget([
        'id' => 'grid-publicacao',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'resizableColumns'=>true,
        'persistResize'=>true,
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,               
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Create Publicacao'), ['create'], ['class' => 'btn btn-success']).' '.
                      Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Importar Publicações'), ['upload'], ['class' => 'btn btn-success']).' '.
                      Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-info'])  ,
            'after' =>false,
        ],
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [   'class' => '\kartik\grid\ExpandRowColumn',                
                'expandTitle'=>Yii::t('app','Expande'),
                'collapseTitle'=>Yii::t('app','Colapsa'),
                'collapseAllTitle' => Yii::t('app','Colapsa todos'),
                'expandAllTitle' => Yii::t('app','Expande todos'),                
                'expandIcon'=>'<span class="glyphicon glyphicon-plus-sign"></span>',
                'collapseIcon'=>'<span class="glyphicon glyphicon-minus-sign"></span>',
                'value' => function ($model, $key, $index, $column) {
                               return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => \yii\helpers\Url::to(['publicacao/detalhes']),
                'detailRowCssClass' => GridView::TYPE_DEFAULT,                
                'pageSummary' => false,
            ],            
            'ISBN',            
            [
                'format' => 'raw',
                'attribute' => 'descricaoHTML',                           
            ],
            'tipoPublicacao',
            ['class' => '\kartik\grid\ActionColumn',
                'hiddenFromExport'=>true],
        ],
        
    ]); Pjax::end(); ?>

</div>
