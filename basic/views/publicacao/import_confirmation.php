<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\BaseInflector;

/* @var $this yii\web\View */
/* @var $array array */
/* @var $columns array */
/* @var $file string */
/* @var $data string */


$this->title = Yii::t('app', 'Confirmação Publicações');
$this->params['breadcrumbs'][] = $this->title;

$importUrl = yii\helpers\Url::to(['/publicacao/import',[''=>'',''=>'']]);
$saveImportUrl = yii\helpers\Url::to(['/publicacao/saveimport',[''=>'',''=>'']]);
?>
<div class="evolucao-vendas-index">

    <h1><?= Html::encode($this->title) ?></h1>    
           
    <?=Html::beginForm($importUrl,'post');?>
        <p>
    <?=Html::submitInput( Yii::t('app', 'Importa {ficheiro}',['ficheiro'=>  BaseInflector::titleize($file)]), ['class' => 'btn btn-success']) ?>            
        </p>
    <?=Html::hiddenInput('file', $file);?>
    <?=Html::hiddenInput('date', $data);?>
    <?php
        $cols = array_merge([['class' => 'yii\grid\SerialColumn']],$columns );
        echo GridView::widget([
                    'dataProvider' => new ArrayDataProvider([
                                                'allModels' => $array,    
                                                'pagination' => [
                                                    'pageSize' => 9999,
                                                ]
                                    ]),        
                    'resizableColumns'=>true,    
                    'persistResize'=>true,
                    'rowOptions'   => function ($model, $index, $widget, $grid) {
                                        return ['class'=>'importRecno'];
                        },

                    'columns' => $cols,

                ]); 
        echo Html::endForm();
        ?>            
    <div id="ajaxWait" style="display:none;">
        <div id="bodyCover"></div>
        <img src="/img/ajax loader.gif">
    </div>
</div>

<?php
$script = <<<EOD
$("form").submit( function(event) {        
        $("#ajaxWait").show("slow");
});            
$.each($('.importRecno').find('input[type=checkbox][disabled]').closest('.importRecno').find('td'),function( idx, me ){
        var bgCor = getBgColorHex($(me).css("color"));
        var newBgCor=LightenDarkenColor(bgCor, 60);
        $(me).css("color",newBgCor);        
});

$("input[type=submit]").attr("disabled", $('input[name="selection[]"]:not(:disabled)').length==0 ? "" : undefined );   
EOD;
$this->registerJs($script);

$script = <<<EOD
$("input[type=checkbox]").change( function() {
    var form = $(this).closest("form");
    var postData = $(form).serialize();                        
    $.ajax({
        url: '$saveImportUrl',
        type: (form.attr('method')!=''?form.attr('method'):'post').toUpperCase(),
        data: postData,        
        contentType: ($(this).find('input[type=file]').length>0 ? false : 'application/x-www-form-urlencoded; charset=UTF-8'),
        cache: false,
        processData:false,
        complete:function(jqXHR, textStatus ){
            var a = this;
        },
        success: function (data, textStatus, jqXHR ){
            var a = this;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var a = this;
        }
    });
});
EOD;
//$this->registerJs($script);
?>


