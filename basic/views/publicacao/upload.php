<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Publicacao */

$this->title = Yii::t('app', 'Upload Publicações');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Publicacaos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-import">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_upload', [
        'model' => $model,
    ]) ?>

</div>
