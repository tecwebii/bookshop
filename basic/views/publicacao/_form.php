<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\jui\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Publicacao */
/* @var $form yii\widgets\ActiveForm */

?>
    <?= Tabs::widget([
        'options' => ['tag' => 'div'],
        'itemOptions' => ['tag' => 'div'],
        'headerOptions' => ['class' => 'panelLink'],
        'clientOptions' => ['collapsible' => false],
        'items' => [
            [   'label'     =>'Publicação',
                'content'   =>$this->render('_form_publicacao',['model'=>$model]),
                'options'   => ['id' => 'tab-publicacao', 
                             /*   'data-url'  =>yii\helpers\Url::toRoute(
                                        (isset($model->isNewRecord)
                                            ?  ['publicacao/create']
                                            :  ['publicacao/insert', 'id' => $model->id])), */
                                ],                
                ],
            [   'label'     =>'Dados produção',
                'content'   =>'',                                
                'options'   => ['id' => 'tab-dadosproducao',
                                'data-url'  =>yii\helpers\Url::toRoute(
                                        ['dadosproducaolivro/update', 'id' => $model->id]), 
                                ],                
                ],
            [   'label'     =>'Autores',
                'content'   =>'',                

                'options'   => ['id' => 'tab-autores',
                                'data-url'  =>yii\helpers\Url::toRoute(
                                        ['publicacaoautor/index', 'idPublicacao' => $model->id, 'tipoAutoria'=> 'Autor']),
                                ]                
                ],
            [   'label'     =>'Coordenadores',
                'content'   =>'',
                'options'   => ['id' => 'tab-coordenadores',
                                'data-url'  =>yii\helpers\Url::toRoute(
                                        ['publicacaoautor/index', 'idPublicacao' => $model->id, 'tipoAutoria'=> 'Coordenador']),
                            ],                
                ],
            [   'label'     =>'Custos',
                'content'   =>'',
                'options'   => ['id' => 'tab-custos',
                                'data-url'  =>yii\helpers\Url::toRoute(
                                        ['publicacaocusto/index', 'id' => $model->id]),
                            ],                
                ],
        ],                
        'clientEvents' => [                                                         
            'beforeActivate' => new JsExpression("function( event, ui ){                
                var url=ui.newPanel.data('url') == undefined ? '' : ui.newPanel.data('url');       
                var loaded=ui.newPanel.data('loaded') == undefined ? '' : ui.newPanel.data('loaded');                      
                if(loaded=='')
                    loadIntoPanel(ui.newPanel,url,'GET',true)
                else
                    addHistory(ui.newPanel,loaded);
            }"),
        ]
        
    ]);?>  

<?php
$disableTabs = !$model->isNewRecord ? ""
                    : "$('.ui-tabs').tabs( 'option', 'disabled',[1,2,3,4]);" ;
$script = <<<EOD
$disableTabs        

EOD;
$this->registerJs($disableTabs);
?>

