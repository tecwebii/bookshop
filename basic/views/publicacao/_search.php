<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicacao-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tipoPublicacao') ?>

    <?= $form->field($model, 'ISBN') ?>

    <?= $form->field($model, 'titulo') ?>

    <?= $form->field($model, 'subTitulo') ?>

    <?php // echo $form->field($model, 'dataEdicao') ?>

    <?php // echo $form->field($model, 'pVenda') ?>

    <?php // echo $form->field($model, 'taxaIva') ?>

    <?php // echo $form->field($model, 'idEditora') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
