<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Publicacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicacao-form">

    <?php $form = ActiveForm::begin([
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnSubmit'=>true,
                ]); ?>
 

    <?= $form->field($model, 'tipoPublicacao')
                    ->dropDownList([ 'Livro' => 'Livro', 'Outro' => 'Outro', ], 
                    array_merge(['prompt'   => ''], 
                        $model->isNewRecord ? [] : ['disabled'=>'true']  ))  ?>

    <?= $form->field($model, 'ISBN')->textInput(['maxlength' => 13]) ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'subTitulo')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'dataEdicao')->widget(\yii\jui\DatePicker::classname(), [
        'options'       => ['class' => 'form-control'],
        'language' => 'pt',
        'dateFormat' => 'dd-MM-yyyy',        
    ]) ?>

    <?= $form->field($model, 'pVenda')->widget(kartik\money\MaskMoney::classname(), [
                'pluginOptions' => [
                    'prefix' => '€ ',
                    'suffix' => '',
                    'allowNegative' => false
                ]
            ]);
    ?>
    <?= $form->field($model, 'taxaIva')->textInput() ?>
    
    <?php $acUrl = Url::to('editora/autocomplete',true); ?>
    
    <?= $form->field($model, 'idEditora')->widget(\app\components\MyAutoComplete::classname(), 
        [   'ajaxUrl'       => $acUrl ,
            'options'       => ['class' => 'form-control'],            
            'clientOptions' => [
                'name'  => 'Publicacao[idEditora]',
                'value' => $model->idEditora,
                ]
            ]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
