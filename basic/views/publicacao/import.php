<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\BaseInflector;

/* @var $this yii\web\View */
/* @var $result array */
/* @var $columns array */
/* @var $file string */
/* @var $data string */


$this->title = Yii::t('app', 'Importação Publicações');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-index">

    <h1><?= Html::encode($this->title) ?></h1>    
                
    <?php
        $cols = array_merge([['class' => 'yii\grid\SerialColumn']],$columns );
        echo GridView::widget([
                    'dataProvider' => new ArrayDataProvider([
                                                'allModels' => $result,  
                                                'pagination' => [
                                                    'pageSize' => 9999,
                                                ]                                 
                                    ]), 
                    'resizableColumns'=>true,
                    'persistResize'=>true,
                    'columns' => $columns,

                ]); 
        echo Html::endForm();
        ?>
    
</div>

<?php
$script = <<<EOD
$.each($('.importRecno').find('input[type=checkbox][disabled]').closest('.importRecno').find('td'),function( idx, me ){
        var bgCor = getBgColorHex($(me).css("color"));
        var newBgCor=LightenDarkenColor(bgCor, 60);
        $(me).css("color",newBgCor);        
});

EOD;
$this->registerJs($script);
?>


