<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CodigoPostalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="codigo-postal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_distrito') ?>

    <?= $form->field($model, 'id_concelho') ?>

    <?= $form->field($model, 'cod_localidade') ?>

    <?= $form->field($model, 'localidade') ?>

    <?php // echo $form->field($model, 'arteria_cod') ?>

    <?php // echo $form->field($model, 'arteria_tipo') ?>

    <?php // echo $form->field($model, 'pri_prep') ?>

    <?php // echo $form->field($model, 'arteria_titulo') ?>

    <?php // echo $form->field($model, 'seg_prep') ?>

    <?php // echo $form->field($model, 'arteria_designacao') ?>

    <?php // echo $form->field($model, 'morada_local') ?>

    <?php // echo $form->field($model, 'troco') ?>

    <?php // echo $form->field($model, 'porta') ?>

    <?php // echo $form->field($model, 'cliente') ?>

    <?php // echo $form->field($model, 'cp4') ?>

    <?php // echo $form->field($model, 'cp3') ?>

    <?php // echo $form->field($model, 'cp_designacao') ?>

    <?php // echo $form->field($model, 'morada') ?>

    <?php // echo $form->field($model, 'codpostal') ?>

    <?php // echo $form->field($model, 'distrito') ?>

    <?php // echo $form->field($model, 'concelho') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
