<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CodigoPostal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="codigo-postal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_distrito')->textInput(['maxlength' => 2]) ?>

    <?= $form->field($model, 'id_concelho')->textInput(['maxlength' => 2]) ?>

    <?= $form->field($model, 'cod_localidade')->textInput(['maxlength' => 5]) ?>

    <?= $form->field($model, 'localidade')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'arteria_cod')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'arteria_tipo')->textInput(['maxlength' => 25]) ?>

    <?= $form->field($model, 'pri_prep')->textInput(['maxlength' => 5]) ?>

    <?= $form->field($model, 'arteria_titulo')->textInput(['maxlength' => 25]) ?>

    <?= $form->field($model, 'seg_prep')->textInput(['maxlength' => 5]) ?>

    <?= $form->field($model, 'arteria_designacao')->textInput(['maxlength' => 70]) ?>

    <?= $form->field($model, 'morada_local')->textInput(['maxlength' => 70]) ?>

    <?= $form->field($model, 'troco')->textInput(['maxlength' => 80]) ?>

    <?= $form->field($model, 'porta')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'cliente')->textInput(['maxlength' => 80]) ?>

    <?= $form->field($model, 'cp4')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'cp3')->textInput(['maxlength' => 3]) ?>

    <?= $form->field($model, 'cp_designacao')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'morada')->textInput(['maxlength' => 80]) ?>

    <?= $form->field($model, 'codpostal')->textInput(['maxlength' => 8]) ?>

    <?= $form->field($model, 'distrito')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'concelho')->textInput(['maxlength' => 30]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
