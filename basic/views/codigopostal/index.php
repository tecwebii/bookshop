<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CodigoPostalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Codigo Postals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="codigo-postal-index">

    <?php Pjax::begin(); echo GridView::widget([
        'id' => 'grid-codigopostal',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'resizableColumns'=>true,
        'persistResize'=>true,
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,               
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Create Codigo Postal'), ['create'], ['class' => 'btn btn-success']).' '.
                      Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-info']),
            'after' =>false,            
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,        
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'codpostal',
            'cp_designacao',
            'distrito',
            'concelho',
            'morada',            

            ['class' => '\kartik\grid\ActionColumn',
                'hiddenFromExport'=>true
                ],
        ],
    ]); Pjax::end(); ?>

</div>
