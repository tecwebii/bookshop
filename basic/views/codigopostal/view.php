<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CodigoPostal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Codigo Postals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="codigo-postal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_distrito',
            'id_concelho',
            'cod_localidade',
            'localidade',
            'arteria_cod',
            'arteria_tipo',
            'pri_prep',
            'arteria_titulo',
            'seg_prep',
            'arteria_designacao',
            'morada_local',
            'troco',
            'porta',
            'cliente',
            'cp4',
            'cp3',
            'cp_designacao',
            'morada',
            'codpostal',
            'distrito',
            'concelho',
        ],
    ]) ?>

</div>
