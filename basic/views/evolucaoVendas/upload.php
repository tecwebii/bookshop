<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EvolucaoVendas */

$this->title = Yii::t('app', 'Upload Evolucao Vendas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Evolucao Vendas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evolucao-vendas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_upload', [
        'model' => $model,
    ]) ?>

</div>
