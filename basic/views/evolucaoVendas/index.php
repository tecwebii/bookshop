<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\EvolucaoVendas;
use app\models\Publicacao;
use yii\jui\AutoComplete;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\db\Query;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EvolucaoVendasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Evolucao Vendas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evolucao-vendas-index">
    <?php         
        $distinctISBN = EvolucaoVendas::find()
                        ->select(['value'=>'publicacao.ISBN', 'label'=>"concat(publicacao.isbn,' - ',publicacao.titulo)",'ISBN'=>'publicacao.ISBN','id'=>'publicacao.ISBN'])
                        ->innerJoin('publicacao','publicacao.isbn=evolucaovendas.isbn')
                        ->distinct()
                        ->asArray()
                        ->all();?>    
    <?php $distinctData = EvolucaoVendas::find()
                        ->select(['value'=>'data', 'value'=>'data'])
                        ->distinct()        
                        ->asArray()
                        ->all();?>
    <?php Pjax::begin(); echo GridView::widget([        
        'id' => 'grid-evolucaovendas',
        'resizableColumns'=>true,
        'persistResize'=>true,
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,               
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Importar Vendas'), ['upload'], ['class' => 'btn btn-success']).' '.
                      Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-info']),
            'after' =>false,
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],        
            [
                'attribute'=>'ISBN',
                'filter' => AutoComplete::widget([
                        'model' => $searchModel,
                        'attribute' => 'ISBN',                        
                        'options' => [ 
                            'class' => 'form-control',
                        ],
                        'clientOptions' => [
                            'source' => $distinctISBN,
                            'autoFill'=>true,
                            'minLength' =>0,
                            'select' => new JsExpression("function( event, ui ) {
                                $('evolucaovendassearch-ISBN').val(ui.item.id);
                            }"),
                        ],                                               
                    ]),
                
            ],
            [
                'attribute' => 'titulo',
                'format' => 'raw',
                'value' => function($model){
                    return $model->publicacao->descricaoHTML;
                }
            ],
            [
                'attribute'=>'data',
                'filter' => AutoComplete::widget([
                        'model' => $searchModel,
                        'attribute' => 'data',                        
                        'options' => [ 
                            'class' => 'form-control',
                        ],
                        'clientOptions' => [
                            'source' => $distinctData,
                            'autoFill'=>true,
                            'minLength' =>0,
                            'select' => new JsExpression("function( event, ui ) {
                                $('evolucaovendassearch-data').val(ui.item.id);
                            }"),
                        ],

                    ]),                
                
            ],
            [
                'attribute'=>'quantidade',            
                'hAlign'=>'right',
            ],
            ['class' => '\kartik\grid\ActionColumn',
                'hiddenFromExport'=>true,
                'template'=>'{delete}'
            ],        
        ],
    ]); Pjax::end(); ?>
            
</div>
