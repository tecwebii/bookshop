<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\BaseInflector;

/* @var $this yii\web\View */
/* @var $array array */
/* @var $columns array */
/* @var $file string */
/* @var $data string */


$this->title = Yii::t('app', 'Confirmação Evolucao Vendas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evolucao-vendas-index">

    <h1><?= Html::encode($this->title) ?></h1>    
           
    <?=Html::beginForm('/evolucaovendas/import','post');?>
        <p>
    <?=Html::submitInput( Yii::t('app', 'Importa {ficheiro}',['ficheiro'=>  BaseInflector::titleize($file)]), ['class' => 'btn btn-success']) ?>            
        </p>
    <?=Html::hiddenInput('file', $file);?>
    <?=Html::hiddenInput('date', $data);?>
    <?php
        $cols = array_merge([['class' => 'yii\grid\SerialColumn']],$columns );
        echo GridView::widget([
                    'dataProvider' => new ArrayDataProvider([
                                                'allModels' => $array,    
                                                'pagination' => [
                                                    'pageSize' => 9999,
                                                ]
                    ]),        
                    'resizableColumns'=>true,    
                    'persistResize'=>true,
                    'rowOptions'   => function ($model, $index, $widget, $grid) {
                                        return ['class'=>'importRecno'];
                        },

                    'columns' => $cols,

                ]); 
        echo Html::endForm();
        ?>
    <div id="ajaxWait" style="display:none;">
        <img src="/img/ajax loader.gif">
    </div>
</div>

<?php
$script = <<<EOD
$("form").submit( function(event) {        
        $("#ajaxWait").show("slow");
});            
$.each($('.importRecno').find('input[type=checkbox][disabled]').closest('.importRecno').find('td'),function( idx, me ){
        var bgCor = getBgColorHex($(me).css("color"));
        var newBgCor=LightenDarkenColor(bgCor, 60);
        $(me).css("color",newBgCor);        
});
        
$("input[type=submit]").attr("disabled", $('input[name="selection[]"]:not(:disabled)').length==0 ? "" : undefined );           

EOD;
$this->registerJs($script);
?>


