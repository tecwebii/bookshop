<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EvolucaoVendas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evolucao-vendas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ISBN')->textInput(['maxlength' => 13]) ?>

    <?= $form->field($model, 'data')->textInput() ?>

    <?= $form->field($model, 'quantidade')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
