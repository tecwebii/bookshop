<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EvolucaoVendasImporta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicacao-autor-form">

    <?php $form = ActiveForm::begin([
        'id' => 'WvendasImporta',
        'options' => ['enctype' => 'multipart/form-data'],
        'validationUrl' => ['/publicacaoautor/validate'],
/*        'enableClientScript' => true,
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,*/
    ]); ?>
    
    <?= $form->field($model, 'dataImportacao')->widget(\yii\jui\DatePicker::classname(), [
        'options'       => ['class' => 'form-control'],
        'language' => 'pt',
        'dateFormat' => 'dd-MM-yyyy',        
    ]) ?>

    <?= $form->field($model, 'ficheiro')->fileInput(['maxlength' => 255]) ?>   

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Importa'), ['btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <div id="ajaxWait" style="display:none;">
        <img src="/img/ajax loader.gif">
    </div>
</div>

<?php
$script = <<<EOD
$("form").submit( function(event) {        
        $("#ajaxWait").show("slow");
});    
EOD;
$this->registerJs($script);        
?>