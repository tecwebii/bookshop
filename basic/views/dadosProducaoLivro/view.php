<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DadosProducaoLivro */

?>
<div class="dados-producao-livro-view">
  
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'numeroPaginas',
            'peso',
            'tiragem',
            'grafica',
            [                      // the owner name of the model
                'label' => Yii::t('app','Paginador'),
                'value' => isset($model->paginador) 
                                ? $model->paginador->id.' '.$model->paginador->nome
                                : '' ,
            ],                                      
            [                      // the owner name of the model
                'label' => Yii::t('app','Designer Capa'),
                'value' => isset($model->designerCapa)
                                ? $model->designerCapa->id.' '.$model->designerCapa->nome
                                : '' ,
            ],  

        ],
    ]);
    ?>

</div>

