<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DadosProducaoLivro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dados-producao-livro-form">
    <?php $form = ActiveForm::begin([
        'id' => 'WdadosProducao',
        'validationUrl' => ['/dadosproducaolivro/validate','id'=>$model->id],
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
                    ]); ?>
  

    <?= $form->field($model, 'id',['options'=>["style"=>"display:none"]])->hiddenInput() ?>

    <?= $form->field($model, 'numeroPaginas')->textInput() ?>

    <?= $form->field($model, 'peso')->textInput() ?>

    <?= $form->field($model, 'tiragem')->textInput() ?>

    <?= $form->field($model, 'grafica')->textInput(['maxlength' => 50]) ?>
   
    <?= $form->field($model, 'idPaginador')->widget(\app\components\MyAutoComplete::classname(), 
        [   'ajaxUrl'       => '/recurso/autocompletepaginador' ,
            'options'       => ['class' => 'form-control'],            
            'clientOptions' => [
                'name'  => 'DadosProducaoLivro[idPaginador]',
                'value' => $model->idPaginador,
                ]
            ]) ?>
    
    <?= $form->field($model, 'idDesignerCapa')->widget(\app\components\MyAutoComplete::classname(), 
        [   'ajaxUrl'       => '/recurso/autocompletecapa' ,
            'options'       => ['class' => 'form-control'],            
            'clientOptions' => [
                'name'  => 'DadosProducaoLivro[idDesignerCapa]',
                'value' => $model->idDesignerCapa,
                ]
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
