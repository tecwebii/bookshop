<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DadosProducaoLivroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dados-producao-livro-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'numeroPaginas') ?>

    <?= $form->field($model, 'peso') ?>

    <?= $form->field($model, 'tiragem') ?>

    <?= $form->field($model, 'grafica') ?>

    <?php // echo $form->field($model, 'idPaginador') ?>

    <?php // echo $form->field($model, 'idDesignerCapa') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
