<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DadosProducaoLivro */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dados Producao Livro',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dados Producao Livros'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dados-producao-livro-update">    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
