<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DadosProducaoLivroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dados Producao Livros');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dados-producao-livro-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Dados Producao Livro'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'numeroPaginas',
            'peso',
            'tiragem',
            'grafica',
            // 'idPaginador',
            // 'idDesignerCapa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
