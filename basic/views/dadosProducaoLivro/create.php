<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DadosProducaoLivro */

$this->title = Yii::t('app', 'Create Dados Producao Livro');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dados Producao Livros'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dados-producao-livro-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
