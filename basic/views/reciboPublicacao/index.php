<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\My;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model \yii\db\ActiveRecord */

$this->title = Yii::t('app', 'Detalhes Recibo');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-publicacao-index">
    <h3><?= Html::encode($this->title) ?></h3>
    <?php $dataProvider = new activeDataProvider([
                                'query' => $model,
                                'pagination' => [
                                    'pageSize' => 9999,
                                ],
                            ]); ?>
    <?= GridView::widget([
        'id' => 'grid-reciboPublicacao',
        'dataProvider' => $dataProvider,
        'layout'=>"{items}",                                                            

        'showFooter'=>true,
        'footerRowOptions'=>['style'=>'border-top-style: ridge;font-weight:bold;background-color:#FFFFCC;'],
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'tipoAutoria',
            [
                'footer'=>Yii::t('app','Total'),        
                'attribute' => Yii::t('app', 'Publicacao'),
                'format' => 'raw',
                'value' => function ($model) {                      
                        return '<div>'.$model->publicacao->descricaoHTML.'</div>';
                },
            ],            
            [ 'class' => 'yii\grid\DataColumn',
                'attribute' => 'valor',
                'value' => function ($model) {                      
                        return Yii::$app->formatter->asCurrency($model->valor);
                },
                'contentOptions'=>['style'=>'text-align:right'],
                'headerOptions'=>['style'=>'text-align:right'],
                'footerOptions'=>['style'=>'text-align:right'],                
                'footer' => Yii::$app->formatter->asCurrency(My::pageTotal($dataProvider->models,'valor')),                
            ],
        ],
    ]); ?>

</div>
