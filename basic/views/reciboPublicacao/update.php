<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReciboPublicacao */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Recibo Publicacao',
]) . ' ' . $model->idRecibo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recibo Publicacaos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idRecibo, 'url' => ['view', 'idRecibo' => $model->idRecibo, 'idPublicacao' => $model->idPublicacao, 'idAutor' => $model->idAutor]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="recibo-publicacao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
