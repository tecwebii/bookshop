<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReciboPublicacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recibo-publicacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idRecibo')->textInput() ?>

    <?= $form->field($model, 'idPublicacao')->textInput() ?>

    <?= $form->field($model, 'idAutor')->textInput() ?>

    <?= $form->field($model, 'valor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
