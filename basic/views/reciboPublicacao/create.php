<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReciboPublicacao */

$this->title = Yii::t('app', 'Create Recibo Publicacao');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recibo Publicacaos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-publicacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
