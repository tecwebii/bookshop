<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* Inicializa GoogleCharts */
use fruppel\googlecharts\GoogleCharts;

echo GoogleCharts::widget([
        'id'=>'initPieChart',
	'visualization' => 'PieChart',                
        'data' =>[ 'cols'=>[], 'rows'=>[]],  
]); 
$this->registerJs(' 
    $(document).ready(function(){
        $("#googlechart_initPieChart").hide();
    });
');  
/* termina inicializacao GoogleCharts */

/* @var $this yii\web\View */
/* @var $searchModel app\models\AutorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Autors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autor-index">
    <?php Pjax::begin(); echo GridView::widget([
        'id' => 'grid-autor',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'resizableColumns'=>true,
        'persistResize'=>true,
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,      
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Create Autor'), ['create'], ['class' => 'btn btn-success']).' '.
                      Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['class' => 'btn btn-info']),            
            'after' =>false,
        ],
        'columns' => [
   //         ['class' => 'yii\grid\SerialColumn'],
            
            ['class' => '\kartik\grid\ExpandRowColumn',                                
                'expandTitle'=>Yii::t('app','Expande'),
                'collapseTitle'=>Yii::t('app','Colapsa'),
                'collapseAllTitle' => Yii::t('app','Colapsa todos'),
                'expandAllTitle' => Yii::t('app','Expande todos'),                
                'expandIcon'=>'<span class="glyphicon glyphicon-plus-sign"></span>',
                'collapseIcon'=>'<span class="glyphicon glyphicon-minus-sign"></span>',
                'value' => function ($model, $key, $index, $column) {
                                return GridView::ROW_COLLAPSED;
                },
                'detailUrl' => \yii\helpers\Url::to(['autor/detalhes']),
                'detailRowCssClass' => GridView::TYPE_DEFAULT,
                'pageSummary' => false,
            ],
            'id',
            'nome',             
            'NIF',            
            ['class' => '\kartik\grid\ActionColumn',
                'hiddenFromExport'=>true
                ],
        ],
    ]); Pjax::end(); ?>

</div>

 