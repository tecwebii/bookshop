<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Autor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="autor-form">

    <?php $form = ActiveForm::begin([
 //       'enableClientScript' => true,
   //     'enableClientValidation' => true,
     //   'enableAjaxValidation' => true,
        
    ]); ?>

    <?= $form->field($model, 'primeiroNome')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'nomeDoMeio')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'apelido')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'NIF')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'morada')->textInput(['maxlength' => 50]) ?>
    
<?php $acUrl = Url::to('codigopostal/autocomplete',true); ?>

    <?= $form->field($model, 'codPostal')->widget(\app\components\MyAutoComplete::classname(), 
        [   'ajaxUrl'       => $acUrl ,
            'renderLabel'   => "function( item ) {
    return item.label 
                    + '<br>Designação: ' + item.cp_designacao 
                    + '<br>Distrito: '   + item.distrito
                    + '<br>Concelho: '   + item.concelho
                    + (item.morada==''? '':'<br>Morada: ' + item.morada);
}",
            'renderItem'    => "function( ul, item ) {
    return $( '<li></li>' )
        .data( 'item.autocomplete', item )
        .append( '<a>' + item.label + '</a>' )
        .appendTo( ul );
}",
            'options'       => ['class' => 'form-control'],            
            'clientOptions' => [
                'name'  => 'Autor[codPostal]',
                'value' => $model->codPostal,
                ]
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
