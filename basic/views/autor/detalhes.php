<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Autor */

$this->title = Yii::t('app', 'Detalhes Autor');
$this->params['breadcrumbs'][] = $this->title;


$url  = Url::toRoute(['codigopostal/html','value'=>$model->codPostal],true);
$response = \app\components\My::cURL2String($url);

$response=$response!='' ? $response : $model->codPostal;

?>
<?= $this->render('_chart', [
    'model' => $model,
]) ?>   
<div class="Autor-details">

    <h3><?= Html::encode($this->title) ?></h3>
    <?= DetailView::widget([
        'model' => $model,        
        'attributes' => [
            'Nome',
            'NIF',
            'morada',
            [                
                'label' => Yii::t('app','Cod Postal'),
                'format'=> 'raw',
                'value'=>$response,
            ],
            
        ],
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => new activeDataProvider([
                                'query' => $model->getDetalhes(),
                                'pagination' => [
                                'pageSize' => 9999,
                                ],
                            ]),      
        'columns' => [
//            [   'class' => 'yii\grid\SerialColumn'],                            
                [
                    'attribute' => Yii::t('app', 'Publicacao'),
                    'format' => 'html',
                    'value' => function ($model) {                                                  
                            return '<div>'.
                                Yii::t('app','ISBN').': '.$model->publicacao->ISBN.'<br>'.
                                Yii::t('app','Titulo').': '.$model->publicacao->descricaoHTML.'<br>'.
                                Yii::t('app','Tiragem').': '.$model->tiragem.'<br>'.
                                Yii::t('app','P Venda').': '.Yii::$app->formatter->asCurrency($model->publicacao->pVenda).'<br>'.
                                Yii::t('app','Valor Total').': '.Yii::$app->formatter->asCurrency($model->valorTotal).'</div>';                                                
                                            
                    },
                ],
                [
                    'attribute' => Yii::t('app', 'Autoria'),
                    'format' => 'raw',
                    'value' => function ($model) {                                                  
                            return '<div>'.$model->tipoAutoria.'<br>'.
                                    Yii::t('app','Comissao '.$model->tipoAutoria).': '.$model->comissao.'<br>'.
                                    Yii::t('app','Comissao '.$model->tipoAutoria.' Total').': '.Yii::$app->formatter->asCurrency($model->comissaoTotal).
                                    '</div>';
                                                                           
                    },                    
                ],
                [
                    'attribute' => Yii::t('app', 'Vendas'),
                    'format' => 'raw',
                    'value' => function ($model) {                                                  
                            return '<div>'.
                                Yii::t('app','Data').': '.$model->dataVenda.'<br>'.
                                Yii::t('app','Total').': '.Yii::$app->formatter->asCurrency($model->totalVendas).'<br>'.
                                Yii::t('app','Quantidade').': '.$model->qtVendas.'<br>'.
                                Yii::t('app','Comissao '.$model->tipoAutoria).': '.Yii::$app->formatter->asCurrency($model->comissaoVendas).
                                '</div>';                                            
                    },
                ],		
                [
                    'attribute' => Yii::t('app', 'Pagamentos'),
                    'format' => 'raw',
                    'value' => function ($model) {                                                  
                            return '<div>'.
                                Yii::t('app','Data').': '.$model->dataPagamento.'<br>'.
                                Yii::t('app','Pago').': '.Yii::$app->formatter->asCurrency($model->pagamentosAnteriores).'<br>'.
                                Yii::t('app','Divida').': '.Yii::$app->formatter->asCurrency($model->comissaoVendas-$model->pagamentosAnteriores).'<br>'.
                                Yii::t('app','Por Liquidar').': '.Yii::$app->formatter->asCurrency($model->comissaoTotal-$model->pagamentosAnteriores).
                                    '</div>';
                    },
                ]
        ],
    ]); ?>
</div>


