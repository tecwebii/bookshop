<?php

use yii\helpers\Html;
use app\components\My;
use fruppel\googlecharts\GoogleCharts;


/* @var $this yii\web\View */
/* @var $model app\models\Publicacao */

$arr = $model->chart->one(); 
?>

<div class="chartAutorPie" style="width:48%;height:98%;display:inline-block;position: relative;">
<?php
$rows=[];
if($arr['comissaoDevida'] < 0 ) {
    $rows = 
        [
            [ 
                'c'=> [
                    ['v'=> 'Por liquidar'],
                    ['v'=> floatval($arr['comissaoTotal']-$arr['comissaoPaga'])]
                ]
            ],
            [
                'c'=> [
                    ['v'=> 'Liquidado'],
                    ['v'=> floatval($arr['comissaoPaga'])]
                ]  
            ]
        ];
}else{
    $rows = 
        [
            [ 
                'c'=> [
                    ['v'=> 'Por liquidar'],
                    ['v'=> floatval($arr['comissaoTotal']-$arr['comissaoVendas'])]
                ]
            ],
            [
                'c'=> [
                    ['v'=> 'Divida'],
                    ['v'=> floatval($arr['comissaoVendas']-$arr['comissaoPaga'])]
                ]  
            ],
            [
                'c'=> [
                    ['v'=> 'Pago'],
                    ['v'=> floatval($arr['comissaoPaga'])]
                ]  
            ],
        ];
}

?>
<?php echo GoogleCharts::widget([
	'id' => 'AutorPieChart_'.$model->id,
	'visualization' => 'PieChart',
        'options' => [
            'title' => Yii::t('app','Direitos Autor + Royalties'),
            'is3D' => true,
        ],
        'responsive' => true,
	'data' => [
            'cols' => [
                [
                    'id' => 'topping',
                    'label' => 'Topping',
                    'type' => 'string'
                ],
                [
                    'id' => 'slices',
                    'label' => 'Slices',
                    'type' => 'number'
                ]
            ],
            'rows' => $rows,
        ],
    
]); ?>
</div>     
<div class="chartAutorTable" style="width:47%;height:98%;display:inline-block;zoom: .87;">
    <?=     \yii\widgets\DetailView::widget([
        'model' => $arr,
        'attributes' => [
            [
                'label' => Yii::t('app','Total'),
                'format'=>'raw',
                'value' => '<div style="text-align: right">'.
                            Yii::$app->formatter->asCurrency($arr['comissaoTotal'])
                        .'</div>',                
            ],
            [
                'label' => Yii::t('app','Vendas'),
                'format'=>'raw',
                'value' => '<div style="text-align: right">'.
                            Yii::$app->formatter->asCurrency($arr['comissaoVendas'])
                        .'</div>',                
            ],
            [
                'label' => Yii::t('app','Pago'),
                'format'=>'raw',
                'value' => '<div style="text-align: right">'.
                            Yii::$app->formatter->asCurrency($arr['comissaoPaga'])
                        .'</div>',                
            ],
            [
                'label' => Yii::t('app','Divida'),
                'format'=>'raw',
                'value' => '<div style="text-align: right">'.
                            Yii::$app->formatter->asCurrency($arr['comissaoDevida'])
                        .'</div>',                
            ],
            [
                'label' => Yii::t('app','Por Liquidar'),
                'format'=>'raw',
                'value' => '<div style="text-align: right">'.
                            Yii::$app->formatter->asCurrency($arr['comissaoPorLiquidar'])
                        .'</div>',                
            ],
            [
                'label' => Yii::t('app','nº de publicações'),
                'format'=>'raw',
                'value' => '<div style="text-align: right">'.
                            $arr['publicacoes']
                        .'</div>',                
            ],
        ],
    ]) ?>
</div> 
