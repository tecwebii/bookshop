<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoCusto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publicacao-custo-form">

    <?php $form = ActiveForm::begin([        
        'id' => 'WpublicacaoCusto',
        'validationUrl' => ['/publicacaocusto/validate'],//,'id'=>$model->id],
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
    ]); ?>
    <?= $form->field($model, 'id',['options'=>["style"=>"display:none"]])->hiddenInput() ?>
    <?= $form->field($model, 'idPublicacao',['options'=>["style"=>"display:none"]])->hiddenInput() ?>
    
    <?= $form->field($model, 'rubrica')
        ->dropDownList(
            ArrayHelper::map(\app\models\Rubrica::find()->all(), 'rubrica', 'rubrica'),
                    array_merge([   'prompt'=>'Escolha uma rubrica',
                                    'readonly' => ! $model->isNewRecord,], 
                        $model->isNewRecord ? [] : ['disabled'=>'true']  ));
    ?>

    <?= $form->field($model, 'valor')->widget(kartik\money\MaskMoney::classname(), [
                'pluginOptions' => [
                    'prefix' => '€ ',
                    'suffix' => '',
                    'allowNegative' => false,
                ]
            ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
