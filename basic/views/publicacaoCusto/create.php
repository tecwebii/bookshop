<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PublicacaoCusto */

$this->title = Yii::t('app', 'Create Publicacao Custo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Publicacao Custos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-custo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
