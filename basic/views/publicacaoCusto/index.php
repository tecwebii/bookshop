<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Publicacao Custos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publicacao-custo-index">

    <h1><?php //= Html::encode($this->title) ?></h1>
    
    <p>
        <?= Html::a(Yii::t('app', 'Create Publicacao Custo'), ['create','id'=>$idPublicacao], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'id' => 'grelha-publicacao-custo',
        'dataProvider' => $dataProvider,                
        'showFooter'=>true,
        'footerRowOptions'=>['style'=>'border-top-style: ridge;font-weight:bold;background-color:#FFFFCC;'],
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'rubrica',
                'footer'=>Yii::t('app','Total'),
            ],
            [ 'class' => 'yii\grid\DataColumn',
                'attribute' => 'valor',
                'value'=> function($model){
                    return Yii::$app->formatter->asCurrency($model->valor);
                },
                'footer'=> Yii::$app->formatter->asCurrency(app\components\My::pageTotal($dataProvider->models,'valor')),                
                'contentOptions'=>['style'=>'text-align:right'],
                'headerOptions'=>['style'=>'text-align:right'],
                'footerOptions'=>['style'=>'text-align:right'],              
            ],          

            ['class' => 'yii\grid\ActionColumn','template' => '{delete} {update}'],
        ],
    ]);    ?>

</div>
