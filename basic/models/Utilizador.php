<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilizador".
 *
 * @property integer $id
 * @property string $nome
 * @property string $senha
 * @property string $authenticationKey
 * @property string $accessToken
 * @property string $email
 * @property string $telefone
 * @property string $tipoUtilizador
 */
class Utilizador extends \yii\db\ActiveRecord
{
    public static $TIPO_ADMIN       = "Admin";
    public static $TIPO_UTILIZADOR  = "Utilizador";
    
    public $senha2;    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'utilizador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'senha', 'email', 'tipoUtilizador'], 'required'],
            [['nome'], 'unique'],
            [['email'], 'unique'],
            [['tipoUtilizador'], 'string'],                
            [['nome', 'senha', 'authenticationKey', 'accessToken', 'email'], 'string', 'max' => 50],
            [['senha2'], 'string', 'max' => 25],
            [['telefone'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nome' => Yii::t('app', 'Nome'),
            'senha' => Yii::t('app', 'Senha'),
            'senha2' => Yii::t('app', 'Senha'),
            'authenticationKey' => Yii::t('app', 'Authentication Key'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'email' => Yii::t('app', 'Email'),
            'telefone' => Yii::t('app', 'Telefone'),
            'tipoUtilizador' => Yii::t('app', 'Tipo Utilizador'),
        ];
    }
    
    
    public function afterFind() {
        parent::afterFind();        
        $this->senha2="";
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->afterFind();
    }
      
    public function beforeValidate() {
        if($this->crypt($this->senha2) != $this->senha) {
            $this->senha = $this->crypt($this->senha2);
            $this->authenticationKey = Yii::$app->security->generateRandomString();
        }        
        return parent::beforeValidate();
    }
    
           
    public function crypt( $value ){
        return \Yii::$app->encrypter->encrypt($value );
    }
    
    
    public function isAdmin(){
        return $this->tipoUtilizador == self::$TIPO_ADMIN;
    }
                
    public static function find()
    {
        $ret = parent::find();
        if( isset(Yii::$app->user) && isset(Yii::$app->user->identity))
            if( ! Yii::$app->user->identity->isAdmin() ) {
                $ret->andWhere( ['id' => Yii::$app->user->identity->getId() ] );
                $a=Yii::$app->user->identity->isAdmin();
                $a1=Yii::$app->user->identity->tipoUtilizador;
                $a2=self::$TIPO_ADMIN;
                if(($a==$a1)>$a2)
                   $b="a"; 
            }
        return $ret;
    }
    
}
