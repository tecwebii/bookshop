<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autor".
 *
 * @property integer $id
 * @property string $primeiroNome
 * @property string $nomeDoMeio
 * @property string $apelido
 * @property string $NIF
 * @property string $morada
 * @property string $codPostal
 *
 * @property Publicacaoautor[] $publicacaoautors
 * @property Recibo[] $recibos
 */
class Autor extends \yii\db\ActiveRecord
{
    private $c_nome;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['primeiroNome', 'apelido'], 'required'],
            [['primeiroNome', 'nomeDoMeio', 'apelido', 'morada'], 'string', 'max' => 50],
            [['NIF'], 'string', 'max' => 12],
            [['codPostal'], 'string', 'max' => 8],
            [['codPostal'], 'match', 'pattern'=> CodigoPostal::zipCodRegExMask()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'primeiroNome' => Yii::t('app', 'Primeiro Nome'),
            'nomeDoMeio' => Yii::t('app', 'Nome Do Meio'),
            'apelido' => Yii::t('app', 'Apelido'),
            'NIF' => Yii::t('app', 'Nif'),
            'morada' => Yii::t('app', 'Morada'),
            'codPostal' => Yii::t('app', 'Cod Postal'),
            'nome' => Yii::t('app', 'Nome'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacaoautors()
    {
        return $this->hasMany(Publicacaoautor::className(), ['idAutor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecibos()
    {
        return $this->hasMany(Recibo::className(), ['idAutor' => 'id']);
    }
    
    public function getNome() {
        if (isset($this->c_nome))
            return $this->c_nome;
        else
            return $this->primeiroNome. ( $this->nomeDoMeio!='' ? ' '.$this->nomeDoMeio : '').' '.$this->apelido;
    }
    
    public function setNome( $nome ) {
        $this->c_nome = $nome;
    }
    
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalhes() {
        return ReciboPublicacao::getAutorDetalhes($this->id);        
    }

    public function getChart() {        
        $q=new \yii\db\Query;
        $q->select(['p.idAutor','p.idPublicacao',
//                    't.total','t.tiragem',
//                    'sum(p.qtVendas) qtVendas',
//                    'sum(p.totalVendas) totalVendas',
//                    'sum(p.comissao) percComissao',
                    'sum(p.totalComissaoAutor) as comissaoTotal',
                    'sum(p.comissaoVendas) comissaoVendas',
                    'sum(p.totalComissaoPaga) comissaoPaga'])
            ->from(['p'=>'publicacaoautorpagamento'])
            ->innerJoin(['t'=>'publicacaototais'],'t.id=p.idPublicacao')
            ->where(['p.idAutor'=>$this->id])
            ->groupBy(['p.idAutor','p.idPublicacao','p.totalComissaoAutor']);
//,'t.total','t.tiragem']);
        $qry=new \yii\db\Query;
        $qry->select(['idAutor',
//                'sum(total) total',
//                'sum(totalVendas) totalVendas',
//                'sum(tiragem) tiragem',
//                'sum(qtVendas) qtVendas',
//                'sum(percComissao) percComissao',
                'sum(comissaoTotal) comissaoTotal',
                'sum(comissaoVendas) comissaoVendas',
                'sum(comissaoPaga) comissaoPaga',
                'sum(comissaoVendas)-sum(comissaoPaga) comissaoDevida',
                'sum(comissaoTotal)-sum(comissaoPaga) comissaoPorLiquidar',
                'count(*) publicacoes'])
            ->from(['a'=>$q])
            ->groupBy('idAutor');
        $a=$qry->createCommand()->getRawSql();
        return $qry;
    }
    
    public static function insertIfNotExist($primeiroNome,$nomeDoMeio,$apelido) {        
        $model = Autor::find()->where([
                        'primeiroNome'  =>$primeiroNome,
                        'nomeDoMeio'    =>$nomeDoMeio,
                        'apelido'       =>$apelido])->one();        
        if($model==null){
            $model= new Autor();
            $model->primeiroNome = $primeiroNome;
            $model->nomeDoMeio   = $nomeDoMeio;            
            $model->apelido      = $apelido;
            $model->save();
        }
        return $model->id;
    }
    
}

