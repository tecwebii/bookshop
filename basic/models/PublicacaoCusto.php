<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publicacaocusto".
 *
 * @property integer $id 
 * @property integer $idPublicacao
 * @property string $rubrica
 * @property double $valor
 *
 * @property Rubrica $rubrica0 
 * @property Publicacao $Publicacao
 */
class PublicacaoCusto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicacaocusto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPublicacao', 'rubrica', 'valor'], 'required'],
            [['idPublicacao'], 'integer'],
            [['valor'], 'number'],
            [['rubrica'], 'string', 'max' => 50],
            [['rubrica'], 'unique', 'targetAttribute' => ['idPublicacao', 'rubrica']],
//            [['rubrica'], 'checkUnique']
        ];
    }
    
/*    public function checkUnique($attribute, $params) {
        $m = PublicacaoCusto::find()                   
                   ->where(['idPublicacao' =>$this->idPublicacao])                
                ->andWhere(['rubrica'  =>$this->rubrica]);
        $a=$m->createCommand()->getRawSql();
        $aOnInsert =  $this->isNewRecord && ($m->one()!=null);
        $aOnUpdate = !$this->isNewRecord && ($m->andWhere(['<>','id',$this->id])->one()!=null);
        if( $aOnInsert || $aOnUpdate )  {            
            $this->addError($attribute, Yii::t('app', 'A rubrica “{rubrica}” já foi atribuida.',
                                    ['rubrica'=>$this->rubrica]));
        }
    }  */

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'idPublicacao' => Yii::t('app', 'Id Publicacao'),
            'rubrica' => Yii::t('app', 'Rubrica'),
            'valor' => Yii::t('app', 'Valor'),
        ];
    }
    
    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getRubrica0() 
   { 
       return $this->hasOne(Rubrica::className(), ['rubrica' => 'rubrica']);        
   } 
 
  
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacao()
    {
        return $this->hasOne(Publicacao::className(), ['id' => 'idPublicacao']);
    }
}
