<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Autor;

/**
 * AutorSearch represents the model behind the search form about `app\models\Autor`.
 */
class AutorSearch extends Autor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['primeiroNome', 'nomeDoMeio', 'apelido', 'NIF', 'morada', 'codPostal','nome'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Autor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'primeiroNome', $this->primeiroNome])
            ->andFilterWhere(['like', "concat(primeiroNome,' ',nomeDoMeio,' ',apelido)", $this->nome])
            ->andFilterWhere(['like', 'nomeDoMeio', $this->nomeDoMeio])
            ->andFilterWhere(['like', 'apelido', $this->apelido])
            ->andFilterWhere(['like', 'NIF', $this->NIF])
            ->andFilterWhere(['like', 'morada', $this->morada])
            ->andFilterWhere(['like', 'codPostal', $this->codPostal]);
        
        $dataProvider->setSort([
            'attributes'=>[
                'nome'=>
                        [   'asc'  => ["concat(primeiroNome,' ',nomeDoMeio,' ',apelido)" => SORT_ASC ],
                            'desc' => ["concat(primeiroNome,' ',nomeDoMeio,' ',apelido)" => SORT_DESC ],
                ],                
            ]
        ]);
        return $dataProvider;
    }
}
