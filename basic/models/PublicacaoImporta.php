<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class PublicacaoImporta extends Model
{
    public $dataImportacao;
    public $ficheiro;
    public $random;
    
    public function init() {
        $this->dataImportacao = date("Y-m-d");
        $this->random = Yii::$app->security->generateRandomString();
        return parent::init();
    }
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['dataImportascao', 'ficheiro'], 'required'],
            // rememberMe must be a boolean value
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'dataImportacao' => Yii::t('app', 'Data de Importação'),            
            'ficheiro' => Yii::t('app', 'Ficheiro'),            
            'random' => Yii::t('app', 'Aleatorio'),            
        ];
    }
    
    public function getData() {
        return $this->dataImportacao;
    }
    
    public function setData($data) {
        $this->dataImportacao = $data;
    }
    
}
