<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publicacaocustos".
 *
 * @property integer $idPublicacao
 * @property string $rubrica
 * @property double $valor
 *
 * @property Publicacao $idPublicacao0
 */
class PublicacaoCustos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicacaocustos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPublicacao', 'rubrica', 'valor'], 'required'],
            [['idPublicacao'], 'integer'],
            [['valor'], 'number'],
            [['rubrica'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPublicacao' => Yii::t('app', 'Id Publicacao'),
            'rubrica' => Yii::t('app', 'Rubrica'),
            'valor' => Yii::t('app', 'Valor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPublicacao0()
    {
        return $this->hasOne(Publicacao::className(), ['id' => 'idPublicacao']);
    }
}
