<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PublicacaoAutor;

/**
 * PublicacaoAutorSearch represents the model behind the search form about `app\models\PublicacaoAutor`.
 */
class PublicacaoAutorSearch extends PublicacaoAutor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPublicacao', 'idAutor'], 'integer'],
            [['tipoAutoria', 'contrato'], 'safe'],
            [['comissao'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PublicacaoAutor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idPublicacao' => $this->idPublicacao,
            'idAutor' => $this->idAutor,
            'comissao' => $this->comissao,
        ]);

        $query->andFilterWhere(['like', 'tipoAutoria', $this->tipoAutoria])
            ->andFilterWhere(['like', 'contrato', $this->contrato]);

        return $dataProvider;
    }
}
