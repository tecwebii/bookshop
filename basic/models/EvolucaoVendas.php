<?php

namespace app\models;

use Yii;
use DateTime;
use \app\components\My;

/**
 * This is the model class for table "evolucaovendas".
 *
 * @property integer $id
 * @property string $ISBN
 * @property string $data
 * @property integer $quantidade
 *
 * @property Publicacao $publicacao
 */
class EvolucaoVendas extends \yii\db\ActiveRecord
{
    public $titulo;
    
    public static function findWithPublicacao() {
        return parent::find()
                ->select(['e.id','e.ISBN','e.data','e.quantidade',
                        "titulo"=>"concat(p.titulo,' ',p.subtitulo)",
                        ])  
                ->from(['e'=>'evolucaovendas'])
                ->innerJoin(['p'=>'publicacao'],'p.isbn=e.isbn');
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'evolucaovendas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ISBN', 'data', 'quantidade'], 'required'],
            [['data'], 'safe'],
            [['data'], 'checkMinimalDate'],
            [['quantidade'], 'integer'],
            [['ISBN'], 'string', 'max' => 13],
            [['ISBN'], 'checkISBNExistense'],
            [['ISBN'], 'unique', 'targetAttribute' => ['ISBN', 'data'],
                'message' => Yii::t('app','A combinação dos campos ISBN ({isbn}) e data ({data}) já existem na tabela evolução de vendas',
                            ['isbn'=>$this->ISBN,'data'=>$this->data])]
        ];
    }

    public function checkISBNExistense($attribute, $params) {
        if(!isset($this->publicacao)) {
            $this->addError($attribute, Yii::t('app', 'ISBN não existe na tabela de publicações.'));
        }
    }
    
    public function checkMinimalDate($attribute, $params)
    {
        if(isset($this->publicacao) && 
           My::sqlData($this->$attribute)<My::sqlData($this->publicacao->dataEdicao)) {
            $this->addError($attribute, Yii::t('app', 'A data de venda é menor que a data de edição.'));
        }
    }
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ISBN' => Yii::t('app', 'Isbn'),
            'data' => Yii::t('app', 'Data'),
            'sqlData' => Yii::t('app', 'Data'),
            'quantidade' => Yii::t('app', 'Quantidade'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacao()
    {
        return $this->hasOne(Publicacao::className(), ['ISBN' => 'ISBN']);
    }

    public function beforeValidate() {
        $this->data=My::sqlData($this->data);        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert) {        
        $this->data=My::sqlData($this->data);        
        return parent::beforeSave($insert);
    }
    
    public function afterValidate() {
        $this->data=My::phpData($this->data);
        return parent::afterValidate();
    }

    public function afterFind() {                
        $this->data=My::phpData($this->data);
        return parent::afterFind();
    }
    
   
}


