<?php

namespace app\models;

use Yii;
use \app\components\My;
use \yii\db\Query;


/**
 * This is the model class for table "recibo".
 *
 * @property integer $id
 * @property integer $idAutor
 * @property string $data
 *
 * @property Autor $autor
 * @property Recibopublicacao[] $recibopublicacoes
 * @property RecVendasComissao[] $detalhes
 */
class Recibo extends \yii\db\ActiveRecord
{
    public $nome,$valor;
    
    
    public static function find() {
        return parent::find();                
    }
    
    public static function findWithAutor() {
        $ret= parent::find()
                ->select(['r.id', 'r.idAutor', 'r.data', 
                        "nome"=>"concat(a.primeiroNome,' ',a.nomeDoMeio,' ',a.apelido)",                     
                        'valor'=>"sum(ifnull(rp.valor,0))"])  
                ->from(['r'=>'recibo'])
                ->innerJoin(['a'=>'autor'],'a.id=r.idAutor')
                ->leftJoin(['rp'=>'recibopublicacao'],'rp.idRecibo=r.id AND rp.valor<>0')
                ->groupBy(['r.id', 'r.idAutor', 'r.data', 
                            'a.primeiroNome','a.nomeDoMeio','a.apelido']);
    //    $a=$ret->createCommand()->getRawSql();
        return $ret;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recibo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idAutor', 'data'], 'required'],
            [['idAutor'], 'integer'],
            [['data'], 'safe'],            
            [['data'], 'checkDate'],
        ];
    }
    
    public function checkDate($attribute, $params)
    {        
        
        if($this->data!='') {
            $id = (empty($this->id)) ? 999999999999999 : $this->id;
            $data = My::sqlData($this->data);        
            if( ($model=Recibo::find()->where("idAutor=$this->idAutor AND id <> $id AND data ='$data'")->one()) != null ) {
                $this->addError($attribute,Yii::t('app','Existe outro recibo ({recibo}) na data definida',['recibo'=>'id='.$model->id]));
            }
            if( ($model=Recibo::find()->where("idAutor=$this->idAutor AND id < $id AND data > '$data'")->one()) != null ) {
                $this->addError($attribute, Yii::t('app', 'Existe um recibo ({recibo}) anterior com data superior á definida.',['recibo'=>'id='.$model->id]));
            }
            if( ($model=Recibo::find()->where("idAutor=$this->idAutor AND id > $id AND data < '$data'")->one()) != null ) {
                $this->addError($attribute, Yii::t('app', 'Existe um recibo ({recibo}) posterior data inferior á definida.',['recibo'=>'id='.$model->id]));           
            }
        }
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'idAutor' => Yii::t('app', 'Id Autor'),
            'data' => Yii::t('app', 'Data'),
            'nome' => Yii::t('app', 'Nome'),
            'valor' => Yii::t('app', 'Valor')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(Autor::className(), ['id' => 'idAutor']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getRecibopublicacoes()
    {
        return $this->hasMany(Recibopublicacao::className(), ['idRecibo' => 'id']);
    }
    
    public function beforeValidate() {
        $this->data=My::sqlData($this->data);        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert) {        
        $this->data=My::sqlData($this->data);        
        return parent::beforeSave($insert);
    }
    
    public function afterValidate() {
        $this->data=My::phpData($this->data);
        return parent::afterValidate();
    }

    public function afterFind() {                
        $this->data=My::phpData($this->data);
        return parent::afterFind();
    }        
    
    /**
     * 
     * @return \yii\db\ActiveRecord
     */
    public function getVendasEComissoes() {        
        $query = (new \yii\db\Query());
        $query=$query
            ->select(['c.idAutor','c.tipoAutoria', 'c.idPublicacao','c.ISBN','c.comissao',
                        'data'=>'max(c.data)',
                        'totalVendas'=>'max(c.totalVendas)',
                        'qtVendas'=>'max(c.qtVendas)',
                        'comissaoVendas'=>'max(c.comissaoVendas)'])
            ->from('publicacaoautorvendascomissao c')            
            ->where(['c.idAutor' => $this->idAutor])
            ->andWhere(['<=','c.data',  My::sqlData($this->data)])
            ->groupBy(['c.tipoAutoria', 'c.idPublicacao','c.ISBN','c.comissao']);
        return $query;
    }
    
    /**
     * 
     * @return \yii\db\ActiveRecord
     */
    public function getPagamentos(){
        $query = (new \yii\db\Query());
        $query=$query
            ->select(['rp.idAutor', 'rp.tipoAutoria', 'rp.idPublicacao',
                        'data'=>'max(r.data)',
                        'sum(rp.valor) as valor',
                        ])
            ->from('recibopublicacao rp')            
            ->innerJoin('recibo r','r.id = rp.idRecibo')
            ->where(['rp.idAutor' => $this->idAutor])
            ->andWhere(['<','r.data',  My::sqlData($this->data)])
            ->groupBy(['rp.idAutor', 'rp.tipoAutoria', 'rp.idPublicacao']);
        return $query;
    }
    
    
    public function getPublicacoes(){
        $query = (new \yii\db\Query());
        $query=$query
            ->select(['pa.idAutor', 'pa.tipoAutoria', 'pa.idPublicacao','pa.comissao',
                        'dp.tiragem',
                        'valorTotal'=>'(dp.tiragem*p.pVenda)',
                        'comissaoTotal'=>'(if(dp.tiragem*p.pVenda*pa.comissao=0,0,dp.tiragem*p.pVenda*pa.comissao/100))'
                        ])
            ->from(['pa'=>'publicacaoautor']) 
            ->innerJoin(['p'=>'publicacao'],'p.id = pa.idPublicacao')
            ->innerJoin(['dp'=>'dadosproducaolivro'],'dp.id = pa.idPublicacao')
            ->where(['idAutor' => $this->idAutor])
            ;
        return $query;
    }
    /**
     * 
     * @return \yii\db\ActiveRecord
     */
    public function getRecibo(){
        $query = (new \yii\db\Query());
        $query=$query
            ->select(['rp.idAutor', 'rp.tipoAutoria', 'rp.idPublicacao',
                        'r.data',
                        'rp.valor',
                        ])
            ->from('recibopublicacao rp')            
            ->innerJoin('recibo r','r.id = rp.idRecibo')
            ->where(['rp.idRecibo' => $this->id])
            ;
        return $query;
    }
    
    /**
     * @return PublicacaoAutorVendasComissao
     */
    public function getDetalhes() {
        return ReciboPublicacao::getAutorRecibo($this->idAutor, $this->data);     
    }
            

}
