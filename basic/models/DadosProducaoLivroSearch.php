<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DadosProducaoLivro;

/**
 * DadosProducaoLivroSearch represents the model behind the search form about `app\models\DadosProducaoLivro`.
 */
class DadosProducaoLivroSearch extends DadosProducaoLivro
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'numeroPaginas', 'peso', 'tiragem', 'idPaginador', 'idDesignerCapa'], 'integer'],
            [['grafica'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DadosProducaoLivro::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'numeroPaginas' => $this->numeroPaginas,
            'peso' => $this->peso,
            'tiragem' => $this->tiragem,
            'idPaginador' => $this->idPaginador,
            'idDesignerCapa' => $this->idDesignerCapa,
        ]);

        $query->andFilterWhere(['like', 'grafica', $this->grafica]);

        return $dataProvider;
    }
}
