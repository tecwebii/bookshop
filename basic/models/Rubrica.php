<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rubrica".
 *
 * @property string $rubrica
 *
 * @property Publicacaocusto[] $publicacaocustos
 * @property Publicacao[] $idPublicacaos
 */
class Rubrica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubrica';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rubrica'], 'required'],
            [['rubrica'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rubrica' => Yii::t('app', 'Rubrica'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacaocustos()
    {
        return $this->hasMany(Publicacaocusto::className(), ['rubrica' => 'rubrica']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPublicacaos()
    {
        return $this->hasMany(Publicacao::className(), ['id' => 'idPublicacao'])->viaTable('publicacaocusto', ['rubrica' => 'rubrica']);
    }
}
