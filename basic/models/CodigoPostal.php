<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_cod_postal".
 *
 * @property integer $id
 * @property string $id_distrito
 * @property string $id_concelho
 * @property string $cod_localidade
 * @property string $localidade
 * property string $arteria_cod
 * property string $arteria_tipo
 * property string $pri_prep
 * property string $arteria_titulo
 * property string $seg_prep
 * property string $arteria_designacao
 * property string $morada_local
 * property string $troco
 * property string $porta
 * property string $cliente
 * property string $cp4
 * property string $cp3
 * @property string $cp_designacao
 * @property string $morada
 * @property string $codpostal
 * @property string $distrito
 * @property string $concelho
 */
class CodigoPostal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cod_postal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          /*  [['id_distrito', 'id_concelho'], 'string', 'max' => 2],
            [['cod_localidade', 'pri_prep', 'seg_prep'], 'string', 'max' => 5],
            [['localidade'], 'string', 'max' => 45],
            [['arteria_cod'], 'string', 'max' => 15],
            [['arteria_tipo', 'arteria_titulo'], 'string', 'max' => 25],
            [['arteria_designacao', 'morada_local'], 'string', 'max' => 70],
            [['troco', 'cliente', 'morada'], 'string', 'max' => 80],
            [['porta'], 'string', 'max' => 20],
            [['cp4'], 'string', 'max' => 4],
            [['cp3'], 'string', 'max' => 3],
            [['cp_designacao', 'distrito', 'concelho'], 'string', 'max' => 30],
            [['codpostal'], 'string', 'max' => 8],
            [['codpostal'], 'match', 'pattern'=> self::zipCodRegExMask()]*/
            [['id_distrito', 'id_concelho'], 'string', 'max' => 2],
            [['cod_localidade', ], 'string', 'max' => 5],
            [['localidade'], 'string', 'max' => 45],
            [['morada'], 'string', 'max' => 80],
            [['cp_designacao', 'distrito', 'concelho'], 'string', 'max' => 30],
            [['codpostal'], 'string', 'max' => 8],
            [['codpostal'], 'match', 'pattern'=> self::zipCodRegExMask()]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_distrito' => Yii::t('app', 'Id Distrito'),
            'id_concelho' => Yii::t('app', 'Id Concelho'),
            'cod_localidade' => Yii::t('app', 'Cod Localidade'),
            'localidade' => Yii::t('app', 'Localidade'),
/*            'arteria_cod' => Yii::t('app', 'Arteria Cod'),
            'arteria_tipo' => Yii::t('app', 'Arteria Tipo'),
            'pri_prep' => Yii::t('app', 'Pri Prep'),
            'arteria_titulo' => Yii::t('app', 'Arteria Titulo'),
            'seg_prep' => Yii::t('app', 'Seg Prep'),
            'arteria_designacao' => Yii::t('app', 'Arteria Designacao'),
            'morada_local' => Yii::t('app', 'Morada Local'),
            'troco' => Yii::t('app', 'Troco'),
            'porta' => Yii::t('app', 'Porta'),
            'cliente' => Yii::t('app', 'Cliente'),
            'cp4' => Yii::t('app', 'Cp4'),
            'cp3' => Yii::t('app', 'Cp3'),*/
            'cp_designacao' => Yii::t('app', 'Cp Designacao'),
            'morada' => Yii::t('app', 'Morada'),
            'codpostal' => Yii::t('app', 'Codpostal'),
            'distrito' => Yii::t('app', 'Distrito'),
            'concelho' => Yii::t('app', 'Concelho'),
        ];
    }
    
    public static function zipCodRegExMask( $lang='pt') {
        $arr=[
            "GB"=> "GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\d{1,4}",
            "JE"=> "JE\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}",
            "GG"=> "GY\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}",
            "IM"=> "IM\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}",
            "US"=> "\d{5}([ \-]\d{4})?",
            "CA"=> "[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]?\d[ABCEGHJ-NPRSTV-Z]\d",
            "DE"=> "\d{5}",
            "JP"=> "\d{3}-\d{4}",
            "FR"=> "\d{2}[ ]?\d{3}",
            "AU"=> "\d{4}",
            "IT"=> "\d{5}",
            "CH"=> "\d{4}",
            "AT"=> "\d{4}",
            "ES"=> "\d{5}",
            "NL"=> "\d{4}[ ]?[A-Z]{2}",
            "BE"=> "\d{4}",
            "DK"=> "\d{4}",
            "SE"=> "\d{3}[ ]?\d{2}",
            "NO"=> "\d{4}",
            "BR"=> "\d{5}[\-]?\d{3}",
            "PT"=> "\d{4}([\-]\d{3})?",
            "FI"=> "\d{5}",
            "AX"=> "22\d{3}",
            "KR"=> "\d{3}[\-]\d{3}",
            "CN"=> "\d{6}",
            "TW"=> "\d{3}(\d{2})?",
            "SG"=> "\d{6}",
            "DZ"=> "\d{5}",
            "AD"=> "AD\d{3}",
            "AR"=> "([A-HJ-NP-Z])?\d{4}([A-Z]{3})?",
            "AM"=> "(37)?\d{4}",
            "AZ"=> "\d{4}",
            "BH"=> "((1[0-2]|[2-9])\d{2})?",
            "BD"=> "\d{4}",
            "BB"=> "(BB\d{5})?",
            "BY"=> "\d{6}",
            "BM"=> "[A-Z]{2}[ ]?[A-Z0-9]{2}",
            "BA"=> "\d{5}",
            "IO"=> "BBND 1ZZ",
            "BN"=> "[A-Z]{2}[ ]?\d{4}",
            "BG"=> "\d{4}",
            "KH"=> "\d{5}",
            "CV"=> "\d{4}",
            "CL"=> "\d{7}",
            "CR"=> "\d{4,5}|\d{3}-\d{4}",
            "HR"=> "\d{5}",
            "CY"=> "\d{4}",
            "CZ"=> "\d{3}[ ]?\d{2}",
            "DO"=> "\d{5}",
            "EC"=> "([A-Z]\d{4}[A-Z]|(?:[A-Z]{2})?\d{6})?",
            "EG"=> "\d{5}",
            "EE"=> "\d{5}",
            "FO"=> "\d{3}",
            "GE"=> "\d{4}",
            "GR"=> "\d{3}[ ]?\d{2}",
            "GL"=> "39\d{2}",
            "GT"=> "\d{5}",
            "HT"=> "\d{4}",
            "HN"=> "(?:\d{5})?",
            "HU"=> "\d{4}",
            "IS"=> "\d{3}",
            "IN"=> "\d{6}",
            "ID"=> "\d{5}",
            "IL"=> "\d{5}",
            "JO"=> "\d{5}",
            "KZ"=> "\d{6}",
            "KE"=> "\d{5}",
            "KW"=> "\d{5}",
            "LA"=> "\d{5}"];
        return '/'.$arr[strtoupper(substr($lang, 0,2))].'/';
        }
}
