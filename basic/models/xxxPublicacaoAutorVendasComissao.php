<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publicacaoautorvendascomissao".
 *
 * @property integer $idPublicacao
 * @property string $ISBN
 * @property string $tipoAutoria
 * @property integer $idAutor
 * @property double $comissao
 * @property string $data
 * @property double $totalVendas
 * @property string $qtVendas
 * @property double $comissaoVendas
 * @property double $porLiquida
 * @property double $divida
 * @property Autor $autor
 * @property Publicacao $publicacao
 * @property ReciboPublicacao $reciboPublicacao
 
 * @property Recibopublicacao[] $recibopublicacoes
 */
class PublicacaoAutorVendasComissao extends \yii\db\ActiveRecord
{
    public $tiragem,$valorTotal,$comissaoTotal,$porLiquidar,$divida,
            $idRecibo,$pagamentosAnteriores,$pagamento,$dataPagamento,$dataRecibo,$dataVenda;
    
    public static function primaryKey() {
        return ['idRecibo','idPublicacao','idAutor','tipoAutoria','data'];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicacaoautorvendascomissao';
    }
       

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPublicacao', 'ISBN', 'idAutor', 'comissao'], 'required'],
            [['idPublicacao', 'idAutor', 'qtVendas'], 'integer'],
            [['tipoAutoria'], 'string'],
            [['comissao', 'totalVendas', 'comissaoVendas'], 'number'],
            [['tiragem', 'valorTotal', 'comissaoTotal','pagamentosAnteriores','pagamento'], 'number'],
            [['data'], 'safe'],
            [['ISBN'], 'string', 'max' => 13],
            
            [['idRecibo','tiragem'], 'integer'],
            [['valorTotal', 'comissaoTotal', 'pagamentosAnteriores','porLiquidar','divida'], 'number'],
            [['pagamento', 'comissaoTotal', 'pagamentosAnteriores'], 'number'],            
            [['dataPagamento','dataRecibo','dataVenda'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPublicacao'  => Yii::t('app', 'Id Publicacao'),
            'ISBN'          => Yii::t('app', 'Isbn'),
            'tipoAutoria'   => Yii::t('app', 'Tipo Autoria'),
            'idAutor'       => Yii::t('app', 'Id Autor'),
            'comissao'      => Yii::t('app', 'Comissao'),
            'data'          => Yii::t('app', 'Data'),
            //
            'dataVenda'     => Yii::t('app', 'Data Venda'),
            'totalVendas'   => Yii::t('app', 'Total Vendas'),
            'qtVendas'      => Yii::t('app', 'Quantidade Vendas'),
            'comissaoVendas'=> Yii::t('app', 'Comissao Vendas'),
            //
            'tiragem'       => Yii::t('app', 'Tiragem'),
            'valorTotal'    => Yii::t('app', 'Valor Total'),
            'comissaoTotal' => Yii::t('app', 'Comissao Total'),
            'porLiquidar'   => Yii::t('app', 'Por Liquidar'),
            'divida'        => Yii::t('app', 'Divida'),
            'dataPagamento' => Yii::t('app', 'Data Pagamento'),
            'pagamentosAnteriores'=>Yii::t('app', 'Pagamentos Anteriores'),
            'pagamento'     =>Yii::t('app', 'Pagamento'),
            'idRecibo'      =>Yii::t('app', 'Id Recibo'),
        ];
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(Autor::className(), ['id' => 'idAutor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacao()
    {
        return $this->hasOne(Publicacao::className(), ['id' => 'idPublicacao']);
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboPublicacao()
    {
        return $this->hasOne(ReciboPublicacao::className(), [
                        'idRecibo' => 'idRecibo',
                        'idPublicacao' => 'idPublicacao',
                        'idAutor'       => 'idAutor',
                        'tipoAutoria'   => 'tipoAutoria']);
    }
    
    /**
     * getPAVC - get PublicacaoAutorVendasComissao
     * @param array $whereCondition ['c.idPublicacao'=> idPublicacao,'c.idAutor'=>idautor, 'c.tipoAutoria'=>tipoAutoria]
     * @return \yii\db\ActiveQuery
     */
    private static function getPAVCminimal($whereCondition) {                       
        $query = (new \yii\db\ActiveQuery(PublicacaoAutorVendasComissao::className()));
        $query=$query
            ->select(['tg.idAutor','tg.tipoAutoria', 'tg.idPublicacao','tg.ISBN','tg.comissao',
                        'dataVenda'=>'max(tg.data)',
            /*            'totalVendas'=>'max(tg.totalVendas)',
                        'qtVendas'=>'max(tg.qtVendas)',
                        'comissaoVendas'=>'max(tg.comissaoVendas)' */
               ])             
            ->from('publicacaoautorvendascomissao tg')                        
            ->groupBy(['tg.idAutor','tg.tipoAutoria', 'tg.idPublicacao','tg.ISBN','tg.comissao']);
        foreach($whereCondition as $k=>$c){
            if($k=='' || $k=='where')
                $query=$query->where($c);
            elseif($k=='and')
                $query=$query->andWhere($c);
            elseif($k=='or')
                $query=$query->orWhere($c);
        }

        $qry=(new \yii\db\ActiveQuery(PublicacaoAutorVendasComissao::className()));
        $qry=$qry
            ->select(['tm2.idAutor','tm2.tipoAutoria', 'tm2.idPublicacao','tm2.ISBN','tm2.comissao','tm2.dataVenda',
                      't2.totalVendas', 't2.qtVendas', 't2.comissaoVendas'])
            ->from(['tm2'=>$query])
            ->innerJoin(['t2'=>'publicacaoautorvendascomissao'],
                        't2.idAutor         = tm2.idAutor      AND
                         t2.tipoAutoria     = tm2.tipoAutoria  AND 
                         t2.idPublicacao    = tm2.idPublicacao AND
                         t2.data            = tm2.dataVenda');
            
        return $qry;
    }

    /**
     * param array $condition ['idAutor'=> $idAutor , 'idPublicacao' => $idPublicacao, 'tipoTiragem'=> $tipoTiragem]
     * @return \yii\db\ActiveQuery
     */   
    private static function getPAVCGeral($c1,$c2,$c3,$idRecibo=[]) {        
        $query = (new \yii\db\ActiveQuery(PublicacaoAutorVendasComissao::className()));        
        $query=$query
                ->select(['tg.idAutor','tg.tipoAutoria','tg.idPublicacao','tg.comissao',
                             'tg.tiragem','tg.valorTotal','tg.comissaoTotal',
                             'vc.dataVenda',
                            'totalVendas'=>'ifnull(vc.totalVendas,0)',
                            'qtVendas'=>'ifnull(vc.qtVendas,0)',
                            'comissaoVendas'=>'ifnull(vc.comissaoVendas,0)',
                            'dataPagamento'=>'p.dataPagamento',
                            'pagamentosAnteriores'=>'ifnull(p.valor,0)',
                            ])
                ->from(['tg'=>PublicacaoAutor::getPATotais($c1)])
                ->leftJoin(['vc' => self::getPAVCminimal($c2)],
                        'vc.idPublicacao = tg.idPublicacao AND
                         vc.tipoAutoria  = tg.tipoAutoria AND
                         vc.idAutor      = tg.idAutor'
                    )
                ->leftJoin(['p' => ReciboPublicacao::getRPPagamentos($c3)], 
                        'p.idPublicacao = tg.idPublicacao AND
                         p.tipoAutoria  = tg.tipoAutoria AND
                         p.idAutor      = tg.idAutor'
                    );
                if($idRecibo!=[]){
                    $query=$query
                        ->addSelect(['idRecibo'=>'rec.id'])
                        ->leftJoin(['rec'=>'recibo'], ['rec.id'=>$idRecibo])
                        ->addSelect(['rec.data','pagamento'=>'ifnull(rt.valor,0)'])
                        ->leftJoin(['rt' => ReciboPublicacao::getRecibo2(['where'=>['rec.id'=>$idRecibo]])], 
                             'rt.idRecibo      = rec.id AND
                             rt.idPublicacao   = tg.idPublicacao AND
                             rt.idAutor        = tg.idAutor AND
                             rt.tipoAutoria    = tg.tipoAutoria');
                }
        return $query;
    }
    
    /**
     * param integer $idAutor 
     * @return \yii\db\ActiveQuery
     */   
    public static function getAutorDetalhes( $idAutor ) {
        return self::getPAVCGeral(['where'=>['t.idAutor'=>$idAutor]],
                                  ['where'=>['tg.idAutor'=>$idAutor]],
                                  ['where'=>['tg.idAutor'=>$idAutor]]);
    }
    
    
    /**
     * param integer $idAutor 
     * param String $data
     * @return \yii\db\ActiveQuery
     */   
    public static function getAutorRecibo( $idAutor, $data ) {        
        $data=\app\components\My::sqlData($data); 
        if (($model = Recibo::findOne(['idAutor'=>$idAutor,'data'=>  $data])) !== null) {
           $qry =  self::getPAVCGeral(
                                  ['where'=>['t.idAutor'=>$idAutor]],
                                  ['where'=>['tg.idAutor'=>$idAutor],'and'=>['<=','tg.data',$data]],
                                  ['where'=>['tg.idAutor'=>$idAutor],'and'=>['<' ,'rec.data',$data]]
                                //    ,$model->id
                                );        
           //CREATE TEMPORARY TABLE IF NOT EXISTS table2 AS (SELECT * FROM table1)
           $table='R'.$model->id.'U'.Yii::$app->user->getId();
           $sql= "DROP TABLE IF EXISTS {table};"
                   . "CREATE TEMPORARY TABLE IF NOT EXISTS {table} AS ({query})";
           $sql= \app\components\My::format($sql, [
                'table'=>$table,
                'query'=>$qry->createCommand()->getRawSql()]);        
           $comm = Yii::$app->db->createCommand($sql);         
           $comm->execute();
           
           $query = (new \yii\db\ActiveQuery(PublicacaoAutorVendasComissao::className()));        
           $query=$query
                ->select(['tg.idAutor','tg.tipoAutoria','tg.idPublicacao','tg.comissao',
                          'tg.tiragem','tg.valorTotal','tg.comissaoTotal',
                          'tg.dataVenda',
                          'tg.totalVendas',
                          'tg.qtVendas',
                          'tg.comissaoVendas',
                          'tg.dataPagamento',
                          'tg.pagamentosAnteriores',
                          'divida'=>'(tg.comissaoVendas-tg.pagamentosAnteriores)',
                          'porLiquidar'=>'(tg.comissaoTotal-tg.pagamentosAnteriores)'])
                ->from(['tg'=>$table])
                ->addSelect(['idRecibo'=>'rec.id'])
                ->leftJoin(['rec'=>'recibo'], ['rec.id'=>$model->id])
                ->addSelect(['rec.data','pagamento'=>'ifnull(rt.valor,0)'])
                ->leftJoin(['rt' => ReciboPublicacao::getRecibo2(['where'=>['rec.id'=>$model->id]])], 
                       'rt.idRecibo      = rec.id AND
                        rt.idPublicacao   = tg.idPublicacao AND
                        rt.idAutor        = tg.idAutor AND
                        rt.tipoAutoria    = tg.tipoAutoria')
                ->where('(tg.comissaoTotal-tg.pagamentosAnteriores)>0')
                ->orderBy('(tg.comissaoVendas-tg.pagamentosAnteriores) desc');
           
           return $query;
           
        }
        return null;
    }
            
        
}


