<?php

namespace app\models;

class User extends \app\models\Utilizador implements \yii\web\IdentityInterface
{
    public $username;             
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $dbUser = User::find()
               ->where([
                   "id" => $id
               ])
               ->one();
       if (!count($dbUser)) {
           return null;
       }
       return $dbUser;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $dbUser = User::find()
               ->where(["accessToken" => $token])
               ->one();
       if (!count($dbUser)) {
           return null;
       }
       return $dbUser;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $dbUser = User::find()
               ->where([
                   "nome" => $username
               ])
               ->one();
       if (!count($dbUser)) {
           return null;
       }
       return $dbUser;
    }
    
    /**
     * Finds user by email
     *
     * @param  string      $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        $dbUser = User::find()
               ->where([
                   "email" => $email
               ])
               ->one();
       if (!count($dbUser)) {
           return null;
       }
       return $dbUser;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authenticationKey;
    }
        
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authenticationKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->senha === $this->crypt($password);
    }
    
    public function afterFind() {                
        $this->username=$this->nome;
        return parent::afterFind();
    }
    
    

}
