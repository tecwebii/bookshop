<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recibopublicacao".
 *
 * @property integer $idRecibo
 * @property integer $idPublicacao
 * @property integer $idAutor
 * @property string $tipoAutoria
 * @property double $valor
 * 
 * @property string $ISBN 
 * @property double $comissao
 * @property string $data
 * @property double $totalVendas
 * @property string $qtVendas
 * @property double $comissaoVendas
 * @property double $porLiquida
 * @property double $divida
 * @property double $pagamento 
 * @property integer $tiragem
 * 
 *
 * @property Publicacaoautor $publicacaoAutor
 * @property Publicacao $publicacao
 * @property Publicacao $autor
 * @property Recibo $recibo
 */
class ReciboPublicacao extends \yii\db\ActiveRecord
{
    public $data,$tiragem,$valorTotal,$comissao,$comissaoTotal,
            $dataVenda,$totalVendas,$qtVendas,$comissaoVendas,
            $dataPagamento,$pagamentosAnteriores,
            $divida,$porLiquidar;
    
    public static function primaryKey() {
        return ['idRecibo','idPublicacao','idAutor','tipoAutoria'];
    }
          
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recibopublicacao';
    }
    
    public function getPagamento() {        
        return $this->valor;
    }
    
    public function setPagamento( $value ) {
        $this->valor = $value;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRecibo', 'idPublicacao', 'idAutor', 'tipoAutoria', 'valor','pagamento'], 'required'],
            [['idRecibo', 'idPublicacao', 'idAutor'], 'integer'],
            [['tipoAutoria'], 'string'],
            [['valor','pagamento'], 'number'],
            [['pagamento'], 'checkPagamento'],
            [['idRecibo', 'idPublicacao', 'idAutor', 'tipoAutoria'], 'unique', 'targetAttribute' => ['idRecibo', 'idPublicacao', 'idAutor', 'tipoAutoria'], 'message' => 'The combination of Id Recibo, Id Publicacao, Id Autor and Tipo Autoria has already been taken.']
        ];
    }
    
    public function checkPagamento($attribute, $params)
    {   
        $porLiquidar = isset($this->porLiquidar) ? $this->porLiquidar : 9999999999999999;                
        if( $this->valor > $porLiquidar ) {
            $this->addError($attribute, Yii::t('app', 'Valor excede o valor por liquidar ({porLiquidar}) (diferença entre o total de comissão e o valor já pago).',['porLiquidar'=>Yii::$app->formatter->asCurrency($porLiquidar)]));                   
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRecibo' => Yii::t('app', 'Id Recibo'),
            'idPublicacao' => Yii::t('app', 'Id Publicacao'),
            'idAutor' => Yii::t('app', 'Id Autor'),
            'tipoAutoria' => Yii::t('app', 'Tipo Autoria'),
            'valor' => Yii::t('app', 'Valor Pago'),
            
            /*public $comissao,$tiragem,$valorTotal,$comissaoTotal,
            $dataVenda,$totalVenda,$qtVendas,$comissaoVendas
            $dataPagamento,$pagamentosAnteriores,
            $divida,$porLiquidar */
            'ISBN'          => Yii::t('app', 'Isbn'),                        
            'comissao'      => Yii::t('app', 'Comissao'),            
            'tiragem'       => Yii::t('app', 'Tiragem'),
            'valorTotal'    => Yii::t('app', 'Valor Total'),
            'comissaoTotal' => Yii::t('app', 'Comissao Total'),
            //
            'dataVenda'     => Yii::t('app', 'Data Venda'),
            'totalVendas'   => Yii::t('app', 'Total Vendas'),
            'qtVendas'      => Yii::t('app', 'Quantidade Vendas'),
            'comissaoVendas'=> Yii::t('app', 'Comissao Vendas'),
            //
            'dataPagamento' => Yii::t('app', 'Data Pagamento Anterior'),
            'pagamentosAnteriores'=>Yii::t('app', 'Pagamentos Anteriores'),
            //
            'divida'        => Yii::t('app', 'Divida'),
            'porLiquidar'   => Yii::t('app', 'Por Liquidar'),
            //            
            'data'          => Yii::t('app', 'Data'),
            'pagamento'     =>Yii::t('app', 'Pagamento'),

        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacao()
    {
        return $this->hasOne(Publicacao::className(), ['id' => 'idPublicacao']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(Autor::className(), ['id' => 'idAutor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacaoAutor()
    {
        return $this->hasOne(Publicacaoautor::className(), ['idPublicacao' => 'idPublicacao', 'idAutor' => 'idAutor', 'tipoAutoria' => 'tipoAutoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecibo()
    {
        return $this->hasOne(Recibo::className(), ['id' => 'idRecibo']);
    }
    
    /**
     * 
     * @param array $whereCondition ['idPublicacao'=> idPublicacao,'idAutor'=>idautor, 'tipoAutoria'=>tipoAutoria]
     * @return \yii\db\ActiveQuery
     */
    public static function getRPPagamentos($whereCondition){
        $query = (new \yii\db\ActiveQuery(ReciboPublicacao::className()));
        $query=$query
            ->select(['tg.idAutor', 'tg.tipoAutoria', 'tg.idPublicacao',
                        'dataPagamento'=>'max(rec.data)',
                        'sum(tg.valor) as valor',
                        ])
            ->from('recibopublicacao tg')            
            ->innerJoin('recibo rec','rec.id = tg.idRecibo')         
            ->groupBy(['tg.idAutor', 'tg.tipoAutoria', 'tg.idPublicacao']);
        foreach($whereCondition as $k=>$c){
            if($k=='' || $k=='where')
                $query=$query->where($c);
            elseif($k=='and')
                $query=$query->andWhere($c);
            elseif($k=='or')
                $query=$query->orWhere($c);
        }
        return $query;
    }            
    
    
    /**
     * 
     * @param array $whereCondition ['idPublicacao'=> idPublicacao,'idAutor'=>idautor, 'tipoAutoria'=>tipoAutoria]
     * @return \yii\db\ActiveQuery
     */
    public static function getRecibo2($whereCondition){
        $query = (new \yii\db\ActiveQuery(ReciboPublicacao::className()));
        $query=$query
            ->select(['tg.idRecibo', 'tg.idAutor', 'tg.tipoAutoria', 'tg.idPublicacao',
                        'rec.data',
                        'sum(ifnull(tg.valor,0)) as valor',
                        ])
            ->from('recibo rec')         
            ->leftJoin('recibopublicacao tg','tg.idRecibo = rec.id')            
            ->groupBy(['tg.idRecibo', 'tg.idAutor', 'tg.tipoAutoria', 'tg.idPublicacao','rec.data']);
        foreach($whereCondition as $k=>$c){
            if($k=='' || $k=='where')
                $query=$query->where($c);
            elseif($k=='and')
                $query=$query->andWhere($c);
            elseif($k=='or')
                $query=$query->orWhere($c);
        }
        //$query->all();
        return $query;
    }            
    
    /**
     * getPAVC - get PublicacaoAutorVendasComissao
     * @param array $whereCondition ['c.idPublicacao'=> idPublicacao,'c.idAutor'=>idautor, 'c.tipoAutoria'=>tipoAutoria]
     * @return \yii\db\ActiveQuery
     */
    private static function getPAVCminimal($whereCondition) {                       
        $query = (new \yii\db\ActiveQuery(ReciboPublicacao::className()));
        $query=$query
            ->select(['tg.idAutor','tg.tipoAutoria', 'tg.idPublicacao','tg.ISBN','tg.comissao',
                        'dataVenda'=>'max(tg.data)',
               ])             
            ->from('publicacaoautorvendascomissao tg')                        
            ->groupBy(['tg.idAutor','tg.tipoAutoria', 'tg.idPublicacao','tg.ISBN','tg.comissao']);
        foreach($whereCondition as $k=>$c){
            if($k=='' || $k=='where')
                $query=$query->where($c);
            elseif($k=='and')
                $query=$query->andWhere($c);
            elseif($k=='or')
                $query=$query->orWhere($c);
        }

        $qry=(new \yii\db\ActiveQuery(ReciboPublicacao::className()));
        $qry=$qry
            ->select(['tm2.idAutor','tm2.tipoAutoria', 'tm2.idPublicacao','tm2.ISBN','tm2.comissao','tm2.dataVenda',
                      't2.totalVendas', 't2.qtVendas', 't2.comissaoVendas'])
            ->from(['tm2'=>$query])
            ->innerJoin(['t2'=>'publicacaoautorvendascomissao'],
                        't2.idAutor         = tm2.idAutor      AND
                         t2.tipoAutoria     = tm2.tipoAutoria  AND 
                         t2.idPublicacao    = tm2.idPublicacao AND
                         t2.data            = tm2.dataVenda');
            
        return $qry;
    }

    /**
     * param array $condition ['idAutor'=> $idAutor , 'idPublicacao' => $idPublicacao, 'tipoTiragem'=> $tipoTiragem]
     * @return \yii\db\ActiveQuery
     */   
    private static function getPAVCGeral($c1,$c2,$c3,$idRecibo=[]) {        
        $query = (new \yii\db\ActiveQuery(ReciboPublicacao::className()));        
        $query=$query
                ->select(['tg.idAutor','tg.tipoAutoria','tg.idPublicacao','tg.comissao',
                             'tg.tiragem','tg.valorTotal','tg.comissaoTotal',
                             'vc.dataVenda',
                            'totalVendas'=>'ifnull(vc.totalVendas,0)',
                            'qtVendas'=>'ifnull(vc.qtVendas,0)',
                            'comissaoVendas'=>'ifnull(vc.comissaoVendas,0)',
                            'dataPagamento'=>'p.dataPagamento',
                            'pagamentosAnteriores'=>'ifnull(p.valor,0)',
                            ])
                ->from(['tg'=>PublicacaoAutor::getPATotais($c1)])
                ->leftJoin(['vc' => self::getPAVCminimal($c2)],
                        'vc.idPublicacao = tg.idPublicacao AND
                         vc.tipoAutoria  = tg.tipoAutoria AND
                         vc.idAutor      = tg.idAutor'
                    )
                ->leftJoin(['p' => ReciboPublicacao::getRPPagamentos($c3)], 
                        'p.idPublicacao = tg.idPublicacao AND
                         p.tipoAutoria  = tg.tipoAutoria AND
                         p.idAutor      = tg.idAutor'
                    );
                if($idRecibo!=[]){
                    $query=$query
                        ->addSelect(['idRecibo'=>'rec.id'])
                        ->leftJoin(['rec'=>'recibo'], ['rec.id'=>$idRecibo])
                        ->addSelect(['rec.data','pagamento'=>'ifnull(rt.valor,0)'])
                        ->leftJoin(['rt' => ReciboPublicacao::getRecibo2(['where'=>['rec.id'=>$idRecibo]])], 
                             'rt.idRecibo      = rec.id AND
                             rt.idPublicacao   = tg.idPublicacao AND
                             rt.idAutor        = tg.idAutor AND
                             rt.tipoAutoria    = tg.tipoAutoria');
                }
        return $query;
    }
    
    /**
     * param integer $idAutor 
     * @return \yii\db\ActiveQuery
     */   
    public static function getAutorDetalhes( $idAutor ) {
        return self::getPAVCGeral(['where'=>['t.idAutor'=>$idAutor]],
                                  ['where'=>['tg.idAutor'=>$idAutor]],
                                  ['where'=>['tg.idAutor'=>$idAutor]]);
    }
    
    
    /**
     * param integer $idAutor 
     * param String $data
     * @return \yii\db\ActiveQuery
     */   
    public static function getAutorRecibo( $idAutor, $data ) {        
        $data=\app\components\My::sqlData($data); 
        if (($model = Recibo::findOne(['idAutor'=>$idAutor,'data'=>  $data])) !== null) {
           $qry =  self::getPAVCGeral(
                                  ['where'=>['t.idAutor'=>$idAutor]],
                                  ['where'=>['tg.idAutor'=>$idAutor],'and'=>['<=','tg.data',$data]],
                                  ['where'=>['tg.idAutor'=>$idAutor],'and'=>['<>' ,'rec.data',$data]]
                                //    ,$model->id
                                );        
           //CREATE TEMPORARY TABLE IF NOT EXISTS table2 AS (SELECT * FROM table1)
           $table='R'.$model->id.'U'.Yii::$app->user->getId();
           $sql= "DROP TABLE IF EXISTS {table};"
                   . "CREATE TABLE IF NOT EXISTS {table} AS ({query})";
           $sql= \app\components\My::format($sql, [
                'table'=>$table,
                'query'=>$qry->createCommand()->getRawSql()]);        
           $comm = Yii::$app->db->createCommand($sql);         
           $comm->execute();
           
           $query = (new \yii\db\ActiveQuery(ReciboPublicacao::className()));        
           $query=$query
                ->select(['tg.idAutor','tg.tipoAutoria','tg.idPublicacao','tg.comissao',
                          'tg.tiragem','tg.valorTotal','tg.comissaoTotal',
                          'tg.dataVenda',
                          'tg.totalVendas',
                          'tg.qtVendas',
                          'tg.comissaoVendas',
                          'tg.dataPagamento',
                          'tg.pagamentosAnteriores',
                          'divida'=>'(tg.comissaoVendas-tg.pagamentosAnteriores)',
                          'porLiquidar'=>'(tg.comissaoTotal-tg.pagamentosAnteriores)'])
                ->from(['tg'=>$table])
                ->addSelect(['idRecibo'=>'rec.id'])
                ->leftJoin(['rec'=>'recibo'], ['rec.id'=>$model->id])
                ->addSelect(['rec.data','pagamento'=>'ifnull(rt.valor,0)'])
                ->leftJoin(['rt' => ReciboPublicacao::getRecibo2(['where'=>['rec.id'=>$model->id]])], 
                       'rt.idRecibo      = rec.id AND
                        rt.idPublicacao   = tg.idPublicacao AND
                        rt.idAutor        = tg.idAutor AND
                        rt.tipoAutoria    = tg.tipoAutoria')
                ->where('(tg.comissaoTotal-tg.pagamentosAnteriores)>0 || tg.dataPagamento>rec.data')
                ->orderBy('(tg.comissaoVendas-tg.pagamentosAnteriores) desc');
           
           return $query;
           
        }
        return null;
    }
    
    
}
