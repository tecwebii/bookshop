<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Publicacao;

/**
 * PublicacaoSearch represents the model behind the search form about `app\models\Publicacao`.
 */
class PublicacaoSearch extends Publicacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'taxaIva', 'idEditora'], 'integer'],
            [['tipoPublicacao', 'ISBN', 'descricao','descricaoHTML','titulo', 'subTitulo', 'dataEdicao'], 'safe'],
            [['pVenda'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Publicacao::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'dataEdicao' => $this->dataEdicao,
            'pVenda' => $this->pVenda,
            'taxaIva' => $this->taxaIva,
            'idEditora' => $this->idEditora,
        ]);

        $query->andFilterWhere(['like', 'tipoPublicacao', $this->tipoPublicacao])
            ->andFilterWhere(['like', 'ISBN', $this->ISBN])
            ->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', "concat(titulo,' ',subTitulo)", $this->descricao])
            ->andFilterWhere(['like', "concat(titulo,' ',subTitulo)", $this->descricaoHTML])
            ->andFilterWhere(['like', 'titulo', $this->titulo])
            ->andFilterWhere(['like', 'subTitulo', $this->subTitulo]);
        $dataProvider->setSort([
            'attributes'=>[
                'descricao'=>
                        [   'asc'  => ["concat(titulo,' ',subTitulo)" => SORT_ASC ],
                            'desc' => ["concat(titulo,' ',subTitulo)" => SORT_DESC ],
                ],
                'descricaoHTML'=>[  
                            'asc'  => ["concat(titulo,' ',subTitulo)" => SORT_ASC ],
                            'desc' => ["concat(titulo,' ',subTitulo)" => SORT_DESC ],
                ]
            ]
        ]);
        
        return $dataProvider;
    }
}
