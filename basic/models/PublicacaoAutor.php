<?php

namespace app\models;

use Yii;
use \yii\helpers\Json;
use \yii\helpers\BaseInflector;

/**
 * This is the model class for table "publicacaoautor".
 *
 * @property integer $id 
 * @property integer $idPublicacao
 * @property integer $idAutor
 * @property string $tipoAutoria
 * @property double $comissao
 * @property string $contrato
 *
 * @property Autor $autor
 * @property Publicacao $publicacao
 * @property Recibopublicacao[] $recibopublicacaos
 * @property PublicacaoAutor $totais

 */
class PublicacaoAutor extends \yii\db\ActiveRecord
{
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicacaoautor';
    }        

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPublicacao', 'idAutor', 'tipoAutoria', 'comissao'], 'required'],
            [['idPublicacao', 'idAutor'], 'integer'],
            [['tipoAutoria'], 'string'],
            [['comissao'], 'number', 'min'=>0,'max'=>100],            
            [['contrato'], 'file'],   
            [['contrato'], 'string', 'max' => 255],   
//            [['idAutor'], 'unique', 'targetAttribute' => ['idPublicacao', 'idAutor', 'tipoAutoria']],
            [['idAutor'], 'checkUnique']
        ];
    }
    
   public function checkUnique($attribute, $params) {
        $m = PublicacaoAutor::find()                   
                   ->where(['idPublicacao' =>$this->idPublicacao])
                ->andWhere(['idAutor'      =>$this->idAutor])
                ->andWhere(['tipoAutoria'  =>$this->tipoAutoria]);
        $a=$m->createCommand()->getRawSql();
        $aOnInsert =  $this->isNewRecord && ($m->one()!=null);
        $aOnUpdate = !$this->isNewRecord && ($m->andWhere(['<>','id',$this->id])->one()!=null);
        if( $aOnInsert || $aOnUpdate )  {            
            $this->addError($attribute, Yii::t('app', 'ID {tipoAutoria} “{id}” já foi atribuido.',
                                    ['tipoAutoria'=>$this->tipoAutoria,
                                     'id'=>$this->idAutor]));
        }
    } 

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'idPublicacao' => Yii::t('app', 'Id Publicacao'),
            'idAutor' => Yii::t('app', 'Id Autor'),
            'tipoAutoria' => Yii::t('app', 'Tipo Autoria'),
            'comissao' => Yii::t('app', 'Comissao '.$this->tipoAutoria),
            'contrato' => Yii::t('app', 'Contrato'),
            'contratoHTML' => Yii::t('app', 'Contrato'),
            'fileName' => Yii::t('app', 'Nome Ficheiro'),
            'originalFileName' => Yii::t('app', 'Ficheiro'),
            'tiragem'       => Yii::t('app', 'Tiragem'),
            'valorTotal'    => Yii::t('app', 'Ficheiro'),
            'comissaoTotal' => Yii::t('app', 'Comissao '.$this->tipoAutoria.' Total'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(Autor::className(), ['id' => 'idAutor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacao()
    {
        return $this->hasOne(Publicacao::className(), ['id' => 'idPublicacao']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecibopublicacaos()
    {
        return $this->hasMany(Recibopublicacao::className(), ['idPublicacao' => 'idPublicacao', 'idAutor' => 'idAutor']);
    }
    
    public function getFileName() {
        $ret='';
        if( $this->contrato!='') {
            $arr=JSON::decode($this->contrato);
            $ret=isset($arr['file']) ? $arr['file'] : '';
        }
        return $ret;        
    }
    
    public function getBuildFileName(){        
        return BaseInflector::slug(
                'Contrato'.' '.$this->tipoAutoria.' '.BaseInflector::id2camel($this->autor->getNome(),' ')
                .' Publicacao '.BaseInflector::id2camel($this->publicacao->getDescricao(' '))
                ,'_',false);
    }
    
    public function getOriginalFileName() {
        $ret='';
        if( $this->contrato!='') {
            $arr=JSON::decode($this->contrato);
            $ret=isset($arr['original']) ? $arr['original'] : '';
        }
        return $ret;        
    }
        
    public function setFileName( $file ) {
        $arr=[];
        if( $this->contrato!='') 
            $arr=JSON::decode($this->contrato);
        $arr['file']=$file;
        $this->contrato=JSON::encode($arr);
    }
    
    public function setOriginalFileName( $file ) {
        $arr=[];
        if( $this->contrato!='') 
            $arr=JSON::decode($this->contrato);
        $arr['original']=$file;
        $this->contrato=JSON::encode($arr);
    }
    
    public function getContratoHTML(){
        $original=  $this->getOriginalFileName();
        return $original==''?''
                : \yii\helpers\Html::a($original,
                            ['/publicacaoautor/download',
                            'idPublicacao'=>$this->idPublicacao,
                            'idAutor'     =>$this->idAutor,
                            'tipoAutoria' =>$this->tipoAutoria, ],
                            ['class' => 'btn btn-primary',
                             'target'=> "_blank"]);
    }
    
    
    public function getTotais() {
        self::getPATotais(['idPublicacao'  => $this->idPublicacao,
                         'idAutor'       => $this->idautor, 
                         'tipoAutoria'   => $this->tipoAutoria]);
    }
    
    
    /**
     * 
     * @param array $whereCondition ['idPublicacao'=> idPublicacao,'idAutor'=>idautor, 'tipoAutoria'=>tipoAutoria]
      * @return \yii\db\ActiveQuery
     */
    public static function getPATotais( $whereCondition ){

        $query = (new \yii\db\ActiveQuery(PublicacaoAutor::className()));
        $query=$query
            ->select(['t.idAutor', 't.tipoAutoria', 't.idPublicacao','t.comissao',
                        'dp.tiragem',
                        'valorTotal'=>'(dp.tiragem*p.pVenda)',
                        'comissaoTotal'=>'(if(dp.tiragem*p.pVenda*t.comissao=0,0,dp.tiragem*p.pVenda*t.comissao/100))'
                        ])
            ->from(['t'=>'publicacaoautor']) 
            ->innerJoin(['p'=>'publicacao'],'p.id = t.idPublicacao')
            ->innerJoin(['dp'=>'dadosproducaolivro'],'dp.id = t.idPublicacao');
        foreach($whereCondition as $k=>$c){
            if($k=='' || $k=='where')
                $query=$query->where($c);
            elseif($k=='and')
                $query=$query->andWhere($c);
            elseif($k=='or')
                $query=$query->orWhere($c);
        }

        return $query;
    }
    
    public static function insertIfNotExist($idPublicacao,$idAutor,$tipoAutoria) {        
        $model = PublicacaoAutor::find()->where([
                                    'idPublicacao'  =>$idPublicacao,
                                    'idAutor'       =>$idAutor,
                                    'tipoAutoria'   =>$tipoAutoria])->one();        
        if($model==null){
            $model= new PublicacaoAutor();
            $model->idPublicacao= $idPublicacao;
            $model->idAutor     = $idAutor;            
            $model->tipoAutoria = $tipoAutoria;
            $model->comissao    = 0;
            $model->save();
        }
        return $model->id;
    }
}
