<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CodigoPostal;

/**
 * CodigoPostalSearch represents the model behind the search form about `app\models\CodigoPostal`.
 */
class CodigoPostalSearch extends CodigoPostal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['id_distrito', 'id_concelho', 'cod_localidade', 'localidade', 'arteria_cod', 'arteria_tipo', 'pri_prep', 'arteria_titulo', 'seg_prep', 'arteria_designacao', 'morada_local', 'troco', 'porta', 'cliente', 'cp4', 'cp3', 'cp_designacao', 'morada', 'codpostal', 'distrito', 'concelho'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CodigoPostal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'id_distrito', $this->id_distrito])
            ->andFilterWhere(['like', 'id_concelho', $this->id_concelho])
            ->andFilterWhere(['like', 'cod_localidade', $this->cod_localidade])
            ->andFilterWhere(['like', 'localidade', $this->localidade])
            ->andFilterWhere(['like', 'arteria_cod', $this->arteria_cod])
            ->andFilterWhere(['like', 'arteria_tipo', $this->arteria_tipo])
            ->andFilterWhere(['like', 'pri_prep', $this->pri_prep])
            ->andFilterWhere(['like', 'arteria_titulo', $this->arteria_titulo])
            ->andFilterWhere(['like', 'seg_prep', $this->seg_prep])
            ->andFilterWhere(['like', 'arteria_designacao', $this->arteria_designacao])
            ->andFilterWhere(['like', 'morada_local', $this->morada_local])
            ->andFilterWhere(['like', 'troco', $this->troco])
            ->andFilterWhere(['like', 'porta', $this->porta])
            ->andFilterWhere(['like', 'cliente', $this->cliente])
            ->andFilterWhere(['like', 'cp4', $this->cp4])
            ->andFilterWhere(['like', 'cp3', $this->cp3])
            ->andFilterWhere(['like', 'cp_designacao', $this->cp_designacao])
            ->andFilterWhere(['like', 'morada', $this->morada])
            ->andFilterWhere(['like', 'codpostal', $this->codpostal])
            ->andFilterWhere(['like', 'distrito', $this->distrito])
            ->andFilterWhere(['like', 'concelho', $this->concelho]);

        return $dataProvider;
    }
}
