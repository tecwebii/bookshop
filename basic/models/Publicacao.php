<?php

namespace app\models;

use Yii;
use DateTime;

/**
 * This is the model class for table "publicacao".
 *
 * @property integer $id
 * @property string $tipoPublicacao
 * @property string $ISBN
 * @property string $titulo
 * @property string $subTitulo
 * @property string $dataEdicao
 * @property double $pVenda
 * @property integer $taxaIva
 * @property integer $idEditora
 *
 * @property DadosProducaoLivro $dadosProducao
 * @property EvolucaoVendas[] $evolucaoVendas
 * @property Editora $editora
 * @property PublicacaoAutor[] $autores
 * @property PublicacaoAutor[] $coordenadores
 * @property PublicacaoCustos[] $custos
 */
class Publicacao extends \yii\db\ActiveRecord
{
    private $c_descricao,$c_descricaoHTML;
    
    public function init() {
        $this->tipoPublicacao = "Livro";
        return parent::init();
    }
    
    
    /**
     * 
     * @param integer $id
     * @return \yii\db\Query
     */
    private static function findChartVendas($id) {
        $m = new \yii\db\Query;
        $m->select(['v.data','v.totalVendas','t.total','v.qtVendas','t.tiragem'])
            ->from(['v'=>'publicacaoautorvendas'])
            ->innerJoin(['t'=>'publicacaototais'],'t.id=v.idPublicacao')
            ->where(['v.idPublicacao'=>$id])
            ->orderBy('v.data');
        return $m;
    }
    
    /**
     * 
     * @return \yii\db\Query
     */
    public function getChart() {
        return Publicacao::findChartVendas($this->id);
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publicacao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipoPublicacao'], 'string'],
            [['tipoPublicacao','ISBN','titulo','dataEdicao', 'pVenda'], 'required'],
            [['ISBN'], 'unique'],
            [['dataEdicao'], 'safe'],
            [['pVenda'], 'number','min'=>0],
            [['taxaIva', 'idEditora'], 'integer'],
            [['ISBN'], 'string', 'max' => 13],
            [['titulo', 'subTitulo'], 'string', 'max' => 100],
            ['idEditora', 'exist','targetClass'=>'\app\models\Editora','targetAttribute'=>'id'],
            [['ISBN'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tipoPublicacao' => Yii::t('app', 'Tipo Publicacao'),
            'ISBN' => Yii::t('app', 'Isbn'),
            'titulo' => Yii::t('app', 'Titulo'),
            'subTitulo' => Yii::t('app', 'Sub Titulo'),
            'dataEdicao' => Yii::t('app', 'Data Edicao'),
            'pVenda' => Yii::t('app', 'P Venda'),
            'taxaIva' => Yii::t('app', 'Taxa Iva'),
            'idEditora' => Yii::t('app', 'Id Editora'),
            'descricao' => Yii::t('app', 'Titulo'),
            'descricaoHTML' => Yii::t('app', 'Titulo'),            
        ];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getDadosProducao()
    {
        return $this->hasOne(DadosProducaoLivro::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getEvolucaoVendas()
    {
        return $this->hasMany(EvolucaoVendas::className(), ['ISBN' => 'ISBN']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getEditora()
    {
        return $this->hasOne(Editora::className(), ['id' => 'idEditora']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getAutores()
    {        
        return $this->hasMany(PublicacaoAutor::className(), ['idPublicacao' => 'id'])
                    ->andWhere(['tipoAutoria'=>'Autor']);

    }
    
    /**
     * @return \yii\db\ActiveRecord
     */
    public function getCoordenadores()
    {
        return $this->hasMany(PublicacaoAutor::className(), ['idPublicacao' => 'id'])
                    ->andWhere(['tipoAutoria'=>'Coordenador']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getCusto()
    {        
        return $this->hasMany(PublicacaoCusto::className(), ['idPublicacao' => 'id']);
    }
    
  
    public function beforeSave($insert) {        
        $this->dataEdicao=  \app\components\My::sqlData($this->dataEdicao);
        return parent::beforeSave($insert);
    }

    public function beforeValidate() {
        $a2=is_numeric("2014-02-03");
        $a=is_numeric($this->dataEdicao);
        $a1=intval($this->dataEdicao);
        if(is_numeric($this->dataEdicao) && intval($this->dataEdicao)>1930) {
            $this->dataEdicao = intval($this->dataEdicao).'-01-01';
        }

        $this->dataEdicao=  \app\components\My::sqlData($this->dataEdicao);
        return parent::beforeValidate();
    }
    public function afterValidate() {
        $this->dataEdicao=  \app\components\My::phpData($this->dataEdicao);
        return parent::afterValidate();
    }
    public function afterFind() {        
        $this->dataEdicao=  \app\components\My::phpData($this->dataEdicao);
        return parent::afterFind();
    }
    
    public function getDescricao($separador="\n") {
        if (isset($this->c_descricao))
            return $this->c_descricao;
        else
        return $this->titulo. ( $this->subTitulo!='' ? $separador .$this->subTitulo : '');
    }
        
    public function setDescricao( $descricao ) {
        $this->c_descricao = $descricao;
    }
    
    public function getDescricaoHTML() {
        if (isset($this->c_descricaoHTML))
            return $this->c_descricaoHTML;
        else
        return ( $this->titulo   !='' ? '<b>'.$this->titulo.'</b>':$this->titulo) . 
               ( $this->subTitulo!='' ? '<br>'.$this->subTitulo : '');
    }
        
    public function setDescricaoHTML( $descricao ) {
        $this->c_descricaoHTML = $descricao;
    }
    
    public function fixVal( $fieldName, $val ){
        
    }

}
