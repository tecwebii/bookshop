<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \yii\helpers\Url;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $detalhes yii\db\ActiveRecord */

$this->title = Yii::t('app', 'Detalhes Autor');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Autor-details">

    <h2><?= Html::encode($this->title) ?></h2>
    <?= GridView::widget([
        'dataProvider' => new activeDataProvider([
                                'query' => $detalhes,
                                'pagination' => [
                                    'pageSize' => 9999,
                                ],
                            ]),      
        'columns' => [
            [   'class' => 'yii\grid\SerialColumn'],                            
                'tipoAutoria',                               
                'idPublicacao',
                'tiragem',
                'valorTotal',
                'comissao',
                'comissaoTotal',
		'totalVendas',		
                'qtVendas',
                'qtVendas',
                'comissaoVendas',
                'pagamentosAnteriores',
        ],
    ]); ?>
</div>
