<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EvolucaoVendas;

/**
 * EvolucaoVendasSearch represents the model behind the search form about `app\models\EvolucaoVendas`.
 */
class EvolucaoVendasSearch extends EvolucaoVendas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
         //   [['id', 'quantidade'], 'integer'],
            [['ISBN', 'data','titulo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EvolucaoVendas::findWithPublicacao();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'data' => \app\components\My::sqlData($this->data),
            'quantidade' => $this->quantidade,
        ]);

        $query->andFilterWhere(['like', 'e.ISBN', $this->ISBN])
              ->andFilterWhere(['like', "concat(p.titulo,' ',p.subTitulo)", $this->titulo]);
        
        $dataProvider->setSort([
            'attributes'=>[
                'id'=>
                        [   'asc'  => ["e.id" => SORT_ASC ],
                            'desc' => ["e.id" => SORT_DESC ],
                ],      
                'ISBN'=>
                        [   'asc'  => ["e.isbn" => SORT_ASC ],
                            'desc' => ["e.isbn" => SORT_DESC ],
                ],      
                'data'=>
                        [   'asc'  => ["e.data" => SORT_ASC ],
                            'desc' => ["e.data" => SORT_DESC ],
                ],      
                'titulo'=>
                        [   'asc'  => ["concat(titulo,' ',subTitulo)" => SORT_ASC ],
                            'desc' => ["concat(titulo,' ',subTitulo)" => SORT_DESC ],
                ],                
            ]
        ]);

        return $dataProvider;
    }
}
