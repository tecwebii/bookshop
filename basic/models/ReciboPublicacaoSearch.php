<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReciboPublicacao;

/**
 * ReciboPublicacaoSearch represents the model behind the search form about `app\models\ReciboPublicacao`.
 */
class ReciboPublicacaoSearch extends ReciboPublicacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRecibo', 'idPublicacao', 'idAutor'], 'integer'],
            [['tipoAutoria'], 'safe'],
            [['valor'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReciboPublicacao::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idRecibo' => $this->idRecibo,
            'idPublicacao' => $this->idPublicacao,
            'idAutor' => $this->idAutor,
            'valor' => $this->valor,
        ]);

        $query->andFilterWhere(['like', 'tipoAutoria', $this->tipoAutoria]);

        return $dataProvider;
    }
}
