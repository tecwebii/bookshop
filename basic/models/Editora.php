<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "editora".
 *
 * @property integer $id
 * @property string $nome
 *
 * @property Publicacao[] $publicacoes
 */
class Editora extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'editora';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 50],
            [['nome'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nome' => Yii::t('app', 'Nome'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacaos()
    {
        return $this->hasMany(Publicacao::className(), ['idEditora' => 'id']);
    }
    
    public static function insertIfNotExist($editora) {
        $model = Editora::find()->where(['nome'=>$editora])->one();        
        if($model==null){
            $model= new Editora();
            $model->nome = $editora;
            $model->save();            
        }
        return $model->id;
    }
         
}
