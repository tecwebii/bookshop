<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dadosproducaolivro".
 *
 * @property integer $id
 * @property integer $numeroPaginas
 * @property integer $peso
 * @property integer $tiragem
 * @property string $grafica
 * @property integer $idPaginador
 * @property integer $idDesignerCapa
 *
 * @property Publicacao $publicacao
 * @property Recurso $paginador
 * @property Recurso $designerCapa
 */
class DadosProducaoLivro extends \yii\db\ActiveRecord
{
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dadosproducaolivro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tiragem'], 'required'],
            [['tiragem'], 'integer', 'min'=>0],
            [['id','numeroPaginas', 'peso', 'idPaginador', 'idDesignerCapa'], 'integer'],
            [['grafica'], 'string', 'max' => 50],
            ['idPaginador', 'exist','targetClass'=>'\app\models\Recurso','targetAttribute'=>'id'],
            ['idDesignerCapa' ,'exist','targetClass'=>'\app\models\Recurso','targetAttribute'=>'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'numeroPaginas' => Yii::t('app', 'Numero Paginas'),
            'peso' => Yii::t('app', 'Peso'),
            'tiragem' => Yii::t('app', 'Tiragem'),
            'grafica' => Yii::t('app', 'Grafica'),
            'idPaginador' => Yii::t('app', 'Id Paginador'),
            'idDesignerCapa' => Yii::t('app', 'Id Designer Capa'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicacao()
    {
        return $this->hasOne(Publicacao::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaginador()
    {
        return $this->hasOne(Recurso::className(), ['id' => 'idPaginador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesignerCapa()
    {
        return $this->hasOne(Recurso::className(), ['id' => 'idDesignerCapa']);
    }
}
