<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EvolucaoVendas;

/**
 * EvolucaoVendasSearch represents the model behind the search form about `app\models\EvolucaoVendas`.
 */
class EvolucaoVendasImporta extends EvolucaoVendas
{    
    public $ficheiro,$dataImportacao;
            
    public function attributeLabels() {
        $ret =array_merge(
                [   'ficheiro'       => Yii::t('app','Ficheiro'),
                    'dataImportacao' => Yii::t('app','Data'),
                ],
                parent::attributeLabels());
        return $ret;
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['ficheiro','dataImportacao'], 'required'],
            [['ficheiro'], 'file'],
            [['id', 'quantidade'], 'integer'],
            [['ISBN', 'data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
}

