<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recurso".
 *
 * @property integer $id
 * @property string $nome
 * @property string $tipoRecurso
 *
 * @property Dadosproducaolivro[] $dadosproducaolivros
 */
class Recurso extends \yii\db\ActiveRecord
{
    const TIPO_Paginacao='Paginação';
    const TIPO_Capa     ='Capa';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recurso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'tipoRecurso'], 'required'],
            [['tipoRecurso'], 'string'],
            [['nome'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nome' => Yii::t('app', 'Nome'),
            'tipoRecurso' => Yii::t('app', 'Tipo Recurso'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDadosproducaolivros()
    {
        return $this->hasMany(Dadosproducaolivro::className(), ['idDesignerCapa' => 'id']);
    }
}
