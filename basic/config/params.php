<?php

return [
    'adminEmail' => 'admin@example.com',  
    'company'    => 'Escolar Editora',
    'copyright'  => 'Europeia',
    'imageLivroURL' => [
        'small'  =>'http://covers.openlibrary.org/b/isbn/{isbn}-S.jpg',
        'medium' =>'http://covers.openlibrary.org/b/isbn/{isbn}-M.jpg',
        'large'  =>'http://covers.openlibrary.org/b/isbn/{isbn}-L.jpg',
        ],
    
    'maskMoneyOptions' => [
        'prefix' => '',
        'suffix' => '€',
        'affixesStay' => true,
        'thousands' => ',',
        'decimal' => '.',
        'precision' => 2, 
        'allowZero' => false,
        'allowNegative' => false,
    ]
];
