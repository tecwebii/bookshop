$(".panelLink").dblclick(function(event){
    var panelId =  $(event.currentTarget).find('a').attr('href');
    var panel    =  $(panelId);
    if( panel.length>0 && panel.data('url')!=undefined )
        loadIntoPanel( panel, panel.data('url') ,'get', true);
});
function addUrlParameter(urlIn,paramName,paramValue){
    return url=urlIn+(urlIn.indexOf('?')>0 ?'&':'?')+paramName+'='+paramValue;    
}        
function removeUrlParameter(urlIn,paramName){
    var t1=urlIn.split('?');
    var url=t1[0],param,queryString='';
    if(t1.length>1){
        param=t1[1].split('&');
        $.each(param,function(index,p) {
            if(p.substring(0,paramName.length+1) != paramName+'='){
                queryString+= (queryString==''?'':'&') + p;
            }
        });
    }           
    url= queryString!='' ? url+'?'+queryString : url;
    return url;
}
function GetURLParameter(urlIn,paramName){
    var t1=urlIn.split('?');
    var url=t1[0],param,ret='';
    if(t1.length>1){
        param=t1[1].split('&');
        $.each(param,function(index,p) {
            if(p.substring(0,paramName.length+1) == paramName+'='){
                ret =decodeURIComponent(p.split('=')[1]);                
                return false;
            }
        });
    }           
    return ret;
}
    
$(window).on("popstate", function(ev) {
    var _stateObj = eval('['+GetURLParameter(document.URL,'_stateObj')+'][0]');
    if(_stateObj)
        loadIntoPanel( $('#'+_stateObj.panelId), _stateObj.href ,'GET',false);                    
    activatePanel( $('#'+_stateObj.panelId));
});     
var activatePanel=function(panel){
    var index = $(panel).closest('.ui-tabs').find('a[href=#'+$(panel).attr('id')+']').parent().index();
    window.setTimeout(function(){
            $(panel).closest('.ui-tabs').tabs("option", "active", index);
    },500);              
}
var addHistory=function( panel, href) {    
    var _stateObj=[{panelId : panel.attr('id'), href : href}][0];
    var url =JSON.stringify(_stateObj);
    //var url =encodeURIComponent(JSON.stringify(_stateObj));
    var historyUrl = addUrlParameter(removeUrlParameter(document.URL,'_stateObj'),'_stateObj',url);
    history.pushState( _stateObj, $('h1').eq(0).text(), historyUrl );
}

var loadIntoPanel=function( panel, href ,method, doPushHistory) {
    if(method.toLowerCase()=='get' && doPushHistory) {
        addHistory(panel, href);        
    }
    var data=[];
    if(method.toLowerCase()=='post'){
        data[yii.getCsrfParam()]=yii.getCsrfToken();
    }
    if( href!= '') {
        $.ajax({
            url:href,
            type:method,
            data:data,
            dataType: 'html',
            complete:function(jqXHR, textStatus ){
                var redir=jqXHR.getResponseHeader("X-Redirect");
                if (redir!=null) {
                    jqXHR.abort();
                    window.stop();
                    return false;
                }                                        
            },
            success:function(data, textStatus, jqXHR ){
                loadData(data,href,panel);                
            }
        });        
    }
},
loadData=function(data, fromUrl, intoPanel){    
    if(data.trim()=='' ) { // && bfromUrl!=$(intoPanel).data('url') 
        loadIntoPanel(intoPanel,intoPanel.data('url'),'GET',true);
    }else{
        intoPanel.data('loaded',fromUrl);
        intoPanel.html(data);
        intoPanel.find('h1').hide();
        activatePanel(intoPanel);                    
        replace_links(intoPanel); 
    }
},
handlerClick = function (event, panel) {
    var me = $(event.currentTarget),
        method = me.data('method'),
        message = me.data('confirm');

    if (message !== undefined) {
        yii.confirm(message, function () {
            loadIntoPanel( $(panel), me.attr('href') , method==undefined?'GET':method , true);            
        });
    } else {
        loadIntoPanel( $(panel), me.attr('href') , 'GET', true);            
    }
    event.stopImmediatePropagation();
    return false;
};
replace_links=function(panel){
    $(panel).find('a').each(function(index){                
        if( $(this).attr('target')=='_self' || $(this).attr('target')==undefined )
            $(this).click( function(event){             
                return handlerClick( event, panel );            
            });
    }),
progressHandlingFunction= function(e){
        if(e.lengthComputable){
            $('progress').attr({value:e.loaded,max:e.total});
        }
    };
    $(panel).find('form').each(function(index){                
        $(this).submit( function(event) {
            var postData =($(this).find('input[type=file]').length>0)
                        ? new FormData($(this)[0])
                        : $(this).serialize();            
            var form=$(this);                
            $.ajax({
                url: form.attr('action'),
                type: (form.attr('method')!=''?form.attr('method'):'post').toUpperCase(),
                data: postData,
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                contentType: ($(this).find('input[type=file]').length>0 ? false : 'application/x-www-form-urlencoded; charset=UTF-8'),
                cache: false,
                processData:false,
                complete:function(jqXHR, textStatus ){
                    var redir=jqXHR.getResponseHeader("X-Redirect");
                    if (redir!=null) {
                        jqXHR.abort();
                        window.stop();
                        return false;
                    }                                        
                },
                success: function (data, textStatus, jqXHR ){
                    loadData(data,form.attr('action'),panel);                    
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
            return false;
        });
    });
}
    
if( (stUrl=GetURLParameter(document.URL,'_stateObj'))!='' ) 
    $(document).trigger('popstate');

