<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\filters\AccessControl;
//use app\components\MyController;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use \yii\web\Controller;
use app\components\My;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        $ret = parent::beforeAction($action);        
        $count = User::find()->count();
        if ( $count==0 ) {
            Yii::$app->session->setFlash('Redirecionamento automatico', 'Tabela de utilizadores vazia');
            return $action->controller->redirect('/utilizador');
            //if ($action->id!='create'){}
        }
        return $ret;
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect('/'.$this->action->controller->id.'/login');
        }else{
            return $this->redirect('/publicacao');
        }
    }        

    /** Yii::$app->getSecurity() yii\base\Security */
    public function actionLogin()
    {        
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
        
        /*
        if (! \Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->getView()->registerJsFile(Yii::$app->request->baseUrl.'/js/aes.js');    
        $this->getView()->registerJsFile(Yii::$app->request->baseUrl.'/js/aes-json-format.js');        
        
        $model = new LoginForm();        
        $formValues=Yii::$app->request->post();
        $values = [];
        if (isset($formValues['json']) ) {
            $decrypVal = My::cryptoJsAesDecrypt( $formValues['_csrf'],$formValues['json']);            
            parse_str( $decrypVal  , $values );
        };
        
        if( $values!=[] && 
            count( explode('@', $values['LoginForm']['username'] )) == 2 &&
            User::findByUsername ($values['LoginForm']['username']) == null &&
            ($user= User::findByEmail ($values['LoginForm']['username']) ) != null) {        
            $values['LoginForm']['username'] = $user->username;
        }
        if($values!=[]){
            if ($model->load($values) && $model->login() ) {
                return $this->goBack();                    
            }                           
        }
        return $this->render('login', [
            'model' => $model,
        ]);*/
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
