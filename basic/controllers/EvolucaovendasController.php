<?php

namespace app\controllers;

use Yii;
use app\models\Publicacao;
use app\models\EvolucaoVendas;
use app\models\EvolucaoVendasSearch;
use app\models\EvolucaoVendasImporta;
use app\components\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use alexgx\phpexcel;
use PHPExcel_IOFactory;
use app\components\MyExcel;
use yii\helpers\Json;
use yii\web\HttpException;
use \yii\grid\GridView;
use \yii\data\ArrayDataProvider;

/**
 * EvolucaoVendasController implements the CRUD actions for EvolucaoVendas model.
 */
class EvolucaovendasController extends MyController
{
    public $uploadFolder;
    
    public function init() {
        $this->uploadFolder = Yii::getAlias('@app') . "\upload_vendas\\";
        parent::init();
    }
        
    private function saveFile($model){
        if ($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model, 'ficheiro');              
            if($file!=null) {            
                $date = date_create($model->dataImportacao);
                $model->ficheiro='Vendas_'.date_format($date,"Y-m-d").'.'.$file->extension;                
            }
            $path=$this->uploadFolder . $model->ficheiro;
            $file->saveAs($path,true);                                                                
            return true;            
        }
        return false;
    }
    
    public function actionImport() {
        $p = Yii::$app->request->post();
        $ficheiro = $p['file'];
        $data = $p['date'];
        $selection = $p['selection'];
        foreach($selection as $i=>$v) $selection[$i]= Json::decode($v);
        
        $transaction = Yii::$app->db->beginTransaction();
        try {            
            $result=[];
            foreach ($selection as $v) {
                $reg = new EvolucaoVendas();
                $reg->ISBN = $v['isbn'];
                $reg->quantidade = intval($v['quantidade']);
                $reg->data       = $data;                
                if($reg->validate()){                    
                    $reg->save();                   
                    $result[]=
                        array_merge(
                                $reg->toArray(),
                                ['message'=>'ok']
                        );
                }else {
                    $err= [];
                    foreach($reg->errors as $field=>$message){
                        $err['campo']=$field;
                        $err['mensagem']=$message[0];
                    }
                    $result[] = array_merge(
                                $reg->toArray(),
                                ['message'=>[$err]]
                        );
                }
            }            
            $transaction->commit();
            return $this->render('import', [
                'file'        => $ficheiro,
                'data'        => $data,
                'result'      => $result,                                             
                'columns'     => 
                       ['ISBN',
                        'data',
                        [
                            'attribute'=>'quantidade',            
                            'hAlign'=>'right',
                        ],                           
                        [                            
                            'attribute' => 'resultado',
                            'format' => 'raw',
                            'contentOptions'=>['style'=>'width:40%;'],
                            'value' => function ($model) {                      
                                            if($model['message']=='ok'){
                                                return '<div>'.$model['message'].'</div>';
                                            }else{
                                                return GridView::widget([
                                                            'layout'=>"{items}",                                                            
                                                            'showHeader' => false, 
                                                            'tableOptions'=>['class'=>'table table-bordered',
                                                                    'style'=>'background-color:transparent'],
                                                            'dataProvider' => new ArrayDataProvider([
                                                                                        'allModels' => $model['message'],                                    
                                                                            ]),                            
                                                            'columns' => [
                                                                [
                                                                    'attribute'=>'campo',
                                                                    'contentOptions'=>['style'=>'border-color: transparent;'],
                                                                ],
                                                                [
                                                                    'attribute'=>'mensagem',
                                                                    'contentOptions'=>['style'=>'border-color: transparent;'],
                                                                ]
                                                                ],

                                                        ]);
                                            };
                                    },

                        ]]
            ]);
        } catch (Exception $e) {            
            $transaction->rollBack();
            throw EvolucaoVendasImportException($e->getMessage());
        }
        
    }
    
    /**
     * Importa relatorio mensal de vendas de Publicação.
     * @return mixed
     */
    public function actionUpload()
    {
        $model = new EvolucaoVendasImporta();
        
        if( $this->saveFile($model))
            return $this->redirect(['confirmation', 
                            'data'      => $model->dataImportacao,
                            'file'      => $model->ficheiro]);
        else
            return $this->render('upload', [
                'model'     => $model,             
            ]);
    }
    
    public function actionConfirmation($data,$file)
    {
        $this->getView()->registerJsFile("/js/color.js");
        $path=$this->uploadFolder . $file;        
                  $a=isset($record['isbn']);
  
        $ex = new MyExcel();
        $ex->readSpreadsheetData($path);        

        $arr=$ex->export2Array( new \app\components\MyExcelImportModel('importacaoVendas.json') );
        foreach($arr as $recno=>$record){
            $model =Publicacao::findOne(['isbn'=>$record['isbn']]);
            $arr[$recno]['valid'] = 
                    isset($record['isbn']) && (Publicacao::findOne(['isbn'=>$record['isbn']]) !== null) ;            
        }
        
        return $this->render('import_confirmation', [
            'array'     => $arr, 
            'columns'   => array_merge([
                            [
                                'class' => 'yii\grid\CheckboxColumn',
                                'checkboxOptions' => function($model, $key, $index, $column) {
                                      return $model['valid'] 
                                              ? ['checked' =>'true',
                                                'value'=>Json::encode([
                                                                'isbn'=>$model['isbn'].'',
                                                                'quantidade'=>$model['quantidade']])] 
                                              : ['disabled'=>'' ,
                                                'value'=>Json::encode([
                                                                'isbn'=>$model['isbn'].'',
                                                                'quantidade'=>$model['quantidade']])];
                                }

                            ]
                        ], $ex->getModel()->columns),
            'file'      => $file,
            'data'      => $data,
        ]);
    }

    /**
     * Lists all EvolucaoVendas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EvolucaoVendasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EvolucaoVendas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EvolucaoVendas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EvolucaoVendas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EvolucaoVendas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EvolucaoVendas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EvolucaoVendas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EvolucaoVendas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EvolucaoVendas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}


class EvolucaoVendasImportException extends HttpException
{
    /**
     * Constructor.
     * @param string $message error message
     * @param integer $code error code
     * @param \Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        parent::__construct(404, $message, $code, $previous);
    }
}
