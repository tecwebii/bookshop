<?php

namespace app\controllers;

use Yii;
use app\models\Editora;
use app\models\EditoraSearch;
use app\components\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\My;

/**
 * EditoraController implements the CRUD actions for Editora model.
 */
class EditoraController extends MyController
{  
    /**
     * Lists all Editora models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EditoraSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Editora model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Editora model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Editora();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Editora model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Editora model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Editora model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Editora the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Editora::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    const MIN_SEARCH_LEN=0;
    public function actionAutocomplete(){                                    
        $id       =Yii::$app->getRequest()->getQueryParam('id');
        $value    =Yii::$app->getRequest()->getQueryParam('value');
        $offset   =Yii::$app->getRequest()->getQueryParam('offset');   
        $limit    =Yii::$app->getRequest()->getQueryParam('limit');   
        $minLength =self::MIN_SEARCH_LEN;
        if($minLength>0 && ($value=='' || strlen($value)<$minLength)) {
            echo CJSON::encode(__METHOD__.' precisa pelo menos '.$minLength.' caracteres, para proceder');
            return;
        }           
        
        $ret=array();        
        if( !empty($id) || is_numeric($value)) {
            $val=!empty($id) ? $id : $value;
            $model = Editora::find()->select('id,nome')->where( ["id"=> intval($val)] )->all();
        }
        if (!isset($model) && empty($id)) {
            $offset= $offset=='' 
                    ? strlen($value)
                    : (intval($offset)<$minLength
                            ? $minLength
                            : intval($offset));
            $val=substr($value,0,$offset);
            $model = Editora::find()->select('id,nome')
                            ->where( ['like','nome',"$val"])->all();
        }
         if ($model !== null) {            
            foreach($model as $k=>$rec) {
                            $ret[] = array(
                                'label'=>$rec['id'].' '.$rec['nome'],
                                'value'=>$rec['id'], 
//                                'id'=>$rec['id'],
//                                'nome' =>$rec['nome'],
                              ); 
                        }   
        }
         My::response_json($ret);            
    }
}
