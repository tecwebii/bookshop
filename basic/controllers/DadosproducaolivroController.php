<?php

namespace app\controllers;

use Yii;
use app\models\DadosProducao;
use app\models\DadosProducaoLivro;
use app\models\DadosProducaoLivroSearch;
use app\models\Publicacao;
use app\components\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * DadosProducaoLivroController implements the CRUD actions for DadosProducaoLivro model.
 */
class DadosproducaolivroController extends MyController
{
    public $layout = 'empty';
                
    /**
     * Displays a single DadosProducaoLivro model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DadosProducaoLivro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function Create($id)
    {
        if (($pubModel = Publicacao::findOne($id)) !== null) {
            $model   = new DadosProducaoLivro();        
            $isLoaded=$model->load(Yii::$app->request->post());
            $model->id = $pubModel->id;
            if($isLoaded && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
        
    }
    
    public function actionValidate($id)
    {       
        $model = new DadosProducaoLivro();
        if ($model->load(Yii::$app->request->post())  )  {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }            
        }     
    }

    
    /**
     * Updates an existing DadosProducaoLivro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {       
        $model = $this->findModel($id);
        if($model==null) 
            return $this->Create ($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Deletes an existing DadosProducaoLivro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        //return $this->redirect(['index']);
    }

    /**
     * Finds the DadosProducaoLivro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DadosProducaoLivro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Publicacao::findOne($id)) !== null) {
            return DadosProducaoLivro::findOne($id);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
