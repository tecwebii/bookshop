<?php

namespace app\controllers;

use Yii;
use app\models\ReciboPublicacao;
use app\models\ReciboPublicacaoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReciboPublicacaoController implements the CRUD actions for ReciboPublicacao model.
 */
class RecibopublicacaoController extends Controller
{
    public $layout = 'empty';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReciboPublicacao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $idRecibo = Yii::$app->request->post('expandRowKey');
        $idRecibo = $idRecibo !='' ? $idRecibo : Yii::$app->request->get('id');
        if($idRecibo!=''){
            $model = ReciboPublicacaoSearch::find()
                    ->where(['idRecibo'=>$idRecibo])
                    ->andWhere(['<>','valor',0]);            
            return $this->render('index', [
                'model' => $model,                
            ]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    
    /**
     * Finds the ReciboPublicacao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idRecibo
     * @param integer $idPublicacao
     * @param integer $idAutor
     * @param string $tipoAutoria
     * @return ReciboPublicacao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idRecibo, $idPublicacao, $idAutor, $tipoAutoria)
    {
        if (($model = ReciboPublicacao::findOne(['idRecibo' => $idRecibo, 'idPublicacao' => $idPublicacao, 'idAutor' => $idAutor, 'tipoAutoria' => $tipoAutoria])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
