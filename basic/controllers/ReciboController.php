<?php

namespace app\controllers;

use Yii;
use app\models\Recibo;
use app\models\ReciboSearch;
use app\components\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\widgets\ActiveForm;
use yii\web\Response;

/**
 * ReciboController implements the CRUD actions for Recibo model.
 */
class ReciboController extends MyController
{
    
    public function beforeAction($action) {
        return parent::beforeAction($action);
    }
        
    /**
     * Lists all Recibo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReciboSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recibo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recibo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recibo();

        if ($model->load(Yii::$app->request->post())){
            if( Yii::$app->request->isAjax ){                         
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model); 
            }else {
                $model->save();
                return $this->redirect(['update', 'id' => $model->id]);
            } 
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Recibo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);        

        if( Yii::$app->request->isPost ){            
            if(($valor = Yii::$app->request->post('ReciboPublicacao'))!=null) {
                if( Yii::$app->request->isAjax ){
                    $porLiquidar = $valor['porLiquidar'];unset($valor['porLiquidar']);
                    reset($valor);$valor = current($valor);
                    $valor = $valor['pagamento'];
                    
                    // end get valor pagamento
                    // get chave primaria do regisro adicionado ou alterado
                    $key=Yii::$app->request->post('editableKey');
                    $key= unserialize($key); unset($key['data']);
                    // Procura chave primaria
                    $rp= \app\models\ReciboPublicacao::findOne($key);
                    if($rp==null){   // se não existe - adiciona
                        $rp = new \app\models\ReciboPublicacao();
                        $rp->setAttributes($key);
                    }
                    $rp->pagamento=$valor;   //  Atribui valor pago
                    $rp->porLiquidar=$porLiquidar;
                    //
                    ActiveForm::validate($rp); 
                    if (! $rp->hasErrors()) {
                        $rp->save ();
                    }                
                    Yii::$app->response->format = Response::FORMAT_JSON;                  
                    $a=$rp->getErrors();
                    return $rp->hasErrors() 
                                ? [ 'message'=>isset($a['pagamento'][0]) ? $a['pagamento'][0] : 'Erro' ]
                                : ['output'=>  \Yii::$app->formatter->asCurrency($valor), 'message'=>''];
                }
            }elseif(Yii::$app->request->post('Recibo')!=null){
                $model->load(Yii::$app->request->post());
                if( Yii::$app->request->isAjax ){                         
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model); 
                }else 
                    $model->save();
            }        
        }        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    

    /**
     * Detalhes of an existing Recibo model.     
     * @param integer $id
     * @return mixed
     */
    public function actionDetalhes($id)
    {
       /* $id=Yii::$app->request->post('expandRowKey');
        $id=$id!='' ? $id : Yii::$app->request->getQueryParam('expandRowKey');*/
        
        $model = $this->findModel($id);
        
        return $this->render('detalhes', [            
            'detalhes'=>$model->getDetalhes(), 
        ]);

    }
    
    
    /**
     * Deletes an existing Recibo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $data = \app\components\My::sqlData($model->data);
        $exist = Recibo::find()->where(["idAutor"=>$model->idAutor])
                        ->andWhere("data>'$data'")->one();
        if( $exist != null ) {
            throw new ReciboInvalidoException(Yii::t('app','Só pode apagar o ultimo recibo de cada Autor/Coordenador.'));
        }else{
            \app\models\ReciboPublicacao::deleteAll("idRecibo=$id");
            $model->delete();        
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Recibo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recibo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recibo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}


class ReciboInvalidoException extends \yii\base\UserException
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Não pode apagar recibo';
    }
}