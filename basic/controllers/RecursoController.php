<?php

namespace app\controllers;

use Yii;
use app\components\My;
use app\models\Recurso;
use app\models\RecursoSearch;
use app\components\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecursoController implements the CRUD actions for Recurso model.
 */
class RecursoController extends MyController
{    
    /**
     * Lists all Recurso models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecursoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recurso model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recurso model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recurso();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Recurso model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Recurso model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Recurso model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recurso the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recurso::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    const MIN_SEARCH_LEN=0;
    public function actionAutocompletepaginador(){                                    
        return $this->autoComplete(\app\models\Recurso::TIPO_Paginacao);
    }
    public function actionAutocompletecapa(){                                    
        return $this->autoComplete(\app\models\Recurso::TIPO_Capa);
    }
    
    private function Autocomplete($tipo){                                        
        $id       =Yii::$app->getRequest()->getQueryParam('id');
        $value    =Yii::$app->getRequest()->getQueryParam('value');
        $offset   =Yii::$app->getRequest()->getQueryParam('offset');   
        $limit    =Yii::$app->getRequest()->getQueryParam('limit');   
        $minLength =self::MIN_SEARCH_LEN;
        if($minLength>0 && ($value=='' || strlen($value)<$minLength)) {
            echo CJSON::encode(__METHOD__.' precisa pelo menos '.$minLength.' caracteres, para proceder');
            return;
        }           
        
        $ret=array();        
        $model = Recurso::find()->select('id,nome')->where(['tipoRecurso'=>$tipo]);
        if( !empty($id) || is_numeric($value)) {
            $val=!empty($id) ? $id : $value;
            $model = $model->andwhere( ["id"=> intval($val)] );
        }
        if (!isset($model) && empty($id)) {
            $offset= $offset=='' 
                    ? strlen($value)
                    : (intval($offset)<$minLength
                            ? $minLength
                            : intval($offset));
            $val=substr($value,0,$offset);
            $model = $model->andwhere( ['like','upper(nome)', "$val"]);
        }
        $model = $model->all();
         if ($model !== null) {            
            foreach($model as $k=>$rec) {
                            $ret[] = array(
                                'label'=>$rec['id'].' '.$rec['nome'],
                                'value'=>$rec['id'], 
   //                             'id'=>$rec['id'],
   //                             'nome' =>$rec['nome'],
                              ); 
                        }   
        }
         My::response_json($ret);            
    }
}
