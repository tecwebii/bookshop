<?php

namespace app\controllers;

use Yii;
use app\models\Publicacao;
use app\models\PublicacaoSearch;
use app\models\DadosProducaoLivro;
use app\components\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\components\MyExcel;
use yii\helpers\Json;

/**
 * PublicacaoController implements the CRUD actions for Publicacao model.
 */
class PublicacaoController extends MyController
{
    public $layout = 'main';    
    
    public $uploadFolder;
    
    public function init() {
        $this->uploadFolder = Yii::getAlias('@app') . "\upload_publicacoes\\";
        parent::init();
    }
        
    private function saveFile($model){
        if ($model->load(Yii::$app->request->post())){
            $file = \yii\web\UploadedFile::getInstance($model, 'ficheiro');              
            if($file!=null) {    
                $date = date_create($model->dataImportacao);
                $model->ficheiro='Publicacoes_'.date_format($date,"Y-m-d").'.'.$file->extension;                                
            }
            $path=$this->uploadFolder . $model->ficheiro;
            $file->saveAs($path,true);                                                                
            return true;            
        }
        return false;
    }    

    public function actionImport() {                 
        
        // retrieve selected ISBNs
        $p = Yii::$app->request->post();
        $ficheiro = $p['file'];
        $data = $p['date'];
        $selection = $p['selection'];
        foreach($selection as $i=>$v) {
            $selection[$i]= Json::decode($v);
            $selected[$selection[$i]['ISBN']] = true;
        }
        // retrieve data
        $storeVar = new \app\components\MyStoreVariable( $this->uploadFolder . basename($ficheiro).'.php' );        
        if(! $storeVar->exists()) {        
            return;
        }
        $arr = $storeVar->load();
        //
        $impModel = new \app\components\MyExcelImportModel('importacaoPublicacoes.json');
        
        $transaction = Yii::$app->db->beginTransaction();
        try {            
            $result=[];
            $i = 0;
            foreach ($arr as $v) {   
                if(! array_key_exists($v['ISBN'].'', $selected) || ! $v['_valid'])
                    continue;
                
                $reg1 = new Publicacao();
                $reg2 = new DadosProducaoLivro();                                    
                foreach ( $v as $fName => $fValue ) {
                    $fValue= $impModel->fixVal($fName, $fValue);                    
                    if    ( $reg1->hasAttribute( $fName ) )
                        $reg1->$fName = $fValue;
                    elseif( $reg2->hasAttribute( $fName ) )
                        $reg2->$fName = $fValue;
                                                            
                    if(strtolower($fName)=='editora'){
                        $reg1->idEditora = \app\models\Editora::insertIfNotExist($fValue);
                    }                    
                }
                                
                if( $reg1->validate() && $reg2->validate()) {                    
                    $reg1->save();                   
                    $reg2->id = $reg1->id;                    
                    $reg2->save();
                    
                    if(isset($v['autores'])){
                        $autores=  explode(";", $v['autores'].'');
                        foreach($autores as $autor) {
                            $nomes= explode(',',$autor);
                            $autorId = \app\models\Autor::insertIfNotExist( 
                                    trim($nomes[sizeof($nomes)-1]) ,
                                    sizeof($nomes)==3 ? trim($nomes[1]):'' ,
                                    sizeof($nomes)>1 ? trim($nomes[0]):'' 
                                    );
                            \app\models\PublicacaoAutor::insertIfNotExist( $reg1->id, $autorId, 'Autor');
                        }                    
                    }
                    
                    if(isset($v['vendas'])){
                        $ev = new \app\models\EvolucaoVendas();
                        $ev->ISBN       = $reg1->ISBN;
                        $ev->data       = strftime("%Y-%m-%d", strtotime("$reg1->dataEdicao +1 day"));
                        $ev->quantidade = $impModel->fixVal('vendas', $v['vendas']);
                        $ev->save();
                    }
                }else {
                    /*$err= [];
                    foreach(array_merge($reg1->errors,$reg2->errors) as $field=>$message){
                        $err['campo']=$field;
                        $err['_mensagem']=$message[0];
                    }                    */
                    $result[] = array_merge(
                                $reg1->toArray(),
                                ['_message'=>  \app\components\My::modelError2String(array_merge($reg1->errors,$reg2->errors))]
                        );
                }
            }            
            $transaction->commit();
            if($result==[]){
                $this->redirect('/publicacao');
            }else
                return $this->render('import', [
                    'file'        => $ficheiro,
                    'data'        => $data,
                    'result'      => $result,                                             
                    'columns'     => array_merge([
                            [   'class' => '\kartik\grid\ExpandRowColumn',                
                                'expandTitle'=>Yii::t('app','Expande'),
                                'collapseTitle'=>Yii::t('app','Colapsa'),
                                'collapseAllTitle' => Yii::t('app','Colapsa todos'),
                                'expandAllTitle' => Yii::t('app','Expande todos'),                
                                'expandIcon'=>'<span class="glyphicon glyphicon-plus-sign"></span>',
                                'collapseIcon'=>'<span class="glyphicon glyphicon-minus-sign"></span>',
                                'value' => function ($model, $key, $index, $column) {
                                               return $model['_message']=='' 
                                                       ? \kartik\grid\GridView::ROW_COLLAPSED
                                                       : \kartik\grid\GridView::ROW_EXPANDED;
                                },
                                'detail' => function ($model, $key, $index, $column) {
                                        return $model['_message'];
                                },                                    
                                'pageSummary' => false,
                            ],                                       
                        ], $impModel->columns),                                                                                 
                ]);
        } catch (Exception $e) {            
            $transaction->rollBack();
            throw EvolucaoVendasImportException($e->getMessage());
        }
        
    }
    
    /**
     * Importa relatorio mensal de vendas de Publicação.
     * @return mixed
     */
    public function actionUpload()
    {
        $model = new \app\models\PublicacaoImporta();
        
        if( $this->saveFile($model)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->redirect([
                    '/publicacao/confirmation',
                    'data'      => $model->dataImportacao,
                    'file'      => $model->ficheiro,
                    'random'    => $model->random]);
        } else
            return $this->render('upload', [
                'model'     => $model,             
            ]);
    }
    
    public function actionGetprogress($key) {
        $progressVar = new \app\components\MyStoreVariable();
        $progressVar->setFullPath($key.'.php');
        Yii::$app->response->format = Response::FORMAT_JSON;
        for($i=0;$i<10;$i++){
            if($progressVar->exists()) {
                $value = $progressVar->load();
                return ['progress'=>$value];
            }else
                sleep (0.025);
        }
    }
    
    public function actionConfirmation($data,$file,$random)
    {    
        $this->getView()->registerJsFile("/js/color.js");
        $path=$this->uploadFolder . $file;                          
  
        $impModel = new \app\components\MyExcelImportModel('importacaoPublicacoes.json');
        
        $storeVar = new \app\components\MyStoreVariable( $this->uploadFolder . basename($file).'.php' );        
//        $progressVar = new \app\components\MyStoreVariable();
//        $progressVar->setFullPath($random.'.php');
//        $progressBar=0;
//        $progressVar->save($progressBar);
        if(! $storeVar->exists()) {
            $ex = new MyExcel();
            $ex->readSpreadsheetData($path);        
//            $progressBar+=5;$progressVar->save($progressBar);            
            $arr=$ex->export2Array( $impModel );
//            $i=0;
            foreach($arr as $recno=>$record){
//                $i++;
//                $progressBar= 5 + ( $i/sizeof($arr)*95 ); $progressVar->save($progressBar);
                $arr[$recno]['_valid'] = true;
                if( $arr[$recno]['_valid'] ) {
                    $m1 = new Publicacao();
                    $m2 = new DadosProducaoLivro();                    
                    foreach ( $record as $fName => $fValue ) {
                    //    sleep(20);
                        $fValue= $impModel->fixVal($fName, $fValue);                        
                        if    ( $m1->hasAttribute( $fName ) )
                            $m1->$fName = $fValue;
                        elseif( $m2->hasAttribute( $fName ) )
                            $m2->$fName = $fValue;
                    }
                    $arr[$recno]['_valid'] = $m1->validate();
                    $arr[$recno]['_valid'] = $arr[$recno]['_valid'] && $m2->validate();
                    $arr[$recno]['_message'] = \app\components\My::modelError2String(array_merge($m1->getErrors(),$m2->getErrors()));                    
                }                
            }
            $storeVar->save($arr);            
        }else{
            $arr = $storeVar->load();
        }
        
        return $this->render('import_confirmation', [
            'array'     => $arr, 
            'columns'   => array_merge([
                            [
                                'class' => 'yii\grid\CheckboxColumn',
                                'checkboxOptions' => function($model, $key, $index, $column) {
                                      return $model['_valid'] 
                                              ? ['checked' =>'true',
                                                'value'=>Json::encode(['ISBN'=>$model['ISBN'].''])] 
                                              : ['disabled'=>'' ,
                                                'value'=>Json::encode(['ISBN'=>$model['ISBN'].'',])];
                                }

                            ],
                            [   'class' => '\kartik\grid\ExpandRowColumn',                
                                'expandTitle'=>Yii::t('app','Expande'),
                                'collapseTitle'=>Yii::t('app','Colapsa'),
                                'collapseAllTitle' => Yii::t('app','Colapsa todos'),
                                'expandAllTitle' => Yii::t('app','Expande todos'),                
                                'expandIcon'=>'<span class="glyphicon glyphicon-plus-sign"></span>',
                                'collapseIcon'=>'<span class="glyphicon glyphicon-minus-sign"></span>',
                                'value' => function ($model, $key, $index, $column) {
                                               return $model['_message']=='' 
                                                       ? \kartik\grid\GridView::ROW_COLLAPSED
                                                       : \kartik\grid\GridView::ROW_EXPANDED;
                                },
                                'detail' => function ($model, $key, $index, $column) {
                                        return $model['_message'];
                                },                                    
                                'pageSummary' => false,
                            ],                                       
                        ], $impModel->columns),
            'file'      => $file,
            'data'      => $data,
        ]);
    }
    

    /**
     * Lists all Publicacao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PublicacaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionChart($id)
    {
        $this->layout='empty';
        $model=$this->findModel($id);            
        return $this->render('_chart', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Publicacao model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Displays a single Publicacao model.
     * @param integer $id
     * @return mixed
     */
    public function actionDetalhes()
    {
        $id=Yii::$app->request->post('expandRowKey');        
        $id=$id!='' ? $id : Yii::$app->request->getQueryParam('id');
        
        $this->layout='empty';
        return $this->render('_view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Publicacao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {        
        $this->getView()->registerJsFile("/js/MyTabs.js",['depends'=>'yii\web\JqueryAsset',]);
        $model   = new Publicacao();   
        $model->tipoPublicacao = 'Livro';
        
        if (isset($_POST['Publicacao']) && $model->load(Yii::$app->request->post())  )  {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }else
                if($model->save())
                    $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('create', [
                'model'   => $model,
            ]);                        
    }

    /**
     * Updates an existing Publicacao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {                
        $this->getView()->registerJsFile("/js/MyTabs.js",['depends'=>'yii\web\JqueryAsset',]);
        $model = $this->findModel($id);
                
        if (isset($_POST['Publicacao']) && $model->load(Yii::$app->request->post())  )  {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }else
                $model->save();
            
        }
                 
        return $this->render('update', [
                'model'     => $model,
            ]);
        
    }

    /**
     * Deletes an existing Publicacao 
     * model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Publicacao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Publicacao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Publicacao::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
