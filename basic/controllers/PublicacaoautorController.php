<?php

namespace app\controllers;

use Yii;
use app\models\PublicacaoAutor;
use app\models\PublicacaoAutorSearch;
use app\components\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;


/**
 * PublicacaoAutorController implements the CRUD actions for PublicacaoAutor model.
 */
class PublicacaoautorController extends MyController
{
    public $layout = 'empty';
    public $uploadFolder;
    
    public function init() {
        $this->uploadFolder = Yii::getAlias('@app') . "\upload_contratos\\";
        parent::init();
    }
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PublicacaoAutor models.
     * @return mixed
     */
    public function actionIndex($idPublicacao,$tipoAutoria)
    {             
        $model = new PublicacaoAutor();
        $model = $model->find()
                        ->where(['idPublicacao'=>$idPublicacao,
                                'tipoAutoria' =>$tipoAutoria]);
        
        $dataProvider=new \yii\data\ActiveDataProvider([
                    'query' => $model,
                    'pagination' => [
                        'pageSize' => 9999,
                    ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'filtro'       => ['id'=>$idPublicacao, 'tipoAutoria'=>$tipoAutoria],
        ]);
    }

    /**
     * Displays a single PublicacaoAutor model.
     * @param integer $id     
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
        
    
    public function actionValidate()
    {   
        $model = new PublicacaoAutor();     
        $values=Yii::$app->request->post();
        if(isset($values['PublicacaoAutor'])) {
            $values=$values['PublicacaoAutor'];
            if(isset($values['id']) && $values['id']!='')
                $model = $this->findModel($values['id']);
        }
        
        if ($model->load(Yii::$app->request->post())  )  {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }            
        }
     
    }

    /**
     * 
     * @param PublicacaoAutor $model
     * @return boolean
     */
    private function save($model){
        
        if ($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model, 'contrato');        
            //Yii::$app->security->generateRandomString().".{$ext}
            if($file!=null) {           
                $model->originalFileName=$file->baseName . '.' . $file->extension;
                $model->fileName=Yii::$app->security->generateRandomString().'.'.$file->extension;                
                $model->fileName=$model->getBuildFileName().'.'.$file->extension;
            }
            if($model->save()) {
                if($file!=null) {
                    $path=$this->uploadFolder . $model->fileName;
                    $file->saveAs($path);                                                    
                }
                return true;
            }
        }
        return false;
    }
    /**
     * Creates a new PublicacaoAutor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idPublicacao,$tipoAutoria)
    {
        $model = new PublicacaoAutor();
        $model->idPublicacao=$idPublicacao;
        $model->tipoAutoria=$tipoAutoria;

        if( ! $this->save($model))                
            return $this->render('create', [
                    'model' => $model,                
                ]);
    }
    
    public function actionDownload($idPublicacao, $idAutor, $tipoAutoria){
        $model = $this->findModel($idPublicacao, $idAutor, $tipoAutoria);
        
        if(file_exists($this->uploadFolder.$model->fileName))
        {
          return Yii::$app
                    ->getResponse()
                    ->sendFile( $this->uploadFolder.$model->fileName ,
                                $model->getOriginalFileName());                          
        }

    }

    /**
     * Updates an existing PublicacaoAutor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id     
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ( ! $this->save($model) )
            return $this->render('update', [
                'model' => $model,                
            ]);
    }

    /**
     * Deletes an existing PublicacaoAutor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     *      
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        //return $this->redirect(['index']);
    }

    /**
     * Finds the PublicacaoAutor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id     
     * @return PublicacaoAutor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PublicacaoAutor::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
        
}
