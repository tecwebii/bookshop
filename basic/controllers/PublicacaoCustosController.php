<?php

namespace app\controllers;

use Yii;
use app\models\PublicacaoCustos;
use app\models\PublicacaoCustosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PublicacaoCustosController implements the CRUD actions for PublicacaoCustos model.
 */
class PublicacaoCustosController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PublicacaoCustos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PublicacaoCustosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PublicacaoCustos model.
     * @param integer $idPublicacao
     * @param string $rubrica
     * @return mixed
     */
    public function actionView($idPublicacao, $rubrica)
    {
        return $this->render('view', [
            'model' => $this->findModel($idPublicacao, $rubrica),
        ]);
    }

    /**
     * Creates a new PublicacaoCustos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PublicacaoCustos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idPublicacao' => $model->idPublicacao, 'rubrica' => $model->rubrica]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PublicacaoCustos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $idPublicacao
     * @param string $rubrica
     * @return mixed
     */
    public function actionUpdate($idPublicacao, $rubrica)
    {
        $model = $this->findModel($idPublicacao, $rubrica);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idPublicacao' => $model->idPublicacao, 'rubrica' => $model->rubrica]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PublicacaoCustos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $idPublicacao
     * @param string $rubrica
     * @return mixed
     */
    public function actionDelete($idPublicacao, $rubrica)
    {
        $this->findModel($idPublicacao, $rubrica)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PublicacaoCustos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $idPublicacao
     * @param string $rubrica
     * @return PublicacaoCustos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idPublicacao, $rubrica)
    {
        if (($model = PublicacaoCustos::findOne(['idPublicacao' => $idPublicacao, 'rubrica' => $rubrica])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
