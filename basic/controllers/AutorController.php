<?php

namespace app\controllers;

use Yii;
use app\components\My;
use app\components\MyController;
use app\models\Autor;
use app\models\AutorSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

 
/**
 * AutorController implements the CRUD actions for Autor model.
 */
class AutorController extends MyController
{
    public $layout='main';
    

    /**
     * Lists all Autor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AutorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    /**
     * Displays a single Autor chart.
     * @param integer $id
     * @return mixed
     */
    public function actionChart($id)
    {
      //  $this->layout = 'empty';
        return $this->render('_chart', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Autor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Autor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Autor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Updates an existing Autor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDetalhes()
    {
        $id=Yii::$app->request->post('expandRowKey');
        $id=$id!='' ? $id : Yii::$app->request->getQueryParam('expandRowKey');
        
        $this->layout='empty';
        $model = $this->findModel($id);
        return $this->render('detalhes', [
            'model' => $model,            
        ]);
    }
    

    /**
     * Updates an existing Autor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Autor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Autor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Autor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Autor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    const MIN_SEARCH_LEN=0;
    public function actionAutocomplete(){                                    
        $value    =Yii::$app->getRequest()->getQueryParam('value');
        $offset   =Yii::$app->getRequest()->getQueryParam('offset');            
        $minLength =self::MIN_SEARCH_LEN;
        if($minLength>0 && ($value=='' || strlen($value)<$minLength)) {
            My::response_json([__METHOD__.' precisa pelo menos '.$minLength.' caracteres, para proceder']);
            return;
        }           
        
        $ret=array();
 
        if( !empty($id) || is_numeric($value)) {
            $val=!empty($id) ? $id : $value;
            $model = Autor::find()
                        ->select("id,primeiroNome,nomeDoMeio,apelido")
                        ->where( ["id"=> intval($val)] )->all();
        }
        if (!isset($model) && empty($id)) {
            $offset= $offset=='' 
                    ? strlen($value)
                    : (intval($offset)<$minLength
                            ? $minLength
                            : intval($offset));
            $val=substr($value,0,$offset);
        
            $model = Autor::find()
                    ->select("id,primeiroNome,nomeDoMeio,apelido")
                    ->where( $val==''?'':['like',"concat(primeiroNome,' ',nomeDoMeio,' ',apelido)" ,"$val"] )
                    ->all();
        }
        if ($model !== null) {            
            foreach($model as $k=>$rec) {
                            $ret[] = array(
                                'label'=>$rec['id'].' - '.$rec['primeiroNome'].' '.$rec['nomeDoMeio'].' '.$rec['apelido'],
                                'value'=>$rec['id'], 
                              ); 
                        }   
        }
         My::response_json($ret);            
    }
}

