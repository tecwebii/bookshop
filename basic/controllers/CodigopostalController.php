<?php

namespace app\controllers;

use Yii;
use app\components\My;
use yii\web\Controller;
use app\models\CodigoPostal;
use app\models\CodigoPostalSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CodigoPostalController implements the CRUD actions for CodigoPostal model.
 */
class CodigopostalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CodigoPostal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CodigoPostalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CodigoPostal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CodigoPostal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CodigoPostal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CodigoPostal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CodigoPostal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CodigoPostal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CodigoPostal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CodigoPostal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    Const MIN_SEARCH_LEN=1;
    public function actionAutocomplete(){
        $id       =Yii::$app->getRequest()->getQueryParam('id');
        $value    =Yii::$app->getRequest()->getQueryParam('value');
        $offset   =Yii::$app->getRequest()->getQueryParam('offset');   
        $limit    =Yii::$app->getRequest()->getQueryParam('limit');   
        $limit    =isset($limit) ? $limit : 40;
        $minLength =self::MIN_SEARCH_LEN;
        if($minLength>0 && (strlen($value)<$minLength && strlen($id)<$minLength)) {
            My::response_json([__METHOD__.' precisa pelo menos '.$minLength.' caracteres, para proceder']);
            return;
        }                   
        $model = null;
        $mod   = CodigoPostal::find()->select([                                
                                'codpostal',
                                'localidade',
                                'cp_designacao',
                                'distrito',
                                'morada',
                                'concelho'])->orderBy("codpostal");
                
        $ret=array();        
        if( !empty($id) && 
                preg_match(CodigoPostal::zipCodRegExMask(), $id, $matches) && 
                isset($matches[0]) && count($matches)>0 ) {
            $model=$mod->limit(1)->where( ["like","codpostal",$matches[0]] )->all();
        }else if( ! empty($value) ) {        
            $offset= $offset=='' 
                    ? strlen($value)
                    : (intval($offset)<$minLength
                            ? $minLength
                            : intval($offset));
            $val=substr($value,0,$offset);
            $model=$mod->limit($limit)
                        ->where( ["like","codpostal",$val] )
                        ->orWhere(["like","localidade",$val])->all();
        }
         if ($model !== null) {            
            foreach($model as $k=>$rec) {
                            $ret[] = array(
                                'label'=>$rec['codpostal'].' '.$rec['localidade'],                                        
                                'value'=>$rec['codpostal'], 
                                'cp_designacao'=>$rec['cp_designacao'],
                                'distrito' =>$rec['distrito'],
                                'concelho' =>$rec['concelho'],
                                'morada' =>$rec['morada'],
                              ); 
                        }   
        }
         My::response_json($ret);            
    }
    
    
    public function actionHtml(){
        $id       =Yii::$app->getRequest()->getQueryParam('id');
        $value    =$id!='' ? $id : Yii::$app->getRequest()->getQueryParam('value');                
        if(strlen($value)!=4 && strlen($value)!=8) {            
            echo '';
            exit();
        }       
        $len=  strlen($value);
        $model=CodigoPostal::find()->select([                                
                                'codpostal',
                                'localidade',
                                'cp_designacao',
                                'distrito',
                                'morada',
                                'concelho'])
                ->limit(1)
                ->where( ["substr(codpostal,1,$len)"=>$value] );
        $model=$model->all();        
        if(isset($model[0])) {
            $rec=$model[0];
            echo $rec['codpostal'].' '.$rec['localidade']
                    . '<br>Designação: ' . $rec['cp_designacao']
                    . '<br>Distrito: '   . $rec['distrito']
                    . '<br>Concelho: '   . $rec['concelho']
                    . (strlen($value)==8 && trim($rec['morada'])==''? '':'<br>Morada: ' . $rec['morada']);
            exit();
        }
    }
}

