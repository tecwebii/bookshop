<?php

namespace app\controllers;

use Yii;
use app\models\PublicacaoCusto;
use app\models\PublicacaoCustoSearch;
use app\components\MyController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * PublicacaoCustoController implements the CRUD actions for PublicacaoCusto model.
 */
class PublicacaocustoController extends MyController
{
    public $layout = 'empty';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PublicacaoCusto models.
     * @return mixed
     */
    public function actionIndex($id)
    {                
        $model = new PublicacaoCusto();
        $model = $model->find()
                        ->where(['idPublicacao'=>$id]);
        
        $dataProvider=new \yii\data\ActiveDataProvider([
                    'query' => $model,
                    'pagination' => [
                        'pageSize' => 9999,
                    ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'idPublicacao' => $id,
        ]);
    }

    /**
     * Displays a single PublicacaoCusto model.
     * @param integer $id     
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionValidate()
    {       
        $model = new PublicacaoCusto();        
        $values=Yii::$app->request->post();
        if(isset($values['PublicacaoCusto'])) {
            $values=$values['PublicacaoCusto'];
            if(isset($values['id']) && $values['id']!='')
                $model = $this->findModel($values['id']);
        }
        
        if ($model->load(Yii::$app->request->post())  )  {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }            
        }
     
    }
    
    /**
     * Creates a new PublicacaoCusto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new PublicacaoCusto();
        $model->idPublicacao = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'idPublicacao' => $model->idPublicacao, 'rubrica' => $model->rubrica]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PublicacaoCusto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id     
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'idPublicacao' => $model->idPublicacao, 'rubrica' => $model->rubrica]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PublicacaoCusto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id     
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PublicacaoCusto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id     
     * @return PublicacaoCusto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PublicacaoCusto::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
